const { Tokens } = require("../scripts/constants");
const { verifyContractHelper } = require("../scripts/deployHelper");

module.exports = async (hre) => {
  const network = await hre.ethers.provider.getNetwork();
  const signers = await hre.ethers.getSigners();
  const accounts = await hre.getNamedAccounts();
  const deployer = accounts.ledger ?? signers[0].address;

  const mining = await hre.ethers.getContract("MiningHill");
  const mspace = await hre.ethers.getContract("MSpaceToken");
  const factory = await hre.ethers.getContract("UniswapV2Factory");
  const bank = await hre.ethers.getContract("Bank");
  
  const {address} = await hre.deployments.deploy("BoringHelper", {
    from: deployer,
    args: [
      mining.address, 
      mspace.address, Tokens.META[network.chainId], Tokens.ETH[network.chainId],
      factory.address, bank.address
    ],
    log: true,
    deterministicDeployment: true,
  });

  if ([11, 12].includes(network.chainId)) {
    await verifyContractHelper({hre: hre, contractName: "BoringHelper", mainnet: network.chainId === 11});
  }
};

module.exports.tags = ['BoringHelper'];

module.exports.dependencies = ["MiningHill", "MSpaceToken", "Factory", "Bank", "Tokens"];