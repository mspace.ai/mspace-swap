const { verifyContractHelper } = require("../scripts/deployHelper");

module.exports = async (hre) => {
  const network = await hre.ethers.provider.getNetwork();
  const signers = await hre.ethers.getSigners();
  const accounts = await hre.getNamedAccounts();
  const deployer = accounts.ledger ?? signers[0].address;
  
  const {address} = await hre.deployments.deploy("Timelock", {
    from: deployer,
    args: [deployer, 172800],
    log: true,
    deterministicDeployment: true,
  });

  if ([11, 12].includes(network.chainId)) {
    await verifyContractHelper({hre: hre, contractName: "Timelock", mainnet: network.chainId === 11});
  }
};

module.exports.tags = ['Timelock'];