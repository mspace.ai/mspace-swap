const { ProxyArtifact } = require("../scripts/constants");
const { toBN, generateDecayPoints, currentBlock } = require("../scripts/utils");
const { verifyImplAndProxyHepler } = require("../scripts/deployHelper");

module.exports = async (hre) => {
  const network = await hre.ethers.provider.getNetwork();
  const signers = await hre.ethers.getSigners();
  const accounts = await hre.getNamedAccounts();
  const deployer = accounts.ledger ?? signers[0].address;

  const mspace = await hre.ethers.getContract("MSpaceToken");

  const numerator = toBN(process.env.DECAY_FORMULAR_NUMERATOR || 967);
  const denominator = toBN(process.env.DECAY_FORMULAR_DENOMINATOR || 1000);
  const startPoint = toBN(process.env.START_POINT_NUMBERATOR || 34).mul(toBN(1e18))
    .div(process.env.START_POINT_DENOMINATOR || 10);

  const latestBlock = await currentBlock();
  const blockPerMonth = process.env.BLOCK_PER_MONTH || (network.chainId === 11 ? 576000:2592000);
  const startReward = process.env.START_REWARD_BLOCK || (latestBlock+200);
  const initArgs = [
    process.env.OPERATION_WALLET || deployer,
    mspace.address,
    startReward, blockPerMonth
  ];

  const {address} = await hre.deployments.deploy("MiningHill", {
    from: deployer,
    log: true,
    proxy: {
      owner: deployer,
      execute: {
        init: {
          methodName: "init",
          args: initArgs,
        }
      },
      proxyContract: ProxyArtifact,
    },
  });

  const Mining = await hre.ethers.getContractFactory("MiningHill");
  const mining = Mining.attach(address);
  if ((await mining.strategyLength()) == 0) {
    const decayPoints = generateDecayPoints(startPoint, numerator, denominator, 47);;
    // await (await mining.setStrategy(decayPoints)).wait();
    await hre.deployments.execute("MiningHill", {from:deployer}, "setStrategy", decayPoints);
    console.log("setup strategy points for MiningHill");
  }

  // transfer owner mspace to mining
  if (await mspace.owner() !== address) {
    // await (await mspace.transferOwnership(address)).wait();
    await hre.deployments.execute("MSpaceToken", {from: deployer}, "transferOwnership", address);
    console.log("Transfer MSpace Ownership to MiningHill");
  }

  if ([11, 12].includes(network.chainId)) {
    await verifyImplAndProxyHepler({hre: hre, contractName: "MiningHill", mainnet: network.chainId === 11});
  }
};

module.exports.tags = ['MiningHill'];

module.exports.dependencies = ["MSpaceToken"];