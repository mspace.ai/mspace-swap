const { ENSRegistryArtifact, ENSRegistryWithFallbackArtifact } = require("../scripts/constants");
const { verifyContractHelper } = require("../scripts/deployHelper");

module.exports = async (hre) => {
  const network = await hre.ethers.provider.getNetwork();
  const signers = await hre.ethers.getSigners();
  const accounts = await hre.getNamedAccounts();
  const deployer = accounts.ledger ?? signers[0].address;

  const {address: ENSRegistryAddress} = await hre.deployments.deploy("ENSRegistry", {
    contract: ENSRegistryArtifact,
    from: deployer,
    log: true,
    deterministicDeployment: true,
  });

  const {address} = await hre.deployments.deploy("ENSRegistryWithFallback", {
    contract: ENSRegistryWithFallbackArtifact,
    from: deployer,
    args: [ENSRegistryAddress],
    log: true,
    deterministicDeployment: true,
  });

  // TODO verify this
  // if ([11, 12].includes(network.chainId)) {
  //   await verifyContractHelper({hre: hre, contractName: "ENSRegistry", mainnet: network.chainId === 11});
  //   await verifyContractHelper({hre: hre., contractName: "ENSRegistryWithFallback", mainnet: network.chainId === 11});
  // }
};

module.exports.tags = ['ENSRegistryWithFallback'];