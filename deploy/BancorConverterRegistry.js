const { verifyContractHelper } = require("../scripts/deployHelper");

module.exports = async (hre) => {
  const network = await hre.ethers.provider.getNetwork();
  const signers = await hre.ethers.getSigners();
  const accounts = await hre.getNamedAccounts();
  const deployer = accounts.ledger ?? signers[0].address;

  const {address} = await hre.deployments.deploy("BancorConverterRegistry", {
    from: deployer,
    log: true,
    deterministicDeployment: true,
  });

  if ([11, 12].includes(network.chainId)) {
    await verifyContractHelper({hre: hre, contractName: "BancorConverterRegistry", mainnet: network.chainId === 11});
  }
};

module.exports.tags = ['BancorConverterRegistry'];