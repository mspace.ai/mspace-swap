const { ProxyArtifact } = require("../scripts/constants");
const { verifyImplAndProxyHepler } = require("../scripts/deployHelper");

module.exports = async (hre) => {
  const network = await hre.ethers.provider.getNetwork();
  const signers = await hre.ethers.getSigners();
  const accounts = await hre.getNamedAccounts();
  const deployer = accounts.ledger ?? signers[0].address;

  const mspace = await hre.ethers.getContract("MSpaceToken");
  const initArgs = [mspace.address];

  const {address} = await hre.deployments.deploy("Bank", {
    from: deployer,
    log: true,
    proxy: {
      owner: deployer,
      execute: {
        init: {
          methodName: "init",
          args: initArgs,
        }
      },
      proxyContract: ProxyArtifact,
    },
  });

  if ([11, 12].includes(network.chainId)) {
    await verifyImplAndProxyHepler({hre:hre, contractName: "Bank", mainnet: network.chainId === 11});
  }
};

module.exports.tags = ['Bank'];

module.exports.dependencies = ["MSpaceToken"];