const { Tokens } = require("../scripts/constants");

module.exports = async (hre) => {
  const network = await hre.ethers.provider.getNetwork();
  const signers = await hre.ethers.getSigners();
  const accounts = await hre.getNamedAccounts();
  const deployer = accounts.ledger ?? signers[0].address;
  
  if (network.chainId === 31337) {
    const {address: weth} = await hre.deployments.deploy("WETHMock", {
      from: deployer,
      log: true,
    });
    Tokens.ETH[network.chainId] = weth;
    
    const {address: wmeta} = await hre.deployments.deploy("WMETAMock", {
      from: deployer,
      log: true,
    });
    Tokens.META[network.chainId] = wmeta;
  }
};

module.exports.tags = ['Tokens'];