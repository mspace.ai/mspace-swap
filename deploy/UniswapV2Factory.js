const { ProxyArtifact } = require("../scripts/constants");
const { verifyImplAndProxyHepler, verifyContractHelper } = require("../scripts/deployHelper");

module.exports = async (hre) => {
  const network = await hre.ethers.provider.getNetwork();
  const signers = await hre.ethers.getSigners();
  const accounts = await hre.getNamedAccounts();
  const deployer = accounts.ledger ?? signers[0].address;

  const {address: PairImplement} = await hre.deployments.deploy("UniswapV2Pair", {
    from: deployer,
    log: true,
    deterministicDeployment: true,
  });

  const {address} = await hre.deployments.deploy("UniswapV2Factory", {
    from: deployer,
    log: true,
    proxy: {
      owner: deployer,
      execute: {
        init: {
          methodName: "init",
          args: [PairImplement],
        }
      },
      proxyContract: ProxyArtifact,
    },
  });

  if ([11, 12].includes(network.chainId)) {
    await verifyContractHelper({hre: hre, contractName: "UniswapV2Pair", mainnet: network.chainId === 11});
    await verifyImplAndProxyHepler({hre: hre, contractName: "UniswapV2Factory", mainnet: network.chainId === 11});
  }
};

module.exports.tags = ['Factory'];