const { ProxyArtifact } = require("../scripts/constants");
const { toBN, currentBlock } = require("../scripts/utils");
const { verifyImplAndProxyHepler } = require("../scripts/deployHelper");

module.exports = async (hre) => {
  const network = await hre.ethers.provider.getNetwork();
  const signers = await hre.ethers.getSigners();
  const accounts = await hre.getNamedAccounts();
  const deployer = accounts.ledger ?? signers[0].address;

  const blockPerMonth = process.env.BLOCK_PER_MONTH ?? (network.chainId === 11 ? 576000:2592000);
  const blockPerYear = blockPerMonth*12;

  const currentBlockNumber = await currentBlock();

  const teamWallet = process.env.TEAM_WALLET ?? deployer.address;
  const teamVestPerYear = toBN(process.env.TEAM_DROP_PER_YEAR ?? "3125000").mul(toBN(10).pow(18));
  const teamVestStart = currentBlockNumber+blockPerYear;

  const airdropWallet = process.env.AIRDROP_WALLET ?? deployer.address;
  const airdropAmount = toBN(process.env.AIRDROP_AMOUNT ?? "12500000").mul(toBN(10).pow(18));
  const airdrop1st = currentBlockNumber+blockPerMonth*2;
  const airdrop2nd = currentBlockNumber+blockPerYear*2;

  const initArgs = [
    blockPerYear,

    teamWallet,
    teamVestPerYear,
    teamVestStart,

    airdropWallet,
    airdropAmount,
    airdrop1st,
    airdrop2nd
  ];

  const {address} = await hre.deployments.deploy("MSpaceToken", {
    from: deployer,
    log: true,
    proxy: {
      owner: deployer,
      execute: {
        init: {
          methodName: "init",
          args: initArgs,
        }
        // onUpgrade?: {
        //   methodName: string,
        //   args: any[]
        // }
      },
      proxyContract: ProxyArtifact,
    },
  });

  const treasuryWallet = process.env.TREASURY_WALLET ?? deployer.address;
  const mspace = await hre.ethers.getContract("MSpaceToken");
  const preMintBalance = await mspace.balanceOf(treasuryWallet);
  if ((await mspace.owner()) === deployer && preMintBalance.eq(0) ) {
    // pre-mint 15% 
    const preMintAmount = toBN(process.env.PRE_MINT_AMOUNT || "37500000").mul(toBN(10).pow(18));
    // await (await mspace.mint(treasuryWallet, preMintAmount)).wait();
    await hre.deployments.execute("MSpaceToken", {from:deployer}, "mint", treasuryWallet, preMintAmount);
    console.log("pre-mint fund for treasury-wallet", treasuryWallet);
  }

  if ([11, 12].includes(network.chainId)) {
    await verifyImplAndProxyHepler({hre: hre, contractName: "MSpaceToken", mainnet: network.chainId === 11});
  }
};

module.exports.tags = ['MSpaceToken'];