const { Tokens, ProxyArtifact } = require("../scripts/constants");
const { verifyImplAndProxyHepler } = require("../scripts/deployHelper");

module.exports = async (hre) => {
  const network = await hre.ethers.provider.getNetwork();
  const signers = await hre.ethers.getSigners();
  const accounts = await hre.getNamedAccounts();
  const deployer = accounts.ledger ?? signers[0].address;

  const factory = await hre.ethers.getContract("UniswapV2Factory");
  const mspace = await hre.ethers.getContract("MSpaceToken");
  const bank = await hre.ethers.getContract("Bank");
  const initArgs = [
    factory.address, bank.address, mspace.address, 
    Tokens.META[network.chainId]
  ];

  const {address} = await hre.deployments.deploy("Extractor", {
    from: deployer,
    log: true,
    proxy: {
      owner: deployer,
      execute: {
        init: {
          methodName: "init",
          args: initArgs,
        }
      },
      proxyContract: ProxyArtifact,
    },
  });

  if ((await factory.feeTo()) !== address) {
    // await (await factory.setFeeTo(address)).wait();
    await hre.deployments.execute("UniswapV2Factory", {from: deployer}, "setFeeTo", address);
    console.log("set Factory feeTo=Extractor");
  }

  if ([11, 12].includes(network.chainId)) {
    await verifyImplAndProxyHepler({hre: hre, contractName: "Extractor", mainnet: network.chainId === 11});
  }
};

module.exports.tags = ['Extractor'];

module.exports.dependencies = ["Factory", "Bank", "MSpaceToken", "Tokens"];