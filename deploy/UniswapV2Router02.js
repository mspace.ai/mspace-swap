const { Tokens } = require("../scripts/constants");
const { verifyContractHelper } = require("../scripts/deployHelper");

module.exports = async (hre) => {
  const network = await hre.ethers.provider.getNetwork();
  const signers = await hre.ethers.getSigners();
  const accounts = await hre.getNamedAccounts();
  const deployer = accounts.ledger ?? signers[0].address;

  const factory = await hre.ethers.getContract("UniswapV2Factory");

  const {address} = await hre.deployments.deploy("UniswapV2Router02", {
    from: deployer,
    args: [
      factory.address, 
      Tokens.META[network.chainId]
    ],
    log: true,
    deterministicDeployment: true,
  });

  if ([11, 12].includes(network.chainId)) {
    await verifyContractHelper({hre: hre, contractName: "UniswapV2Router02", mainnet: network.chainId === 11});
  }
};

module.exports.tags = ['Router'];

module.exports.dependencies = ["Factory", "Tokens"];