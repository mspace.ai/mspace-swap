# 1. Swap contracts

Folder `contracts/uniswapv2` fork from [uniswapv2](https://github.com/Uniswap/v2-core). Provide liquidity for earn 0.25% for each swap.

# 2. Stake lv1

Folder `contracts/stake` fork from [sushiswap](https://github.com/sushiswap/sushiswap)

When LPs deposit their MP tokens into `MiningHill`, overtime they will receive MSP tokens for reward. The reward will follow 3 phrases:
 - Phrase `prelaunch`: in 2 weeks (401526 blocks), `MiningHill` will mint `X1` mstar tokens when a block passed.
 - Phrase `bootstrap`: in 2 weeks (401526 blocks), `MiningHill` will mint `X2` mstar tokens when a block passed.
 - Phrase `decay`: `MiningHill` will mint MStar tokens when a blocks passed follow the `Decay Formula`. Each tick of formula is 1 month (803053 blocks).

## 2.1. Decay Formula: reward/block = a*b^x

Eg: with a=11, b=0.953
| Time | x | reward/block | MStar supplied |
|---|---|---|---|
| 2021-12-01 | 0 | 11 | 8,833,581 |
| 2022-01-01 | 1 | 10.483 | 8,418,403 |
| 2022-01-01 | 2 | 9.990299 | 8,022,738 |
| 2022-01-01 | 3 | 9.520754947 | 7,645,669 |
| ... | ... | ... | ... |
| 2022-01-01 | 46 | 1.201340489 | 964,740 |

## 2.2. Distribution strategy

 - For each pool: each block reward will split in to pools base on allocPoint of each pool.
 - For each user in a pool: the reward split base on poolShare of an user (`poolShare = userAmount * blocksPassed`)

Eg: 

 - User A deposit 100 LP tokens into `MiningHill` from block 10 -> 15
 - User B deposit 50 LP tokens into `MiningHill` from block 12 -> 15

=> so user A poolShare = 100*(15-10) = 500, user B poolShare = 50*(15-12) = 150, totalShare = 650

If each block the pool got 1 MStar (decimal is 18) for reward. Then from block 10 -> 15, the pool got 5 MStar. So user A got 5\*500/650 = 3.846 MStar, user B got 5\*150/650 = 1.154 MStar

# 3. Stake lv2

Swap protocol fee 0.05% will be collected into `Interstellar`. The contract will convert them to MSP & send to `SpaceCenter`. Stake MSP tokens into SpaceCenter to earn extra MSP after each swap. 

# 3. Run test

```
npx hardhat flatten
npx hardhat test
```

# 4. Deploy

Copy `.env.example` to `.env` & edit 

```
npx hardhat --network metadium deploy
```