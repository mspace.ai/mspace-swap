const { expect } = require("chai");
const { ethers } = require("hardhat");

describe("Bank", async function () {
  before(async function() {
    this.Bank = await ethers.getContractFactory("Bank");
    this.ERC20Mock = await ethers.getContractFactory("ERC20Mock");
    this.signers = await ethers.getSigners();
    this.admin = this.signers[0];
    this.dev = this.signers[1];
    this.lp1 = this.signers[2];
    this.lp2 = this.signers[3];
    this.trader1 = this.signers[4];
    this.trader2 = this.signers[5];
  });

  beforeEach(async function() {
    this.mspace = await this.ERC20Mock.deploy("MSP Token", "MSP", 18);
    await this.mspace.deployed();
    this.bank = await this.Bank.deploy();
    await this.bank.deployed();
    await this.bank.init(this.mspace.address);
  });

  it("enter & leave", async function() {
    await this.mspace.mint(this.lp1.address, 1000);
    await this.mspace.connect(this.lp1).approve(this.bank.address, 300);
    await this.bank.connect(this.lp1).enter(300);
    expect(await this.mspace.balanceOf(this.lp1.address)).to.equal(700);
    expect(await this.mspace.balanceOf(this.bank.address)).to.equal(300);
    expect(await this.bank.balanceOf(this.lp1.address)).to.equal(300);

    await this.bank.connect(this.lp1).leave(300);
    expect(await this.mspace.balanceOf(this.lp1.address)).to.equal(1000);
    expect(await this.mspace.balanceOf(this.bank.address)).to.equal(0);
    expect(await this.bank.balanceOf(this.lp1.address)).to.equal(0);
  });
});