const { time } = require("@openzeppelin/test-helpers");
const { expect } = require("chai");
const { advanceTime, advanceBlock } = require("../scripts/utils");

ethers.utils.Logger.setLogLevel('off');

describe("Utils time travel for unit tests", async () => {
  const duration = 600;
  const targetBlock = 10;
  // const targetBlock = 10000;

  const currentTime = async () => {
    return (await time.latest()).toNumber();
  }
  const currentBlock = async () => {
    return (await time.latestBlock()).toNumber();
  }

  it("time travel duration", async () => {   
    const passBlock = await currentBlock();
    const passTime = await currentTime();
    await advanceTime(duration);
    const nowBlock = await currentBlock();
    const nowTime = await currentTime();
    expect(nowBlock - passBlock).to.equal(1);
    expect(nowTime - passTime).to.gte(duration);
  });

  it("time travel block", async () => {
    const passBlock = await currentBlock();
    await advanceBlock(targetBlock);
    const nowBlock = await currentBlock();
    expect(nowBlock - passBlock).to.equal(targetBlock);
  });
});