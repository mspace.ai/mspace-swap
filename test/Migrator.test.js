const { expect } = require("chai");
const { ethers } = require("hardhat");
const { currentBlock } = require("../scripts/utils");

describe("Migrator", async function () {
  before(async function() {
    this.ERC20 = await ethers.getContractFactory("ERC20Mock");
    this.Mining = await ethers.getContractFactory("MiningHill");
    this.MiningPoolMigrator = await ethers.getContractFactory("MiningPoolMigrator");

    const WMETA = await ethers.getContractFactory("WMETAMock");
    this.wmeta = await WMETA.deploy();
    await this.wmeta.deployed();

    this.Router = await ethers.getContractFactory("UniswapV2Router02");
    this.Factory = await ethers.getContractFactory("UniswapV2Factory");
    this.Pair = await ethers.getContractFactory("UniswapV2Pair");
    this.pairImpl = await this.Pair.deploy();
    await this.pairImpl.deployed();

    this.signers = await ethers.getSigners();
    this.admin = this.signers[0];
    this.dev = this.signers[1];
    this.lp1 = this.signers[2];
    this.lp2 = this.signers[3];
  });

  async function addLiquidity(user, router, tk1, tk1Amount, tk2, tk2Amount) {
    await tk1.mint(user.address, tk1Amount);
    await tk1.connect(user).approve(router.address, tk1Amount);
    await tk2.mint(user.address, tk2Amount);
    await tk2.connect(user).approve(router.address, tk2Amount);
    await router.connect(user).addLiquidity(
      tk1.address, tk2.address,
      tk1Amount, tk2Amount, 0, 0,
      user.address,
      Math.floor(new Date().getTime()/1000) + 3600
    );
  }

  it("migrate pools", async function() {
    const latestBlock = await currentBlock();
    const mining = await this.Mining.deploy();
    await mining.deployed();
    const msp = await this.ERC20.deploy("MSP", "MSpace Token", 18);
    await msp.deployed();
    await mining.init(this.dev.address, msp.address, latestBlock+50, 10);

    const token1 = await this.ERC20.deploy("TK1", "TK1 oken", 18);
    await token1.deployed();
    const token2 = await this.ERC20.deploy("TK2", "TK2 oken", 18);
    await token2.deployed();
    const token3 = await this.ERC20.deploy("TK3", "TK3 oken", 18);
    await token3.deployed();

    const factory = await this.Factory.deploy();
    await factory.deployed();
    await factory.init(this.pairImpl.address);

    const router = await this.Router.deploy(factory.address, this.wmeta.address);
    await router.deployed();

    await addLiquidity(this.lp1, router, token1, 1000000, token2, 1000000);
    await addLiquidity(this.lp2, router, token1, 1000000, token3, 1000000);
    const pair1 = this.Pair.attach(await factory.getPair(token1.address, token2.address));
    const pair2 = this.Pair.attach(await factory.getPair(token1.address, token3.address));

    await mining.add(10, pair1.address, false);
    await pair1.connect(this.lp1).approve(mining.address, await pair1.balanceOf(this.lp1.address));
    await mining.connect(this.lp1).deposit(0, await pair1.balanceOf(this.lp1.address));

    await mining.add(5, pair2.address, false);
    await pair2.connect(this.lp2).approve(mining.address, await pair2.balanceOf(this.lp2.address));
    await mining.connect(this.lp2).deposit(1, await pair2.balanceOf(this.lp2.address));

    const factory2 = await this.Factory.deploy();
    await factory2.deployed();
    await factory2.init(this.pairImpl.address);

    const router2 = await this.Router.deploy(factory2.address, this.wmeta.address);
    await router2.deployed();

    await addLiquidity(this.lp1, router2, token1, 1000000, token2, 1000000);
    await addLiquidity(this.lp2, router2, token1, 1000000, token3, 1000000);
    const newPair1 = this.Pair.attach(await factory2.getPair(token1.address, token2.address));
    const newPair2 = this.Pair.attach(await factory2.getPair(token1.address, token3.address));

    const migrator = await this.MiningPoolMigrator.deploy(
      mining.address, 
      factory.address, factory2.address, 
      0
    );
    await migrator.deployed();
    await expect(
      mining.connect(this.dev).setMigrator(migrator.address)
    ).to.be.revertedWith("Ownable: caller is not the owner");
    await mining.setMigrator(migrator.address);
    expect(await mining.migrator()).to.equal(migrator.address);

    expect(await pair1.balanceOf(mining.address)).to.gt(0);
    expect(await newPair1.balanceOf(mining.address)).to.equal(0);
    expect((await mining.poolInfo(0)).lpToken).to.equal(pair1.address);
    await mining.migrate(0);
    expect(await pair1.balanceOf(mining.address)).to.equal(0);
    expect(await newPair1.balanceOf(mining.address)).to.gt(0);
    expect((await mining.poolInfo(0)).lpToken).to.equal(newPair1.address);

    await expect(mining.migrate(0)).to.be.revertedWith("not from old factory");

    expect(await pair2.balanceOf(mining.address)).to.gt(0);
    expect(await newPair2.balanceOf(mining.address)).to.equal(0);
    expect((await mining.poolInfo(1)).lpToken).to.equal(pair2.address);
    await mining.migrate(1);
    expect(await pair2.balanceOf(mining.address)).to.equal(0);
    expect(await newPair2.balanceOf(mining.address)).to.gt(0);
    expect((await mining.poolInfo(1)).lpToken).to.equal(newPair2.address);

    // TODO need verify reserves
  });

  // do not need
  // it("migrate pairs", async function() {});
});