const { expect } = require("chai");
const { ethers } = require("hardhat");
const { toBN, advanceBlock, currentBlock } = require("../scripts/utils");

describe("Multicall", async function() {
  before(async function() {
    this.Multicall = await ethers.getContractFactory("Multicall2");
    this.ERC20 = await ethers.getContractFactory("ERC20Mock");
    this.WMETA = await ethers.getContractFactory("WMETAMock");

    const signers = await ethers.getSigners();
    this.admin = signers[0];
    this.user = signers[1];
  });

  beforeEach(async function() {
    this.multicall = await this.Multicall.deploy();
    await this.multicall.deployed();

    this.token1 = await this.ERC20.deploy("TK1", "token1", 18);
    await this.token1.deployed();
    await this.token1.mint(this.user.address, 100000);

    this.token2 = await this.ERC20.deploy("TK2", "token2", 18);
    await this.token2.deployed();
    await this.token2.mint(this.user.address, 200000);

    this.wmeta = await this.WMETA.deploy();
    await this.wmeta.mint(this.user.address, 300000);
  });

  it("aggregate", async function() {
    const tokens = [this.token1.address, this.token2.address, this.wmeta.address];
    const calls = [];
    const fragment = this.ERC20.interface.getFunction("balanceOf");
    for (let i=0; i< tokens.length; i++) {
      const callData = this.ERC20.interface.encodeFunctionData(fragment, [this.user.address]);
      const address = tokens[i];
      calls.push([address, callData]);
    }
    // console.log(calls);
    const blockNumber = await currentBlock();
    const results = await this.multicall.callStatic.aggregate(calls);
    expect(results.hash).to.equal(undefined);
    expect(results.blockNumber).to.equal(blockNumber);
    expect(results.returnData).to.not.equal(undefined);
    expect(results.returnData.length).to.equal(3);

    const call1Result = this.ERC20.interface.decodeFunctionResult(fragment, results.returnData[0]);
    expect(call1Result[0]).to.equal(await this.token1.balanceOf(this.user.address));

    const call2Result = this.ERC20.interface.decodeFunctionResult(fragment, results.returnData[1]);
    expect(call2Result[0]).to.equal(await this.token2.balanceOf(this.user.address));

    const call3Result = this.ERC20.interface.decodeFunctionResult(fragment, results.returnData[2]);
    expect(call3Result[0]).to.equal(await this.wmeta.balanceOf(this.user.address));
  });

  it("WMETA details", async function() {
    const calls = [];
    const fragments = [
      this.ERC20.interface.getFunction("name"),
      this.ERC20.interface.getFunction("symbol"),
      this.ERC20.interface.getFunction("decimals"),
    ];
    for (let i=0; i< fragments.length; i++) {
      const callData = this.ERC20.interface.encodeFunctionData(fragments[i], []);
      calls.push([this.wmeta.address, callData]);
    }
    const results = await this.multicall.callStatic.aggregate(calls);

    const name = this.ERC20.interface.decodeFunctionResult(fragments[0], results.returnData[0])[0];
    expect(name).to.equal(await this.wmeta.name());

    const symbol = this.ERC20.interface.decodeFunctionResult(fragments[1], results.returnData[1])[0];
    expect(symbol).to.equal(await this.wmeta.symbol());

    const decimals = this.ERC20.interface.decodeFunctionResult(fragments[2], results.returnData[2])[0];
    expect(decimals).to.equal(await this.wmeta.decimals());
  });
});