const { expect } = require("chai");
const { ethers } = require("hardhat");
const { toBN, advanceBlock, currentBlock } = require("../scripts/utils");

describe("MSpaceToken", async function () {
  before(async function() {
    this.MSpaceToken = await ethers.getContractFactory("MSpaceToken");
    this.signers = await ethers.getSigners();
    this.admin = this.signers[0];
    this.team = this.signers[1];
    this.airdrop = this.signers[2];

    this.blockPerYear = 10;
    this.tenPercentSupply = "25000000";
    this.teamVestPerYear = toBN(this.tenPercentSupply).mul(toBN(10).pow(18)).div(4);
    this.airdropAmount = toBN(this.tenPercentSupply).mul(toBN(10).pow(18)).div(2);
  });

  beforeEach(async function() {
    this.mspace = await this.MSpaceToken.deploy();
    await this.mspace.deployed();
    const currentBlockNumber = await currentBlock();
    await this.mspace.init(
      this.blockPerYear,

      this.team.address,
      this.teamVestPerYear,
      currentBlockNumber+1,

      this.airdrop.address,
      this.airdropAmount,
      currentBlockNumber+10,
      currentBlockNumber+20
    );
  });

  it("init twice", async function() {
    const currentBlockNumber = await currentBlock();
    await expect(this.mspace.init(
      this.blockPerYear,

      this.team.address,
      this.teamVestPerYear,
      currentBlockNumber+1,

      this.airdrop.address,
      this.airdropAmount,
      currentBlockNumber+10,
      currentBlockNumber+20
    )).to.be.revertedWith("Initializable: contract is already initialized");
  });

  it("info", async function() {
    expect(await this.mspace.name()).to.equal("MSP Token");
    expect(await this.mspace.symbol()).to.equal("MSP");
    expect(await this.mspace.decimals()).to.equal(18);
  })

  it("teamVesting", async function() {
    await expect(this.mspace.connect(this.team).teamVesting())
      .to.be.revertedWith("Ownable: caller is not the owner"); 

    await expect(this.mspace.teamVesting()).to.be.revertedWith("MSP: not in time");
    await advanceBlock(this.blockPerYear);
    await this.mspace.teamVesting();
    let balance = await this.mspace.balanceOf(this.team.address);
    expect(balance).to.equal(this.teamVestPerYear);

    await advanceBlock(this.blockPerYear*2);
    await this.mspace.teamVesting();
    balance = await this.mspace.balanceOf(this.team.address);
    expect(balance).to.equal(this.teamVestPerYear.mul(3));

    await advanceBlock(this.blockPerYear*10);
    await this.mspace.teamVesting();
    balance = await this.mspace.balanceOf(this.team.address);
    expect(balance).to.equal(this.teamVestPerYear.mul(4));

    await advanceBlock(this.blockPerYear);
    await expect(this.mspace.teamVesting()).to.be.revertedWith("MSP: vesting done");
  });

  it("teamVesting all at once", async function() {
    await advanceBlock(this.blockPerYear*10);
    await this.mspace.teamVesting();
    let balance = await this.mspace.balanceOf(this.team.address);
    expect(balance).to.equal(this.teamVestPerYear.mul(4));

    await expect(this.mspace.teamVesting()).to.be.revertedWith("MSP: vesting done");
  });

  it("airdrop", async function() {
    await expect(this.mspace.connect(this.team).airdrop1())
      .to.be.revertedWith("Ownable: caller is not the owner");
    await expect(this.mspace.connect(this.team).airdrop2())
      .to.be.revertedWith("Ownable: caller is not the owner");

    await advanceBlock(10);
    await this.mspace.airdrop1();
    let balance = await this.mspace.balanceOf(this.airdrop.address);
    expect(balance).to.equal(this.airdropAmount.mul(1));

    await advanceBlock(10);
    await this.mspace.airdrop2();
    balance = await this.mspace.balanceOf(this.airdrop.address);
    expect(balance).to.equal(this.airdropAmount.mul(2));
  });

  it("setAirdropWallet", async function() {
    await expect(this.mspace.connect(this.team).setAirdropWallet(this.admin.address))
      .to.be.revertedWith("Ownable: caller is not the owner");
    
    expect(await this.mspace.getAddress("airdropWallet")).to.equal(this.airdrop.address);
    await this.mspace.setAirdropWallet(this.admin.address);
    expect(await this.mspace.getAddress("airdropWallet")).to.equal(this.admin.address);
  });

  it("set eternal storage", async function() {
    expect(this.mspace.setAddress).to.equal(undefined);
  });

  it("setTeamWallet", async function() {
    await expect(this.mspace.connect(this.team).setTeamWallet(this.admin.address))
      .to.be.revertedWith("Ownable: caller is not the owner");

    expect(await this.mspace.getAddress("teamWallet")).to.equal(this.team.address);
    await this.mspace.setTeamWallet(this.admin.address);
    expect(await this.mspace.getAddress("teamWallet")).to.equal(this.admin.address);
  });
});