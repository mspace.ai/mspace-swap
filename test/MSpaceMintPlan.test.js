const { expect } = require("chai");
const { ethers } = require("hardhat");
const { toBN, generateDecayPoints, advanceBlock, currentBlock } = require("../scripts/utils");


describe("MSpaceMintPlan", async function () {
  const numerator = toBN(953);
  const denominator = toBN(1000);
  const startPoint = toBN(34).mul(toBN(1e18)).div(10);
  const decayPoints = generateDecayPoints(startPoint, numerator, denominator, 47);

  before(async function() {
    this.MSpaceMintPlan = await ethers.getContractFactory("MintPlanMock");
    this.signers = await ethers.getSigners();
    this.admin = this.signers[0];
    this.lp1 = this.signers[2];
  });

  async function setUp(runner, startReward, blockStep, strategyPoints) {
    runner.plan = await runner.MSpaceMintPlan.deploy();
    await runner.plan.deployed();
    await runner.plan.init(startReward, blockStep);
    await runner.plan.setStrategy(strategyPoints);
  }

  async function defaultSetup(runner) {
    const latestBlock = await currentBlock();
    const blockPerMonth = 10;
    const startReward = latestBlock+50;
    // set up strategy points
    const prelaunchReward = toBN(21250000).mul(toBN(1e18)).div(Math.floor(blockPerMonth/2));
    const bootstrapReward = toBN(30).mul(toBN(1e18));
    const strategyPoints = [prelaunchReward, bootstrapReward].concat(decayPoints.map(p=> [p,p]).flat());
    await setUp(runner, startReward, blockPerMonth/2, strategyPoints);
  }

  beforeEach(async function() {
    await defaultSetup(this);
  });

  it("mspacePerBlock", async function() {
    let mspacePerBlock = await this.plan.mspacePerBlock();
    expect(mspacePerBlock).to.equal(0);

    const startReward = await this.plan.getUint("startReward");
    let latestBlock = await currentBlock();
    await advanceBlock(startReward.sub(latestBlock).add(2));
    mspacePerBlock = await this.plan.mspacePerBlock();
    expect(mspacePerBlock).to.gt(0);
    expect(mspacePerBlock).to.equal(await this.plan.rewardStrategy(0));

    const blockStep = await this.plan.getUint("blockStep");
    await advanceBlock(blockStep);
    mspacePerBlock = await this.plan.mspacePerBlock();
    expect(mspacePerBlock).to.gt(0);
    expect(mspacePerBlock).to.equal(await this.plan.rewardStrategy(1));

    const length = await this.plan.strategyLength();
    latestBlock = await currentBlock();
    await advanceBlock(toBN(latestBlock).sub(startReward).add(blockStep*length));
    mspacePerBlock = await this.plan.mspacePerBlock();
    expect(mspacePerBlock).to.equal(0);
  });

  it("setFormula not owner", async function() {
    await expect(this.plan.connect(this.lp1).setStrategy(decayPoints))
      .to.be.revertedWith("Ownable: caller is not the owner");
  });

  it("strategy points", async function() {
    let length = await this.plan.strategyLength();
    expect(length).to.equal(2+decayPoints.length*2);
    let point = await this.plan.rewardStrategy(2);
    expect(point).to.equal(decayPoints[0]);
    point = await this.plan.rewardStrategy((2+decayPoints.length*2)-1);
    expect(point).to.equal(decayPoints[decayPoints.length-1]);

    // setStrategyPoint
    length = await this.plan.strategyLength();
    const index = Math.floor(Math.random()*length);
    await this.plan.setStrategyPoint(2, 100);
    point = await this.plan.rewardStrategy(2);
    expect(point).to.equal(100);

    length = await this.plan.strategyLength();
    expect(length).to.equal(2+decayPoints.length*2);
  });

  it("calcReward", async function() {
    const latestBlock = await currentBlock();
    const blockPerMonth = 803054;
    const prelaunch = latestBlock+50;
    const blockStep = Math.round(blockPerMonth/2);
    const prelaunchReward = toBN(21250000).mul(toBN(1e18)).div(blockStep);
    const bootstrapReward = toBN(30).mul(toBN(1e18));
    const startBootstrap = prelaunch+blockStep;
    const startFormula = startBootstrap+blockStep;
    const endReward = startFormula+11*blockPerMonth+3*12*blockPerMonth;
    // repeat decayPoints because blockstep = 1/2 blockPerMonth
    const strategyPoints = [prelaunchReward, bootstrapReward].concat(decayPoints.map(p=> [p,p]).flat());
    await setUp(this, prelaunch, blockStep, strategyPoints);

    let reward = await this.plan.calcReward(prelaunch-10, prelaunch);
    expect(reward).to.equal(0);
    reward = await this.plan.calcReward(endReward, endReward+10);
    expect(reward).to.equal(0);
    reward = await this.plan.calcReward(endReward+blockPerMonth+10, endReward+blockPerMonth+20);
    expect(reward).to.equal(0);
    reward = await this.plan.calcReward(prelaunch, prelaunch+10);
    expect(reward).to.equal((await this.plan.rewardStrategy(0)).mul(10));
    reward = await this.plan.calcReward(startBootstrap, startBootstrap+10);
    expect(reward).to.equal((await this.plan.rewardStrategy(1)).mul(10));
    reward = await this.plan.calcReward(startFormula, startFormula+10);
    expect(reward).to.equal(decayPoints[0].mul(10));
    reward = await this.plan.calcReward(startFormula+blockPerMonth, startFormula+blockPerMonth+10);
    expect(reward).to.equal(decayPoints[1].mul(10));

    reward = await this.plan.calcReward(startBootstrap-1, startBootstrap+1);
    expect(reward).to.equal((await this.plan.rewardStrategy(0)).add(await this.plan.rewardStrategy(1)));
    reward = await this.plan.calcReward(startFormula-1, startFormula+1);
    expect(reward).to.equal((await this.plan.rewardStrategy(1)).add(decayPoints[0]));

    reward = await this.plan.calcReward(prelaunch, startBootstrap);
    expect(reward).to.equal(prelaunchReward.mul(toBN(blockStep)));

    // 12045791
    reward = await this.plan.calcReward(startBootstrap, startFormula);
    expect(reward).to.equal(bootstrapReward.mul(toBN(blockStep)));

    // decayPoints.map(p=> [p,p]).flat().map(p => p.mul(blockStep)).reduce((a,b)=>a.add(b))
    reward = await this.plan.calcReward(startFormula, endReward);
    expect(reward).to
      .gt(toBN(52046500).mul(toBN(1e18)))
      .lt(toBN(52047500).mul(toBN(1e18)));

    // decayPoints.map(p=> [p,p]).flat().map(p => p.mul(blockStep)).reduce((a,b)=>a.add(b))
    //   .add(prelaunchReward.mul(blockStep))
    //   .add(bootstrapReward.mul(blockStep))
    reward = await this.plan.calcReward(prelaunch, endReward);
    expect(reward).to
      .gt(toBN(85342300).mul(toBN(1e18)))
      .lt(toBN(85343300).mul(toBN(1e18)));
  });
});