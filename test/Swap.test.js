const { expect } = require("chai");
const { ethers } = require("hardhat");
const { ProxyArtifact } = require("../scripts/constants");

describe("Swap", async function () {
  before(async function() {
    this.Factory = await ethers.getContractFactory("UniswapV2Factory");
    this.Pair = await ethers.getContractFactory("UniswapV2Pair");
    this.pairImplement = await this.Pair.deploy();
    await this.pairImplement.deployed();
    this.Router = await ethers.getContractFactory("UniswapV2Router02");
    this.ERC20Mock = await ethers.getContractFactory("ERC20Mock");
    this.WMETAMock = await ethers.getContractFactory("WMETAMock");
    this.signers = await ethers.getSigners();
    this.admin = this.signers[0];
    this.dev = this.signers[1];
    this.lp1 = this.signers[2];
    this.lp2 = this.signers[3];
    this.trader1 = this.signers[4];
    this.trader2 = this.signers[5];
  });

  beforeEach(async function() {
    this.wmeta = await this.WMETAMock.deploy();
    await this.wmeta.deployed();
    this.token1 = await this.ERC20Mock.deploy("token 1", "TK1", 18);
    await this.token1.deployed();
    this.token2 = await this.ERC20Mock.deploy("token 2", "TK1", 18);
    await this.token2.deployed();

    this.factory = await this.Factory.deploy();
    await this.factory.deployed();
    await this.factory.init(this.pairImplement.address);
    // address receive protocol fee 0.3%
    await this.factory.setFeeTo(this.dev.address);
    this.router = await this.Router.deploy(this.factory.address, this.wmeta.address);
    await this.router.deployed();
  });

  async function addLiquidity(runner, user, tk1, tk2){
    await runner.token1.mint(user.address, tk1);
    await runner.token1.connect(user).approve(runner.router.address, tk1);
    await runner.token2.mint(user.address, tk2);
    await runner.token2.connect(user).approve(runner.router.address, tk2);

    await runner.router.connect(user).addLiquidity(
      runner.token1.address, runner.token2.address,
      tk1, tk2, 0, 0,
      user.address,
      Math.floor(new Date().getTime()/1000) + 3600
    );
  }

  async function swapTk1(runner, user, tk1) {
    await runner.token1.mint(user.address, tk1);
    await runner.token1.connect(user).approve(runner.router.address, tk1);

    return runner.router.connect(user).swapExactTokensForTokens(
      tk1, 0, [runner.token1.address, runner.token2.address],
      user.address,
      Math.floor(new Date().getTime()/1000) + 3600
    );
  }

  it("Factory", async function() {
    await this.factory.createPair(this.token1.address, this.token2.address);
    const pairAddress = await this.factory.getPair(this.token1.address, this.token2.address);
    expect(pairAddress).to.not.equal(ethers.constants.AddressZero);
    const pair = await this.Pair.attach(pairAddress);
    await pair.balanceOf(this.admin.address);
    // console.log(await this.factory.pairCodeHash());
  });

  it("addLiquidity", async function() {
    // first liquidity of pair must be gte 1e3 (sqrt(tk1*tk2) > 1e3)
    const tk1 = 10000;
    const tk2 = 5000;
    await addLiquidity(this, this.lp1, tk1, tk2);

    const pairAddr = await this.factory.getPair(this.token1.address, this.token2.address);
    expect(pairAddr).to.not.equal(ethers.constants.AddressZero);
    expect(await this.token1.balanceOf(pairAddr)).to.equal(tk1);
    expect(await this.token2.balanceOf(pairAddr)).to.equal(tk2);
    const pair = await this.Pair.attach(pairAddr);
    expect(await pair.balanceOf(this.lp1.address)).to.equal(Math.floor(Math.sqrt(tk1*tk2)-1e3));
  });

  async function addLiquidityMeta(runner, user, meta, tk) {
    await runner.token2.mint(user.address, tk);
    await runner.token2.connect(user).approve(runner.router.address, tk);

    await runner.router.connect(user).addLiquidityMETA(
      runner.token2.address,
      tk, 0, 0,
      user.address,
      Math.floor(new Date().getTime()/1000) + 3600,
      {value: meta}
    );
    expect(await runner.wmeta.balanceOf(user.address)).to.equal(0);
    expect(await runner.token2.balanceOf(user.address)).to.equal(0);
  }

  it("addLiquidityMETA", async function() {
    const tk1 = 10000;
    const tk2 = 5000;
    await addLiquidityMeta(this, this.lp1, tk1, tk2)

    const pairAddr = await this.factory.getPair(this.wmeta.address, this.token2.address);
    expect(pairAddr).to.not.equal(ethers.constants.AddressZero);
    expect(await this.wmeta.balanceOf(pairAddr)).to.equal(tk1);
    expect(await this.token2.balanceOf(pairAddr)).to.equal(tk2);
    const pair = await this.Pair.attach(pairAddr);
    expect(await pair.balanceOf(this.lp1.address)).to.equal(Math.floor(Math.sqrt(tk1*tk2)-1e3));
  });

  it("removeLiquidity", async function() {
    const tk1 = 10000;
    const tk2 = 5000;
    await addLiquidity(this, this.lp1, tk1, tk2);
    
    const pairAddr = await this.factory.getPair(this.token1.address, this.token2.address);
    expect(await this.token1.balanceOf(this.lp1.address)).to.equal(0);
    expect(await this.token2.balanceOf(this.lp1.address)).to.equal(0);
    const pair = await this.Pair.attach(pairAddr);
    const liquidity = await pair.balanceOf(this.lp1.address);
    expect(liquidity).to.gt(0);
    
    await pair.connect(this.lp1).approve(this.router.address, liquidity);
    const totalLiquidity = await pair.totalSupply();
    expect(liquidity.add(1e3)).to.equal(totalLiquidity);
    await this.router.connect(this.lp1).removeLiquidity(
      this.token1.address, this.token2.address,
      liquidity, 0, 0,
      this.lp1.address,
      Math.floor(new Date().getTime()/1000) + 3600
    );
    expect(await this.token1.balanceOf(this.lp1.address))
      .to.equal(liquidity.mul(tk1).div(totalLiquidity));
    expect(await this.token2.balanceOf(this.lp1.address))
      .to.equal(liquidity.mul(tk2).div(totalLiquidity));
    expect(await pair.balanceOf(this.lp1.address)).to.equal(0);
  });

  it("removeLiquidityMeta", async function() {
    const tk1 = 10000;
    const tk2 = 5000;
    await addLiquidityMeta(this, this.lp1, tk1, tk2)

    const pairAddr = await this.factory.getPair(this.wmeta.address, this.token2.address);
    expect(await this.wmeta.balanceOf(this.lp1.address)).to.equal(0);
    expect(await this.token2.balanceOf(this.lp1.address)).to.equal(0);
    const pair = await this.Pair.attach(pairAddr);
    const liquidity = await pair.balanceOf(this.lp1.address);
    expect(liquidity).to.gt(0);
    
    await pair.connect(this.lp1).approve(this.router.address, liquidity);
    const totalLiquidity = await pair.totalSupply();
    expect(liquidity.add(1e3)).to.equal(totalLiquidity);
    const oldLp1MetaBalance = await ethers.provider.getBalance(this.lp1.address);
    const tx = await (await this.router.connect(this.lp1).removeLiquidityMETA(
      this.token2.address,
      liquidity, 0, 0,
      this.lp1.address,
      Math.floor(new Date().getTime()/1000) + 3600
    )).wait();
    const newLp1MetaBalance = await ethers.provider.getBalance(this.lp1.address);
    expect(
      newLp1MetaBalance
        .add(tx.gasUsed.mul(tx.effectiveGasPrice))
        .sub(oldLp1MetaBalance)
    ).to.equal(liquidity.mul(tk1).div(totalLiquidity));
    expect(await this.token2.balanceOf(this.lp1.address))
      .to.equal(liquidity.mul(tk2).div(totalLiquidity));
    expect(await pair.balanceOf(this.lp1.address)).to.equal(0);
  });

  it("swapExactTokensForTokens", async function() {
    const tk1 = 10000000;
    const tk2 = 50000;
    await addLiquidity(this, this.lp1, tk1, tk2);
    
    const inTk = 300000;
    await swapTk1(this, this.trader1, inTk);
    expect(await this.token1.balanceOf(this.trader1.address)).to.equal(0);
    // keep liquidity is the same tk1*tk2=(tk1+a)(tk2-b) => b=a*tk2/(tk1+a)
    // a=inTk*0.997 => 0.3% is protocol fee
    expect(await this.token2.balanceOf(this.trader1.address)).to.equal(
      Math.floor(inTk*0.997*tk2/(tk1+inTk*0.997))
    );

    const pairAddr = await this.factory.getPair(this.token1.address, this.token2.address);
    const pair = await this.Pair.attach(pairAddr);
    
    // mintFee is only called when liquidity is changed
    expect(await pair.balanceOf(this.dev.address)).to.eq(0);
    await addLiquidity(this, this.lp2, tk1, tk2);
    const liquidity = await pair.balanceOf(this.lp2.address);
    await pair.connect(this.lp2).approve(this.router.address, liquidity);
    await this.router.connect(this.lp2).removeLiquidity(
      this.token1.address, this.token2.address,
      liquidity, 0, 0,
      this.lp2.address,
      Math.floor(new Date().getTime()/1000) + 3600
    );
    expect(await pair.balanceOf(this.dev.address)).to.gt(0);
    // 51 (1/6 of protocol fee ~ 0.05%)
    // fee = sqrt(inTk*(inTk*tk2/(tk1+inTk)))*0.0005/2
    const fee = Math.floor(Math.sqrt(inTk*(inTk*tk2/(tk1+inTk)))*0.0005/2);
    expect(await pair.balanceOf(this.dev.address)).to.gte(fee-1).lt(fee+1);
  });

  it("upgradePair", async function(){
    await addLiquidity(this, this.lp1, 100000, 100000);
    const pairAddress = await this.factory.getPair(this.token1.address, this.token2.address);
    await expect(
      this.factory.upgradePair(this.token1.address, this.token2.address)
    ).to.be.revertedWith("UniswapV2: UPGRADE_ERROR");

    const PairMock = await ethers.getContractFactory("PairMockV2000");
    const pairImplement = await PairMock.deploy();
    await pairImplement.deployed();

    const Proxy = await ethers.getContractFactory(ProxyArtifact);
    const pairProxy = Proxy.attach(pairAddress);
    await expect(
      pairProxy.upgradeTo(pairImplement.address)
    ).to.be.revertedWith("");

    await expect(
      this.factory.connect(this.dev).setPairImplement(pairImplement.address)
    ).to.be.revertedWith("Ownable: caller is not the owner");

    await this.factory.setPairImplement(pairImplement.address);

    const pair = this.Pair.attach(pairAddress);
    expect(await pair.version()).to.not.equal("2000");
    await this.factory.upgradePair(this.token1.address, this.token2.address);
    expect(await pair.version()).to.equal("2000");
  });

  it("lockPair", async function(){
    await addLiquidity(this, this.lp1, 100000000, 1000000000);
    const pairAddress = await this.factory.getPair(this.token1.address, this.token2.address);
    const pair = this.Pair.attach(pairAddress);
    await expect(
      pair.setLock(true)
    ).to.be.revertedWith("Ownable: caller is not the owner");

    await this.factory.lockPair(this.token1.address, this.token2.address);
    await expect(swapTk1(this, this.trader1, 30000)).to.be.revertedWith("UniswapV2: LOCKED");

    await this.factory.unlockPair(this.token1.address, this.token2.address);
    await swapTk1(this, this.trader2, 30000);
    expect(await this.token1.balanceOf(this.trader2.address)).to.equal(0);
    expect(await this.token2.balanceOf(this.trader2.address)).to.gt(0);
  });
});