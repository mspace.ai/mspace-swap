const { expect } = require("chai");
const { ethers } = require("hardhat");
const { toBN, generateDecayPoints, advanceBlock, currentBlock } = require("../scripts/utils");

describe("Test bugs", async function() {
  async function setupMining() {
    const signers = await ethers.getSigners();
    const admin = signers[0];
    const dev = signers[1];
    const user = signers[2];

    const Mining = await ethers.getContractFactory("MiningHill");
    const mining = await Mining.deploy();
    await mining.deployed();

    const MSpaceToken = await ethers.getContractFactory("MSpaceToken");
    const mspace = await MSpaceToken.deploy();
    await mspace.deployed();
    const currentBlockNumber = await currentBlock();
    await mspace.init(
      10*12,

      dev.address,
      toBN("6250000").mul(toBN(10).pow(18)),
      currentBlockNumber+1,

      dev.address,
      toBN("12500000").mul(toBN(10).pow(18)),
      currentBlockNumber+10,
      currentBlockNumber+20
    );

    const latestBlock = await currentBlock();
    const blockPerMonth = 10;
    const prelaunch = latestBlock+500;
    await (await mining.init(
      admin.address,
      mspace.address,
      prelaunch, Math.floor(blockPerMonth/2)
    )).wait();

    await mspace.transferOwnership(mining.address);

    const ERC20Mock = await ethers.getContractFactory("ERC20Mock");
    const pair = await ERC20Mock.deploy("MSpaceSwap LP Token", "MP", 18);
    await pair.deployed();
    return {signers, admin, dev, user, mining, mspace, pair} 
  }

  it('pendingMSpace when not have any pools', async function() {
    const signers = await ethers.getSigners();
    const user = signers[0];

    const Mining = await ethers.getContractFactory("MiningHill");
    const mining = await Mining.deploy();
    await mining.deployed();
    await expect(mining.pendingMSpace(0, user.address)).to.be.revertedWith("Mining: pool invalid");
  });

  it('pendingMSpace when not have userInfo', async function() {
    const {user, mining, pair} = await setupMining();
    
    await mining.add(10, pair.address, false);

    const pending = await mining.pendingMSpace(0, user.address);
    expect(pending).to.equal(0);
  });

  it("harvest before startReward", async function() {
    const {user, mining, mspace, pair} = await setupMining();

    await pair.mint(user.address, 10000000);
    await pair.connect(user).approve(mining.address, 10000000);
    
    await mining.add(10, pair.address, false);
    await mining.connect(user).deposit(0, 10000000);
    
    await advanceBlock(100);
    await mining.connect(user).harvest(0);
    expect(await mspace.balanceOf(user.address)).to.equal(0);
  });

  it("harvest empty pool", async function() {
    const {user, mining, mspace, pair} = await setupMining();

    await mining.add(10, pair.address, false);
    
    await advanceBlock(100);
    await mining.connect(user).harvest(0);
    expect(await mspace.balanceOf(user.address)).to.equal(0);
  });

  it("harvest multi pools & multi users", async function() {
    const {signers, mining, mspace, pair: pair1} = await setupMining();
    await mining.setStrategy([9000, 9000, 9000, 9000, 9000, 9000, 9000]);

    const user1 = signers[2];
    const user2 = signers[3];
    const ERC20Mock = await ethers.getContractFactory("ERC20Mock");
    const pair2 = await ERC20Mock.deploy("MSpaceSwap LP Token", "MP", 18);
    await pair2.deployed();
    await mining.add(10, pair1.address, false);
    await mining.add(20, pair2.address, false);

    await pair1.mint(user1.address, 100);
    await pair1.connect(user1).approve(mining.address, 100);
    await pair1.mint(user2.address, 200);
    await pair1.connect(user2).approve(mining.address, 200);
    await pair2.mint(user1.address, 100);
    await pair2.connect(user1).approve(mining.address, 100);
    await pair2.mint(user2.address, 200);
    await pair2.connect(user2).approve(mining.address, 200);

    await mining.connect(user1).deposit(0, 100);
    await mining.connect(user2).deposit(0, 200);
    await mining.connect(user1).deposit(1, 100);
    await mining.connect(user2).deposit(1, 200);

    const startReward = await mining.getUint("startReward");
    let latestBlock = await currentBlock();
    await advanceBlock(startReward.sub(latestBlock).add(2));
    const nintyPercent = 0.9;
    expect(await mining.pendingMSpace(0, user1.address)).to.equal(2000*nintyPercent);
    expect(await mining.pendingMSpace(0, user2.address)).to.equal(4000*nintyPercent);
    expect(await mining.pendingMSpace(1, user1.address)).to.equal(4000*nintyPercent);
    expect(await mining.pendingMSpace(1, user2.address)).to.equal(8000*nintyPercent);

    await mining.connect(user1).harvest(0);
    expect(await mspace.balanceOf(user1.address)).to.equal(3000*nintyPercent);
    expect(await mining.pendingMSpace(0, user1.address)).to.equal(0);
    await mining.connect(user1).harvest(1);
    expect(await mspace.balanceOf(user1.address)).to.equal((3000+8000)*nintyPercent);
    expect(await mining.pendingMSpace(1, user1.address)).to.equal(0);

    await mining.connect(user2).harvest(0);
    expect(await mspace.balanceOf(user2.address)).to.equal(10000*nintyPercent);
    expect(await mining.pendingMSpace(0, user2.address)).to.equal(0);
    await mining.connect(user2).harvest(1);
    expect(await mspace.balanceOf(user2.address)).to.equal((10000+24000)*nintyPercent);
    expect(await mining.pendingMSpace(1, user2.address)).to.equal(0);
  });

  it("calcReward", async function() {
    const block = toBN(37105824100);
    const Plan = await hre.ethers.getContractFactory("MintPlanMock");
    const plan = await Plan.deploy();
    await plan.deployed();
    await (await plan.init(block.add(100), 2592000)).wait();
    await (await plan.setStrategy([
      toBN("2450000000000000000"), 
      toBN("2369150000000000000"), 
      toBN("2290968050000000000")
    ])).wait();

    const from = block.add(101);
    const to   = block.add(101+100);
    const reward = await plan.calcReward(from, to);
    expect(reward).to.equal(toBN("2450000000000000000").mul(to-from));
    // console.log(toBN("2450000000000000000").mul(to-from).toString());
    // console.log(toBN("2369150000000000000").mul(to-from).toString());
  });
});