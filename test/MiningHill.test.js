const { logger } = require("@ethersproject/wordlists");
const { expect } = require("chai");
const { ethers } = require("hardhat");
const { toBN, generateDecayPoints, advanceBlock, currentBlock } = require("../scripts/utils");

describe("MiningHill", async function() {
  const numerator = toBN(953);
  const denominator = toBN(1000);
  const startPoint = toBN(34).mul(toBN(1e18)).div(10);
  const decayPoints = generateDecayPoints(startPoint, numerator, denominator, 47);

  before(async function() {
    this.MSpaceToken = await ethers.getContractFactory("MSpaceToken");
    this.Mining = await ethers.getContractFactory("MiningHill");
    this.ERC20Mock = await ethers.getContractFactory("ERC20Mock");
    this.signers = await ethers.getSigners();
    this.admin = this.signers[0];
    this.team = this.signers[1];
    this.lp1 = this.signers[2];
    this.lp2 = this.signers[3];
    this.trader1 = this.signers[4];
    this.trader2 = this.signers[5];
    this.airdrop = this.signers[6];

    this.tenPercentMSP = "25000000";
  });

  async function setUp(runner, startReward, blockStep, strategyPoints) {
    runner.mspace = await runner.MSpaceToken.connect(runner.admin).deploy();
    await runner.mspace.deployed();

    const currentBlockNumber = await currentBlock();
    await runner.mspace.init(
      blockStep*2*12,

      runner.team.address,
      toBN(runner.tenPercentMSP).mul(toBN(10).pow(18)).div(4), // 2.5%
      currentBlockNumber+30,

      runner.airdrop.address,
      toBN(runner.tenPercentMSP).mul(toBN(10).pow(18)).div(2), // 5%
      currentBlockNumber+30,
      currentBlockNumber+40
    );
    
    runner.mining = await runner.Mining.connect(runner.admin).deploy();
    await runner.mining.deployed();
    await runner.mining.init(
      runner.admin.address,
      runner.mspace.address,
      startReward, blockStep
    );
    await runner.mining.setStrategy(strategyPoints);

    await runner.mspace.transferOwnership(runner.mining.address);
    
    runner.pair1 = await runner.ERC20Mock.connect(runner.admin).deploy("MSpaceSwap LP Token", "MP", 18);
    await runner.pair1.deployed();
    runner.pair2 = await runner.ERC20Mock.connect(runner.admin).deploy("MSpaceSwap LP Token", "MP", 18);
    await runner.pair2.deployed();
  };

  async function defaultSetup(runner) {
    const latestBlock = await currentBlock();
    const blockPerMonth = 10;
    const startReward = latestBlock+50;
    // set up strategy points
    const prelaunchReward = toBN(21250000).mul(toBN(1e18)).div(Math.floor(blockPerMonth/2));
    const bootstrapReward = toBN(30).mul(toBN(1e18));
    const strategyPoints = [prelaunchReward, bootstrapReward].concat(decayPoints.map(p=> [p,p]).flat());
    await setUp(runner, startReward, blockPerMonth/2, strategyPoints);
  }

  beforeEach(async function() {
    await defaultSetup(this);
  });

  it("transferOwner", async function() {
    let owner = await this.mining.owner();
    expect(owner).to.equal(this.admin.address);
    await this.mining.transferOwnership(this.team.address);
    owner = await this.mining.owner();
    expect(owner).to.equal(this.team.address);
  });

  it("init twice", async function() {
    await expect(this.mining.init(
      this.admin.address,
      this.mspace.address,
      0, 1
    )).to.be.revertedWith("Initializable: contract is already initialized");
  });

  it("add", async function() {
    await this.mining.add(10, this.pair1.address, false);
    let pool = await this.mining.poolInfo(0);
    expect(await this.mining.poolLength()).to.equal(1);
    expect(pool["allocPoint"]).to.equal(10);
    expect(pool["totalShare"]).to.equal(0);
    expect(pool["lastUpdate"]).to.equal(await currentBlock());
  });

  it("add twice", async function() {
    await this.mining.add(10, this.pair1.address, false);
    expect(await this.mining.poolLength()).to.equal(1);
    await expect(this.mining.add(10, this.pair1.address, false))
      .to.be.revertedWith("Mining: pool existed");
  });

  it("add after prelaunch", async function() {
    const forwardBlocks = 2;
    const prelaunch = await this.mining.getUint("startReward");
    let latestBlock = await currentBlock();
    await advanceBlock(prelaunch.sub(latestBlock).add(forwardBlocks));

    await this.mining.add(10, this.pair1.address, false);
    latestBlock = await currentBlock();
    let pool = await this.mining.poolInfo(0);
    expect(await this.mining.poolLength()).to.equal(1);
    expect(pool["allocPoint"]).to.equal(10);
    expect(pool["totalShare"]).to.equal(0);
    expect(pool["lastUpdate"]).to.equal(latestBlock);
  });

  it("deposit", async function() {
    await this.mining.add(10, this.pair1.address, false);
    await this.pair1.mint(this.lp1.address, 100);
    await this.pair1.connect(this.lp1).approve(this.mining.address, 100);
    await this.mining.connect(this.lp1).deposit(0, 100);
    expect(await this.pair1.balanceOf(this.mining.address)).to.equal(100);
    let user = await this.mining.userInfo(0, this.lp1.address);
    expect(user["amount"]).to.equal(100);
    expect(user["lastUpdate"]).to.equal(await currentBlock());
    expect(user["poolShare"]).to.equal(0);

    let pool = await this.mining.poolInfo(0);
    expect(pool["totalShare"]).to.equal(0);
    expect(pool["lastUpdate"]).to.equal(await currentBlock());
  });

  it("add many", async function() {
    await this.mining.add(10, this.pair1.address, false);
    let pool0 = await this.mining.poolInfo(0);
    expect(pool0["allocPoint"]).to.equal(10);
    expect(pool0["totalShare"]).to.equal(0);
    expect(pool0["lastUpdate"]).to.equal(await currentBlock());

    await this.mining.add(20, this.pair2.address, false);
    let pool1 = await this.mining.poolInfo(1);
    expect(pool1["allocPoint"]).to.equal(20);
    expect(pool1["totalShare"]).to.equal(0);
    expect(pool1["lastUpdate"]).to.equal(await currentBlock());

    expect(await this.mining.poolLength()).to.equal(2);
    expect(await this.mining.totalAllocPoint()).to.equal(30);
  });
  
  it("set", async function() {
    await this.mining.add(10, this.pair1.address, false);
    let pool = await this.mining.poolInfo(0);
    expect(pool["allocPoint"]).to.equal(10);
    await this.mining.set(0, 20, false);
    pool = await this.mining.poolInfo(0);
    expect(pool["allocPoint"]).to.equal(20);
  });

  it("updatePool", async function() {
    await this.mining.add(10, this.pair1.address, false);
    await this.pair1.mint(this.lp1.address, 100);
    await this.pair1.connect(this.lp1).approve(this.mining.address, 100);
    await this.mining.connect(this.lp1).deposit(0, 100);
   
    // not into prelaunch phrase
    let poolLastUpdate = (await this.mining.poolInfo(0))["lastUpdate"];
    let userLastUpdate = (await this.mining.userInfo(0, this.lp1.address))["lastUpdate"];
    const prelaunch = await this.mining.getUint("startReward");
    expect(poolLastUpdate).to.equal(await currentBlock());
    expect(userLastUpdate).to.equal(await currentBlock());

    await this.mining.updatePool(0);
    poolLastUpdate = (await this.mining.poolInfo(0))["lastUpdate"];
    expect(poolLastUpdate).to.equal(await currentBlock());
    
    let latestBlock = await currentBlock();
    expect(latestBlock).to.lte(prelaunch);

    // fast forward into relaunch phrase
    const forwardBlocks = 2;
    await advanceBlock(prelaunch.sub(latestBlock).add(forwardBlocks));
    await this.mining.updatePool(0);
    latestBlock = await currentBlock();
    poolLastUpdate = (await this.mining.poolInfo(0))["lastUpdate"];
    expect(poolLastUpdate).to.equal(latestBlock);
    let poolShare = (await this.mining.poolInfo(0))["totalShare"];
    expect(poolShare).to.equal((await this.pair1.balanceOf(this.mining.address)).mul(forwardBlocks+1));

    await advanceBlock(forwardBlocks);
    await this.mining.updatePool(0);
    latestBlock = await currentBlock();
    expect((await this.mining.poolInfo(0))["totalShare"]).to.equal(poolShare.add(
      (await this.pair1.balanceOf(this.mining.address)).mul(forwardBlocks+1)
    ));
  });

  it("massUpdatePools", async function() {
    await this.mining.add(10, this.pair1.address, false);
    await this.pair1.mint(this.lp1.address, 100);
    await this.pair1.connect(this.lp1).approve(this.mining.address, 100);
    await this.mining.connect(this.lp1).deposit(0, 100);

    await this.mining.add(20, this.pair2.address, false);
    await this.pair2.mint(this.lp1.address, 50);
    await this.pair2.connect(this.lp1).approve(this.mining.address, 50);
    await this.mining.connect(this.lp1).deposit(1, 50);

    let latestBlock = await currentBlock();
    const forwardBlocks = 2;
    const prelaunch = await this.mining.getUint("startReward");
    await advanceBlock(prelaunch.sub(latestBlock).add(forwardBlocks));
    await this.mining.massUpdatePools();
    latestBlock = await currentBlock();

    let pool0 = await this.mining.poolInfo(0);
    expect(pool0["allocPoint"]).to.equal(10);
    expect(pool0["totalShare"]).to.equal((await this.pair1.balanceOf(this.mining.address)).mul(forwardBlocks+1));
    expect(pool0["lastUpdate"]).to.equal(latestBlock);

    let pool1 = await this.mining.poolInfo(1);
    expect(pool1["allocPoint"]).to.equal(20);
    expect(pool1["totalShare"]).to.equal((await this.pair2.balanceOf(this.mining.address)).mul(forwardBlocks+1));
    expect(pool1["lastUpdate"]).to.equal(latestBlock);
  });

  it("pendingMSpace", async function() {
    const pool0Alloc = 10;
    const pool1Alloc = 5;
    await this.mining.add(pool0Alloc, this.pair1.address, false);

    await this.pair1.mint(this.lp1.address, 100);
    await this.pair1.connect(this.lp1).approve(this.mining.address, 100);
    await this.mining.connect(this.lp1).deposit(0, 100);

    await this.pair1.mint(this.lp2.address, 50);
    await this.pair1.connect(this.lp2).approve(this.mining.address, 50);
    await this.mining.connect(this.lp2).deposit(0, 50);

    await this.mining.add(pool1Alloc, this.pair2.address, false);

    await this.pair2.mint(this.lp2.address, 100);
    await this.pair2.connect(this.lp2).approve(this.mining.address, 50);
    await this.mining.connect(this.lp2).deposit(1, 50);

    let latestBlock = await currentBlock();
    const forwardBlocks = 2;
    const prelaunch = await this.mining.getUint("startReward");
    await advanceBlock(prelaunch.sub(latestBlock).add(forwardBlocks));
    latestBlock = await currentBlock();
    expect(await this.mining.pendingMSpace(1, this.lp1.address)).to.equal(0);
    expect(await this.mining.pendingMSpace(1, this.lp2.address)).to.gt(0);

    let reward = await this.mining.pendingMSpace(0, this.lp1.address);
    let totalReward = (await this.mining.rewardStrategy(0)).mul(forwardBlocks);
    let poolReward = totalReward.mul(pool0Alloc).div(pool0Alloc+pool1Alloc);
    let userShare = (await this.mining.userInfo(0, this.lp1.address))["amount"].mul(forwardBlocks);
    let poolShare = (await this.pair1.balanceOf(this.mining.address)).mul(forwardBlocks);

    expect(reward).to.equal(poolReward.mul(userShare).div(poolShare).mul(90).div(100));
  });
  
  it("withdraw", async function() {
    await this.mining.add(10, this.pair1.address, false);
    await this.pair1.mint(this.lp1.address, 100);
    await this.pair1.connect(this.lp1).approve(this.mining.address, 100);
    await this.mining.connect(this.lp1).deposit(0, 100);

    let latestBlock = await currentBlock();
    const forwardBlocks = 2;
    const prelaunch = await this.mining.getUint("startReward");
    await advanceBlock(prelaunch.sub(latestBlock).add(forwardBlocks));

    await this.mining.updatePool(0);
    let poolShare = (await this.mining.poolInfo(0))["totalShare"];
    expect(poolShare).to.gt(0);

    await this.pair1.mint(this.lp1.address, 100);
    await this.pair1.connect(this.lp1).approve(this.mining.address, 100);
    await this.mining.connect(this.lp1).deposit(0, 100);

    poolShare = (await this.mining.poolInfo(0))["totalShare"];
    userShare = (await this.mining.userInfo(0, this.lp1.address))["poolShare"];
    expect(userShare).to.equal(poolShare);

    await this.pair1.mint(this.lp2.address, 50);
    await this.pair1.connect(this.lp2).approve(this.mining.address, 50);
    await this.mining.connect(this.lp2).deposit(0, 50);
    await this.mining.updatePool(0);
    poolShare = (await this.mining.poolInfo(0))["totalShare"];
    expect(userShare).to.lt(poolShare);

    expect(await this.pair1.balanceOf(this.mining.address)).to.equal(250);
    expect(await this.pair1.balanceOf(this.lp1.address)).to.equal(0);
    expect(await this.pair1.balanceOf(this.lp2.address)).to.equal(0);
    expect(await this.mspace.balanceOf(this.mining.address)).to.gt(0);
    expect(await this.mspace.balanceOf(this.lp1.address)).to.equal(0);
    expect(await this.mspace.balanceOf(this.lp2.address)).to.equal(0);

    await this.mining.connect(this.lp1).withdraw(0, 100);
    expect(await this.pair1.balanceOf(this.mining.address)).to.equal(150);
    expect(await this.pair1.balanceOf(this.lp1.address)).to.gt(50);
    expect(await this.mspace.balanceOf(this.mining.address)).to.gt(0);

    await this.mining.connect(this.lp2).withdraw(0, 50);
    expect(await this.pair1.balanceOf(this.mining.address)).to.equal(100);
    expect(await this.mspace.balanceOf(this.mining.address)).to.gt(0);

    await this.mining.connect(this.lp1).withdraw(0, 100);
    expect(await this.mspace.balanceOf(this.mining.address)).to.gt(0);
  });

  it("emergencyWithdraw", async function() {
    await this.mining.add(10, this.pair1.address, false);
    await this.pair1.mint(this.lp1.address, 100);
    await this.pair1.connect(this.lp1).approve(this.mining.address, 100);
    await this.mining.connect(this.lp1).deposit(0, 100);

    let latestBlock = await currentBlock();
    const forwardBlocks = 2;
    const prelaunch = await this.mining.getUint("startReward");
    await advanceBlock(prelaunch.sub(latestBlock).add(forwardBlocks));

    await this.mining.connect(this.lp1).emergencyWithdraw(0);
    expect(await this.pair1.balanceOf(this.lp1.address)).to.equal(100);
    expect(await this.pair1.balanceOf(this.mining.address)).to.equal(0);
  });

  it("harvest", async function() {
    await this.mining.add(10, this.pair1.address, false);
    await this.pair1.mint(this.lp1.address, 100);
    await this.pair1.connect(this.lp1).approve(this.mining.address, 100);
    await this.mining.connect(this.lp1).deposit(0, 100);

    let latestBlock = await currentBlock();
    const forwardBlocks = 2;
    const prelaunch = await this.mining.getUint("startReward");
    await advanceBlock(prelaunch.sub(latestBlock).add(forwardBlocks));

    // one more block passed
    await this.mining.connect(this.lp1).harvest(0);
    const reward = await this.mspace.balanceOf(this.lp1.address);
    expect(await this.mspace.balanceOf(this.mining.address)).to.equal(0);
    expect((await this.mining.poolInfo(0))["rewardDebt"]).to.equal(0);
    expect((await this.mining.userInfo(0, this.lp1.address))["poolShare"]).to.equal(0);
    expect(reward).to.gt(0);
    expect(reward).to.equal((await this.mining.rewardStrategy(0)).mul(forwardBlocks+1).mul(90).div(100));
  });

  it("harvest empty", async function() {
    await this.mining.add(10, this.pair1.address, false);
    await this.pair1.mint(this.lp1.address, 100);
    await this.pair1.connect(this.lp1).approve(this.mining.address, 100);
    await this.mining.connect(this.lp1).deposit(0, 100);

    await this.mining.add(5, this.pair2.address, false);

    await this.mining.connect(this.lp1).harvest(1);
    expect(await this.mspace.balanceOf(this.lp1.address)).to.equal(0);
    await this.mining.connect(this.lp2).harvest(0);
    expect(await this.mspace.balanceOf(this.lp2.address)).to.equal(0);
  });

  it("pendingMSpace=harvest", async function() {
    async function setupPool(runner) {
      await runner.mining.add(10, runner.pair1.address, false);
      await runner.pair1.mint(runner.lp1.address, 100);
      await runner.pair1.connect(runner.lp1).approve(runner.mining.address, 100);
      await runner.mining.connect(runner.lp1).deposit(0, 100);
    }
    
    await setupPool(this);

    let latestBlock = await currentBlock();
    const forwardBlocks = 2;
    let prelaunch = await this.mining.getUint("startReward");
    await advanceBlock(prelaunch.sub(latestBlock).add(forwardBlocks+1));
    const pending = await this.mining.pendingMSpace(0, this.lp1.address);
    expect(pending).to.gt(0);

    // one more block passed
    await defaultSetup(this);
    await setupPool(this);
    latestBlock = await currentBlock();
    prelaunch = await this.mining.getUint("startReward");
    await advanceBlock(prelaunch.sub(latestBlock).add(forwardBlocks));
    const oldBalance = await this.mspace.balanceOf(this.lp1.address);
    // this transaction will made one block passed so forwardBlocks in pending must equal harvest+1
    await this.mining.connect(this.lp1).harvest(0);
    const newBalance = await this.mspace.balanceOf(this.lp1.address);
    expect(newBalance.sub(oldBalance)).to.equal(pending);
  });

  it("operationWallet", async function() {
    await this.mining.add(10, this.pair1.address, false);
    await this.pair1.mint(this.lp1.address, 100);
    await this.pair1.connect(this.lp1).approve(this.mining.address, 100);
    await this.mining.connect(this.lp1).deposit(0, 100);

    let latestBlock = await currentBlock();
    const forwardBlocks = 2;
    const prelaunch = await this.mining.getUint("startReward");
    await advanceBlock(prelaunch.sub(latestBlock).add(forwardBlocks));
    await this.mining.connect(this.lp1).harvest(0);

    const balance = await this.mspace.balanceOf(this.lp1.address);
    expect(balance).to.gt(0);
    const operationFee = await this.mspace.balanceOf(await this.mining.getAddress("operationWallet"));
    // LP take 90% of fund, operator take 10%
    expect(operationFee).to.equal(balance.div(9));
  });

  it("pending=harvest with multi pools & multi users", async function() {
    async function setupPool(runner) {
      await runner.mining.add(10, runner.pair1.address, false);
      await runner.pair1.mint(runner.lp1.address, 100);
      await runner.pair1.connect(runner.lp1).approve(runner.mining.address, 100);
      await runner.mining.connect(runner.lp1).deposit(0, 100);
      await runner.pair1.mint(runner.lp2.address, 200);
      await runner.pair1.connect(runner.lp2).approve(runner.mining.address, 200);
      await runner.mining.connect(runner.lp2).deposit(0, 200);

      await runner.mining.add(15, runner.pair2.address, false);
      await runner.pair2.mint(runner.lp1.address, 50);
      await runner.pair2.connect(runner.lp1).approve(runner.mining.address, 50);
      await runner.mining.connect(runner.lp1).deposit(1, 50);
      await runner.pair2.mint(runner.lp2.address, 25);
      await runner.pair2.connect(runner.lp2).approve(runner.mining.address, 25);
      await runner.mining.connect(runner.lp2).deposit(1, 25);
    }

    await setupPool(this);
    let latestBlock = await currentBlock();
    const forwardBlocks = 2;
    let prelaunch = await this.mining.getUint("startReward");
    await advanceBlock(prelaunch.sub(latestBlock).add(forwardBlocks+1));
    const pending = await this.mining.pendingMSpace(0, this.lp1.address);
    expect(pending).to.gt(0);

    await defaultSetup(this);
    await setupPool(this);
    latestBlock = await currentBlock();
    prelaunch = await this.mining.getUint("startReward");
    await advanceBlock(prelaunch.sub(latestBlock).add(forwardBlocks));
    let oldBalance = await this.mspace.balanceOf(this.lp1.address);
    await this.mining.connect(this.lp1).harvest(0);
    const harvest = (await this.mspace.balanceOf(this.lp1.address)).sub(oldBalance);
    expect(harvest).to.gt(0);

    expect(pending).to.equal(harvest);
  });

  it("call MSP functions", async function(){
    await expect(this.mspace.setTeamWallet(this.admin.address))
      .to.be.revertedWith("Ownable: caller is not the owner");
    await expect(this.mspace.setAirdropWallet(this.admin.address))
      .to.be.revertedWith("Ownable: caller is not the owner");
    await expect(this.mspace.airdrop1()).to.be.revertedWith("Ownable: caller is not the owner");
    await expect(this.mspace.airdrop2()).to.be.revertedWith("Ownable: caller is not the owner");
    await expect(this.mspace.teamVesting()).to.be.revertedWith("Ownable: caller is not the owner");

    await expect(this.mining.airdrop1()).to.be.revertedWith("MSP: not in time to drop round 1st");
    await expect(this.mining.airdrop2()).to.be.revertedWith("MSP: not in time to drop round 2nd");
    await expect(this.mining.teamVesting()).to.be.revertedWith("MSP: not in time");

    await advanceBlock(20);
    await this.mining.airdrop1();
    expect(await this.mspace.balanceOf(this.airdrop.address))
      .to.equal(toBN(this.tenPercentMSP).mul(toBN(10).pow(18)).div(2));

    await advanceBlock(10);
    await this.mining.airdrop2();
    expect(await this.mspace.balanceOf(this.airdrop.address))
      .to.equal(toBN(this.tenPercentMSP).mul(toBN(10).pow(18)));

    await advanceBlock(10*12);
    await this.mining.teamVesting();
    expect(await this.mspace.balanceOf(this.team.address))
      .to.equal(
        toBN(this.tenPercentMSP).mul(toBN(10).pow(18)).div(4)
      );

    await this.mining.setTeamWallet(this.admin.address);
    expect(await this.mspace.getAddress("teamWallet")).to.equal(this.admin.address);
    await this.mining.setAirdropWallet(this.admin.address);
    expect(await this.mspace.getAddress("airdropWallet")).to.equal(this.admin.address);
  });
});