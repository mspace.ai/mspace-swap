const { expect } = require("chai");
const { ethers } = require("hardhat");
const { ProxyArtifact } = require("../scripts/constants");


describe("Proxy", async function () {
  before(async function() {
    this.Proxy = await ethers.getContractFactory(ProxyArtifact);

    this.ContractV1 = await ethers.getContractFactory("ContractMockV1");
    this.contractV1Impl = await this.ContractV1.deploy();
    await this.contractV1Impl.deployed();

    this.ContractV2 = await ethers.getContractFactory("ContractMockV2");
    this.contractV2Impl = await this.ContractV2.deploy();
    await this.contractV2Impl.deployed();

    this.signers = await ethers.getSigners();
    this.admin  = this.signers[0];
    this.dev  = this.signers[1];
  });

  beforeEach(async function() {
    this.proxy = await this.Proxy.deploy(this.contractV1Impl.address, this.admin.address, "0x");
    await this.proxy.deployed();
  });

  it("admin", async function() {
    expect(await this.proxy.admin()).to.equal(this.admin.address);
  });

  it("implementation", async function() {
    expect(await this.proxy.implementation()).to.equal(this.contractV1Impl.address);
  });

  it("upgradeTo", async function() {
    const contract = this.ContractV1.attach(this.proxy.address);
    expect(await contract.version()).to.equal(1);
    const contract2 = this.ContractV2.attach(this.proxy.address);
    expect(await contract2.version()).to.equal(1);

    await expect(
      this.proxy.connect(this.dev).upgradeTo(this.contractV2Impl.address)
    ).to.be.revertedWith("");

    await this.proxy.connect(this.admin).upgradeTo(this.contractV2Impl.address);
    expect(await this.proxy.implementation()).to.equal(this.contractV2Impl.address);
    expect(await contract.version()).to.equal(2);
  });
});