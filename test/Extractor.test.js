const { expect } = require("chai");
const { ethers } = require("hardhat");

describe("Extractor", async function () {
  before(async function() {
    this.WMETAMock = await ethers.getContractFactory("WMETAMock");
    this.ERC20Mock = await ethers.getContractFactory("ERC20Mock");
    this.Factory = await ethers.getContractFactory("UniswapV2Factory");
    this.Router = await ethers.getContractFactory("UniswapV2Router02");
    this.Pair = await ethers.getContractFactory("UniswapV2Pair");
    this.pairImplement = await this.Pair.deploy();
    await this.pairImplement.deployed();

    this.Extractor = await ethers.getContractFactory("Extractor");
    this.Bank = await ethers.getContractFactory("Bank");

    this.signers = await ethers.getSigners();
    this.admin = this.signers[0];
    this.dev = this.signers[1];
    this.lp1 = this.signers[2];
    this.lp2 = this.signers[3];
    this.trader1 = this.signers[4];
    this.trader2 = this.signers[5];
  });

  beforeEach(async function() {
    this.factory = await this.Factory.deploy();
    await this.factory.deployed();
    await this.factory.init(this.pairImplement.address);
    this.wmeta = await this.WMETAMock.deploy();
    await this.wmeta.deployed();
    // console.log("wmeta", this.wmeta.address);
    this.router = await this.Router.deploy(this.factory.address, this.wmeta.address);
    await this.router.deployed();

    this.token1 = await this.ERC20Mock.deploy("Token 1", "TK1", 18);
    await this.token1.deployed();
    // console.log("token1", this.token1.address);
    this.token2 = await this.ERC20Mock.deploy("Token 2", "TK2", 18);
    await this.token2.deployed();
    // console.log("token2", this.token2.address);

    this.mspace = await this.ERC20Mock.deploy("MSP Token", "MSP", 18);
    await this.mspace.deployed();
    // console.log("mspace", this.mspace.address);
    this.bank = await this.Bank.deploy();
    await this.bank.deployed();
    await this.bank.init(this.mspace.address);
    // console.log("bank", this.bank.address);
    // await this.mspace.transferOwnership(this.bank.address);

    this.extractor = await this.Extractor.deploy();
    await this.extractor.deployed();
    await this.extractor.init(
      this.factory.address, this.bank.address, 
      this.mspace.address, this.wmeta.address
    );
    await this.factory.setFeeTo(this.extractor.address);
    
  });

  async function createPair(user, router, token1, token2, tk1, tk2) {
    await token1.mint(user.address, tk1);
    await token1.connect(user).approve(router.address, tk1);
    await token2.mint(user.address, tk2);
    await token2.connect(user).approve(router.address, tk2);
    await router.connect(user).addLiquidity(
      token1.address, token2.address,
      tk1, tk2, 0, 0,
      user.address,
      Math.floor(new Date().getTime()/1000) + 3600
    );
  }

  it("init twice", async function() {
    await expect(this.extractor.init(
      this.factory.address, this.bank.address, 
      this.mspace.address, this.wmeta.address
    )).to.be.revertedWith("Initializable: contract is already initialized");
  });

  async function setupSwap(runner, tk1, tk2) {
    
    await createPair(runner.lp1, runner.router, runner.token1, runner.token2, tk1, tk2);

    for (let i=0; i<10; i++) {
      const inTk1 = 30000000;
      await runner.token1.mint(runner.trader1.address, inTk1);
      await runner.token1.connect(runner.trader1).approve(runner.router.address, inTk1);
      await runner.router.connect(runner.trader1).swapExactTokensForTokens(
        inTk1, 0, [runner.token1.address, runner.token2.address],
        runner.trader1.address,
        Math.floor(new Date().getTime()/1000) + 3600
      );

      const inTk2 = 1500000;
      await runner.token2.mint(runner.trader1.address, inTk2);
      await runner.token2.connect(runner.trader1).approve(runner.router.address, inTk2);
      await runner.router.connect(runner.trader1).swapExactTokensForTokens(
        inTk2, 0, [runner.token2.address, runner.token1.address],
        runner.trader1.address,
        Math.floor(new Date().getTime()/1000) + 3600
      );
    }

    const pairAddr = await runner.factory.getPair(runner.token1.address, runner.token2.address);
    const pair = await runner.Pair.attach(pairAddr);
    const liquidity = await pair.balanceOf(runner.lp1.address);
    await pair.connect(runner.lp1).approve(runner.router.address, liquidity);
    await runner.router.connect(runner.lp1).removeLiquidity(
      runner.token1.address, runner.token2.address,
      liquidity, 0, 0,
      runner.lp1.address,
      Math.floor(new Date().getTime()/1000) + 3600
    );
    return pair;
  }

  it("convert", async function() {
    const tk1 = 1000000000;
    const tk2 = 500000000;
    const pair = await setupSwap(this, tk1, tk2);

    expect(await pair.balanceOf(this.extractor.address)).to.equal(52429);

    // Stake MSP
    const stake = 100000;
    await this.mspace.mint(this.lp1.address, stake);
    await this.mspace.connect(this.lp1).approve(this.bank.address, stake);
    await this.bank.connect(this.lp1).enter(stake);

    // create bridge pairs
    await createPair(this.lp2, this.router, this.token1, this.mspace, tk1, tk2);
    await this.extractor.setBridge(this.token1.address, this.mspace.address);
    await createPair(this.lp2, this.router, this.token2, this.mspace, tk1, tk2);
    await this.extractor.setBridge(this.token2.address, this.mspace.address);
    // await createPair(this.lp2, this.router, this.wmeta, this.mspace, tk1, tk2);

    // after convert & leave, lp1 will get more MSP than at first
    await this.extractor.convert(this.token1.address, this.token2.address);
    await this.bank.connect(this.lp1).leave(stake);
    expect(await this.mspace.balanceOf(this.lp1.address)).to.gt(stake);
  });

  it("not setup bridge to convert", async function() {
    const tk1 = 1000000000;
    const tk2 = 500000000;
    const pair = await setupSwap(this, tk1, tk2);

    // Stake MSP
    const stake = 100000;
    await this.mspace.mint(this.lp1.address, stake);
    await this.mspace.connect(this.lp1).approve(this.bank.address, stake);
    await this.bank.connect(this.lp1).enter(stake);

    await expect(this.extractor.convert(this.token1.address, this.token2.address))
      .to.be.revertedWith("Extractor: Cannot convert");
  });

  it("not have pool to convert", async function() {
    const tk1 = 1000000000;
    const tk2 = 500000000;
    const pair = await setupSwap(this, tk1, tk2);

    // Stake MSP
    const stake = 100000;
    await this.mspace.mint(this.lp1.address, stake);
    await this.mspace.connect(this.lp1).approve(this.bank.address, stake);
    await this.bank.connect(this.lp1).enter(stake);

    await this.extractor.setBridge(this.token1.address, this.mspace.address);

    await expect(this.extractor.convert(this.token1.address, this.token2.address))
      .to.be.revertedWith("Extractor: Cannot convert");
  });

  it("setup bridge twice", async function() {
    const tk1 = 1000000000;
    const tk2 = 500000000;
    const pair = await setupSwap(this, tk1, tk2);

    // Stake MSP
    const stake = 100000;
    await this.mspace.mint(this.lp1.address, stake);
    await this.mspace.connect(this.lp1).approve(this.bank.address, stake);
    await this.bank.connect(this.lp1).enter(stake);

    await this.extractor.setBridge(this.token1.address, this.mspace.address);
    await this.extractor.setBridge(this.token1.address, this.mspace.address);
  });
});