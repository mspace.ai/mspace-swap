// change if env LOCAL
const ProxyArtifact = "flattened/UpgradeableProxy.sol:UpgradeableProxy";
const ENSRegistryArtifact = "flattened/ENSRegistry.sol:ENSRegistry";
const ENSRegistryWithFallbackArtifact = "flattened/ENSRegistryWithFallback.sol:ENSRegistryWithFallback";
// const ProxyArtifact = "UpgradeableProxy";
// const ENSRegistryArtifact = "ENSRegistry";
// const ENSRegistryWithFallbackArtifact = "ENSRegistryWithFallback";

const ZERO_ADDRESS = "0x0000000000000000000000000000000000000000";

const Tokens = {
  ETH: {
    1: "0xc02aaa39b223fe8d0a0e5c4f27ead9083c756cc2",
    11: "0x56538e02076d2978fDFbeae76128fCCC06ac102B",
    12: "0x2de8F3f0e07414e32790c257b009F76ecb11cFDf",
    42: "0x6c9411fbb5c9f4cc9c2805e05a1a1390ee145b02",
  },
  META: {
    1: "0x8647a310336371590ad3be7ffca515ace901fdd6",
    11: "0x3dCB7c0b536022Eb3D33919A1493a5B7789E97D0",
    12: "0x6c9411fbb5c9f4cc9c2805e05a1a1390ee145b02", // accidently the same address with WETH in kovan
    42: "0x6197fc60e7ff8cc6d4d14696827f3384b82dd739",
  },
  USDC: {
    1: "0xa0b86991c6218b36c1d19d4a2e9eb0ce3606eb48",
    11: "0xe2600B19f17fa97f0d671EC2c513C99dB19f1971",
    12: "0xb18D248866F03b88bEA4a256FC5593D7a68bb64B",
    42: "0x26FEe24B4c9cAD2138b7010C3c88B0ad0517d782",
  },
  DAI: {
    1: "0x6b175474e89094c44da98b954eedeac495271d0f",
    11: "0xd148E880A106049dF1f72c2Ba92B4c7130AD6F20",
    12: "0x2251984A9b0ec156349d546d51Ebb7124Dc79588",
    42: "0x2cc5fFAFb67aeeEde55B241876108C455FC48aE4",
  },
  BTC: {
    1: "0x2260fac5e5542a773aa44fbcfedf7c193bc2c599",
    11: "0xb7A558AF782251f51152E290988bcD2AfFd1C204",
    12: "0x8691B0EB0Ee188BA0d28A41a0aDd403F4e44FD47",
    42: "0x9cf8ec6022E4DaE94Ef53555BDBDC9CAe98A25be",
  },
}

module.exports = {
  ZERO_ADDRESS, 
  ProxyArtifact, ENSRegistryArtifact, ENSRegistryWithFallbackArtifact,
  Tokens,
}