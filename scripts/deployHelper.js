const axios = require('axios');

const mainnetKey = "28d59eb54e6096137700d9433d162df6d5ac8e86fe95eab9dd7fb1481e2ecaa9";
const testnetKey = "c48f84169775dfeb80f6577044e1df2dd0b18df122d1c6d48ab71176f3e35180";

async function isVerified({address, mainnet = true} = {}) {
  const endpoint = mainnet == true ? `https://explorerapi.metadium.com/contracts/${address}/code` : `https://testnetapi.metadium.com/contracts/${address}/code`;
  const apiKey = mainnet == true ? mainnetKey : testnetKey;

  try{ 
    const res = await axios.get(
      endpoint,
      {
        headers: {
          "Content-Type": "application/json",
          "api-key": apiKey
        }
      }
    );
    // console.log(res);
    return res.data["verify_status"] === 1;
  } catch (e) {
    if (e.response.status === 404) {
      return false;
    }
    // console.log(e.response);
    console.log(
      `check contract ${address} is verified got error`, 
      'status=', e.response.status, 
      'data=', e.response.data
    );
  }
  return false;
}

async function verifier({hre, contractName, address, metadata, source, mainnet = true} = {}) {
  const endpoint = mainnet == true ? `https://explorerapi.metadium.com/contracts/${address}/verify` : `https://testnetapi.metadium.com/contracts/${address}/verify`;
  const apiKey = mainnet == true ? mainnetKey : testnetKey;
  const abiCoder = new hre.ethers.utils.AbiCoder();
  const verifyData = {
    contract_name: contractName,
    compiler: metadata.compiler.version.split("+commit")[0],  
    optimization_enabled: metadata.settings.optimizer.enabled == true ? 1:0,
    runs_optimizer: metadata.settings.optimizer.runs,
    contract_source: Buffer.from(source).toString("base64"),
    constructor_arguments: abiCoder.encode([], []).slice(2),
    libraries: [],
  }

  try{ 
    const res = await axios.post(
      endpoint, verifyData,
      {
        headers: {
          "Content-Type": "application/json",
          "api-key": apiKey
        }
      }
    );
    // console.log(res);
    return res.data["results"] === 1;
  } catch (e) {
    // console.log(e.response);
    console.log(
      `verify ${contractName} got error`, 
      'status=', e.response.status, 
      'data=', e.response.data
    );
  }
  return false;
}

async function verifyImplAndProxyHepler({hre, contractName, mainnet = true} = {}) {
  let dpl = await hre.deployments.get(`${contractName}_Implementation`);
  if (!(await isVerified({address: dpl.address, mainnet}))) {
    let metadata = JSON.parse(dpl.metadata);
    let result = await verifier({
      hre: hre,
      contractName: contractName,
      address: dpl.address,
      metadata: metadata,
      source: metadata.sources[`flattened/${contractName}.sol`].content,
      mainnet: mainnet
    });
    console.log(`verify "${contractName}_Implement" at ${dpl.address}`, result);
  } else {
    console.log(`"${contractName}_Implement" at ${dpl.address} was verified`);
  }

  dpl = await hre.deployments.get(`${contractName}_Proxy`);
  if (!(await isVerified({address: dpl.address, mainnet}))) {
    const buildInfo = await hre.artifacts.getBuildInfo("flattened/UpgradeableProxy.sol:UpgradeableProxy");
    let metadata = JSON.parse(
      buildInfo.output
        .contracts["flattened/UpgradeableProxy.sol"]["UpgradeableProxy"].metadata
    );
    let result = await verifier({
      hre: hre,
      contractName: "UpgradeableProxy",
      address: dpl.address,
      metadata: metadata,
      source: metadata.sources["flattened/UpgradeableProxy.sol"].content,
      mainnet: mainnet
    });
    console.log(`verify "${contractName}_Proxy" at ${dpl.address}`, result);
  } else {
    console.log(`"${contractName}_Proxy" at ${dpl.address} was verified`);
  }
}

async function verifyContractHelper({hre, contractName, mainnet = true} = {}) {
  let dpl = await hre.deployments.get(contractName);
  if (!(await isVerified({address: dpl.address, mainnet: mainnet}))) {
    let metadata = JSON.parse(dpl.metadata);
    let result = await verifier({
      hre: hre,
      contractName: contractName,
      address: dpl.address,
      metadata: metadata,
      source: metadata.sources[`flattened/${contractName}.sol`].content,
      mainnet: mainnet
    });
    console.log(`verify "${contractName}" at ${dpl.address}`, result);
  } else {
    console.log(`"${contractName}" at ${dpl.address} was verified`);
  }
}

module.exports = {
  verifier, verifyImplAndProxyHepler, verifyContractHelper,
  isVerified,
};
