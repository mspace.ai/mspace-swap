const hre = require("hardhat");

function _require(exp, exceptionMessage) {
  if (!exp) {
    throw new Error(exceptionMessage);
  }
}

const BIG_INT_ZERO = hre.ethers.BigNumber.from(0);
const ADDRESS_ZERO = hre.ethers.constants.AddressZero;
const MAX_UINT256 = hre.ethers.constants.MaxUint256;

function toBigNumber(str) {
  return hre.ethers.BigNumber.from(str.toString());
}

function toAddress(str) {
  return hre.ethers.utils.sha256(str).substring(0, 42);
}

class SafeSub {
  _safeSub(val1, val2) {
    try {
      _require(val1.gte(val2), "Subtract got negative number");
    } catch (e) {
      // console.log(val1.toString(), val2.toString());
      throw e;
    }
    return val1.sub(val2);
  }
}

class ContextMimic extends SafeSub {
  constructor() {
    super();
    this._context = {
      msg: {
        sender: ADDRESS_ZERO
      },
      block: {
        number: BIG_INT_ZERO
      }
    };
  }
  setContext({msgSender, blockNumber} = {}) {
    this._context.msg.sender = msgSender;
    this._context.block.number = blockNumber;
    return this;
  }
  _msgSender() {
    return this._context.msg.sender;
  }
  _blockNumber() {
    return this._context.block.number;
  }
}

class OwnableMimic extends ContextMimic {
  constructor() {
    super();
    this._owner = ADDRESS_ZERO;
  }
  _setOwner(_owner) {
    this._owner = _owner;
    return this;
  }
  owner() {
    return this._owner;
  }
  onlyOwner() {
    _require(this.owner() == this._msgSender(), "Ownable: caller is not the owner");
  }
  transferOwnership(newOwner) {
    this.onlyOwner();
    _require(newOwner != ADDRESS_ZERO, "Ownable: new owner is the zero address");
    this._setOwner(newOwner);
  }
}

module.exports = {
  BIG_INT_ZERO, ADDRESS_ZERO, MAX_UINT256,
  OwnableMimic, ContextMimic,
  _require,
  toAddress, toBigNumber
}