const logger = require("../logger");
const hre = require("hardhat");
const { Tokens } = require("../constants");
const { 
  HomeNetwork,
  MiningAddress, FactoryAddress, ExtractorAddress, MSpaceAddress, BankAddress,
  Multicall2Address,
} = require("../setupTestnet/_constants");
const { generatePairAddress } = require("../generatePairAddress");

async function checkGeneratePairAddress() {
  hre.changeNetwork(HomeNetwork);
  const Factory = await hre.ethers.getContractFactory("UniswapV2Factory");
  const factory = Factory.attach(FactoryAddress);
  const initCodeHash = await factory.pairCodeHash();
  const pairAddress = await factory.getPair(Tokens.DAI[12], Tokens.WMETA[12]);
  console.log("initCodeHash", initCodeHash);

  const generated1PairAddress = generatePairAddress(FactoryAddress, Tokens.DAI[12], Tokens.WMETA[12], initCodeHash);
  console.log("pairAddress", generated1PairAddress, pairAddress);
  const generated2PairAddress = generatePairAddress(FactoryAddress, Tokens.WMETA[12], Tokens.DAI[12], initCodeHash);
  console.log("pairAddress", generated2PairAddress, pairAddress);

  const Pair = await hre.ethers.getContractFactory("UniswapV2Pair");
  const pair = Pair.attach(generated1PairAddress);
  console.log(await pair.getReserves());
}

async function tryMultiCall() {
  hre.changeNetwork(HomeNetwork);
  // const Multicall = await hre.ethers.getContractFactory("Multicall");
  // const multicall = Multicall.attach(MulticallAddress);
  const Multicall = await hre.ethers.getContractFactory("Multicall2");
  const multicall = Multicall.attach(Multicall2Address);
  // const calls = [];
  // for (let i=0; i< tokens.length; i++) {
  //   const fragment = ERC20.interface.getFunction("balanceOf");
  //   const callData = ERC20.interface.encodeFunctionData(fragment, [admin.address]);
  //   const address = tokens[i];
  //   calls.push([address, callData]);
  // }
  // calls.push([
  //   MulticallAddress, 
  //   Multicall.interface.encodeFunctionData(
  //     Multicall.interface.getFunction("getMetaBalance"), 
  //     [admin.address]
  //   )
  // ]);
  // // console.log(calls);
  // const results = await multicall.callStatic.tryBlockAndAggregate(false, calls);

  // const results = await multicall.callStatic.tryBlockAndAggregate(false, [
  //   ["0x13775e938d2300f9EF692149a02007fcA446d0F4", "0x0902f1ac"],
  //   ["0x31960EAc1423f114E0eE3363BA0461E7C58F9fCC", "0x0902f1ac"],
  //   ["0x32cfa648636e5f0000A67504C122ccBA27f0812F", "0x0902f1ac"],
  //   ["0x493C26C2e5478D291Cdab499ECa21107fa284208", "0x0902f1ac"],
  //   ["0x86e0A2C26aFae1b2BD63a2689cF0E1431f666e7F", "0x0902f1ac"],
  //   ["0x91C2d6dA6b035808610b66c78A20CEC4bf3d1b63", "0x0f28c97d"],
  //   ["0x91C2d6dA6b035808610b66c78A20CEC4bf3d1b63", "0x730b44ca000000000000000000000000268ab5915e8fe079fa6d938231887b141828f472"],
  //   ["0xa76CCd51d3aB0a8eA6Cb5022fFC190084bcD91d3", "0x0902f1ac"],
  //   ["0xbd5Ec845A37BBd58C3b63Ef5b5F212Ed1e4941F1", "0x0178b8bf5a0d3f4e057f2e583a4248a0c52186f673c0c14d7c9d504ed2bbb82c3e06a204"],
  // ]);
  // console.log(results);
}

async function printTokenInfo() {
  hre.changeNetwork(HomeNetwork);
  const tokens = [
    Tokens.WETH[12], Tokens.WBTC[12], Tokens.WMETA[12], Tokens.DAI[12], Tokens.USDC[12],
    MSpaceAddress, BankAddress,
  ];
  const ERC20 = await hre.ethers.getContractFactory("ERC20Mock");
  const Factory = await hre.ethers.getContractFactory("UniswapV2Factory");
  const factory = Factory.attach(FactoryAddress);
  for (let i=0; i< tokens.length; i++) {
    const erc20 = ERC20.attach(tokens[i]);
    console.log(tokens[i], await erc20.symbol(), await erc20.name(), await erc20.decimals());
    console.log("WMETA pair", await factory.getPair(tokens[i], Tokens.WMETA[12]));
    console.log("MSP pair", await factory.getPair(tokens[i], MSpaceAddress));
  }
}

async function getEvent() {
  hre.changeNetwork(HomeNetwork);
  const Mining = await hre.ethers.getContractFactory("MiningHill");
  const mining = Mining.attach(MiningAddress);

  const poolLength = await mining.poolLength();
  for(let i=0; i<poolLength; i++) {
    console.log(
      hre.ethers.utils.formatUnits(await mining.pendingMSpace(i, "0x268AB5915E8fe079fA6D938231887B141828F472"), 18),
      hre.ethers.utils.formatUnits(await mining.rewardDebt(i), 18)
    );
  }

  // const events = await mining.queryFilter("Deposit", 16646682, 16646908);
  // // const events = await mining.queryFilter("Deposit", "0xac5a3fd0c3be0accf51dfe457b38a776f7753694d05325c5bd85bb11d2ffa8ed");
  // console.log(events.map(event => event.blockNumber));
}

async function main() {
  // await printTokenInfo();
  // await tryMultiCall();
  // await checkGeneratePairAddress();
  // await getEvent();

  hre.changeNetwork(HomeNetwork);
  const Factory = await hre.ethers.getContractFactory("UniswapV2Factory");
  const factory = Factory.attach(FactoryAddress);
  let pairAddr = await factory.getPair(Tokens.USDC[12], MSpaceAddress);
  console.log("USDC-MSP", pairAddr);
  pairAddr = await factory.getPair(Tokens.DAI[12], Tokens.WMETA[12]);
  console.log("DAI-META", pairAddr);
  pairAddr = await factory.getPair(Tokens.USDC[12], Tokens.WMETA[12]);
  console.log("USDC-META", pairAddr);

  // const ERC20 = await hre.ethers.getContractFactory("ERC20Mock");
  // const erc20 = ERC20.attach("0x38F092fDBE5aD97977d66b8AEe9B6EEe155b3fEd");
  // console.log(await erc20.balanceOf("0x268AB5915E8fe079fA6D938231887B141828F472"));

}

if (require.main === module) {
  main()
    .then(() => process.exit(0))
    .catch((error) => {
      console.error(error);
      process.exit(1);
    });
}
