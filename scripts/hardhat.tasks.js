const fs = require('fs');

// WARNING the name of contracts must unique or task flatten:custom will mixup their sources
const mainContracts = [
  "dao/Timelock.sol",
  "extra/WMETA.sol",
  "helpers/BancorConverterRegistry.sol",
  "helpers/BoringHelper.sol",
  "helpers/ENSRegistry.sol",
  "helpers/ENSRegistryWithFallback.sol",
  "helpers/Multicall2.sol",

  "mock/DAI/DaiMock.sol",
  "mock/WBTC/WBTCMock.sol",
  "mock/ContractMockV1.sol",
  "mock/ContractMockV2.sol",
  "mock/ERC20Mock.sol",
  "mock/FiatTokenV2_1.sol",
  "mock/MintPlanMock.sol",
  "mock/UniswapV2Router02Mock.sol",
  "mock/WETHMock.sol",
  "mock/WMETAMock.sol",
  "mock/PairMockV2000.sol",

  "proxy/UpgradeableProxy.sol",

  "stake/MiningHill.sol",
  "stake/MiningPoolMigrator.sol",
  "stake/Extractor.sol",
  "stake/MSpaceToken.sol",
  "stake/Bank.sol",
  
  "uniswapv2/UniswapV2ERC20.sol",
  "uniswapv2/UniswapV2Factory.sol",
  "uniswapv2/UniswapV2Pair.sol",
  "uniswapv2/UniswapV2Router02.sol",
]

const flatten = async(sourceDir) => {
  try { 
    fs.rmdirSync(sourceDir, {recursive: true}); 
    console.log(`clean folder ${sourceDir}`); 
  } catch (_ignore) {}
  fs.mkdirSync(sourceDir);

  let removeMultiLicenses = function(source) {
    let lines = source.split("\n");
    let isLicenseLines = lines.map((line) => line.indexOf("SPDX-License-Identifier") != -1 ? true:false);
    let lastLicenseLine = -1;
    for (let i=0; i<isLicenseLines.length; i++) {
      if (isLicenseLines[i] === true) {
        lastLicenseLine = i; break;
      }
    }
    let newLines = [];
    let addedLicense = false;
    for (let i=0; i<isLicenseLines.length; i++) {
      if (isLicenseLines[i] === false) {
        newLines.push(lines[i]);
      } else {
        if (addedLicense === false) {
          newLines.push(lines[lastLicenseLine]);
          addedLicense = true;
        }
      }
    }
    return newLines.join("\n");
  }
  
  let promies = mainContracts.map(async(contractPath) => {
    let source = await run("flatten:get-flattened-sources", {files: [`contracts/${contractPath}`]});
    source = removeMultiLicenses(source);
    let fileName = contractPath.split("/").pop();
    fs.writeFileSync(`${sourceDir}/${fileName}`, source);
  });
  await Promise.all(promies);
  console.log(`flattened ${mainContracts.length} files from sources to ${sourceDir}`);
}

const deploy = async(taskArgs, hre, runSuper) => {
  // await run("flatten:custom");
  console.log("NOTE: before run task `deploy` should run task `flatten` to update code in `flattened` folder");
  
  process.env.TEAM_WALLET ?? console.log("WARN: you should set env TEAM_WALLET or it will be admin address");
  process.env.OPERATION_WALLET ?? console.log("WARN: you should set env OPERATION_WALLET or it will be admin address");
  process.env.TREASURY_WALLET ?? console.log("WARN: you should set env TREASURY_WALLET or it will be admin address");
  process.env.AIRDROP_WALLET ?? console.log("WARN: you should set env AIRDROP_WALLET or it will be admin address");

  process.env.BLOCK_PER_MONTH ?? console.log("WARN: you should set env BLOCK_PER_MONTH for MiningHill or it will be 576000 for metadium testnet | 2592000 for metadium");

  process.env.START_REWARD_BLOCK ?? console.log("WARN: you should set env START_REWARD_BLOCK for MiningHill or it will be currentBlockNumber");

  await runSuper(taskArgs, hre);
}


module.exports = {
  flatten, deploy
}