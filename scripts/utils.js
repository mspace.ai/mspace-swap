const { time } = require("@openzeppelin/test-helpers");
const colors = require('ansi-colors');

const advanceTime = async (time) => {
  await ethers.provider.send("evm_increaseTime", [time]);
  await ethers.provider.send("evm_mine");

  return Promise.resolve(ethers.provider.getBlock('latest'));
}

const advanceBlock = async (blocks) => {
  if (blocks >= 10000) {
    console.log(`${colors.black.bgYellow('WARN')} travel through many blocks maybe slow`);
  }
  for (let i=0; i < blocks; i++) {
    await ethers.provider.send("evm_mine");
  }
  return Promise.resolve(ethers.provider.getBlock('latest'));
}

const currentTime = async () => {
  return (await time.latest()).toNumber();
}
const currentBlock = async () => {
  return (await time.latestBlock()).toNumber();
}

function toBN(str) {
  const hre = require("hardhat");
  return hre.ethers.BigNumber.from(str.toString());
}

function generateDecayPoints(startPoint, numerator, denominator, length) {
  const points = [];
  for (let i=0; i<length; i++) {
    // decay formula a*b^x
    points.push(startPoint.mul(numerator.pow(i)).div(denominator.pow(i)));
  }
  return points;
}

function timeout(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

function toText(amount, decimal) {
  const hre = require("hardhat");
  return hre.ethers.utils.formatUnits(amount, decimal);
}


module.exports = {
  advanceTime, advanceBlock, currentBlock, currentTime, generateDecayPoints, toBN, toText, timeout
}