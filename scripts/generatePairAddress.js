const { arrayify, concat, hexDataSlice, isHexString, hexDataLength } = require("@ethersproject/bytes");
const { _base36To16 } = require("@ethersproject/bignumber");
const { keccak256 } = require("@ethersproject/keccak256");
const { pack, keccak256: keccak256_solidity } = require('@ethersproject/solidity');

// code from swap website
function getChecksumAddress(address) {
  if (!isHexString(address, 20)) {
    throw new Error("invalid address", "address", address);
  }

  address = address.toLowerCase();

  const chars = address.substring(2).split("");

  const expanded = new Uint8Array(40);
  for (let i = 0; i < 40; i++) {
    expanded[i] = chars[i].charCodeAt(0);
  }

  const hashed = arrayify(keccak256(expanded));

  for (let i = 0; i < 40; i += 2) {
    if ((hashed[i >> 1] >> 4) >= 8) {
      chars[i] = chars[i].toUpperCase();
    }
    if ((hashed[i >> 1] & 0x0f) >= 8) {
      chars[i + 1] = chars[i + 1].toUpperCase();
    }
  }

  return "0x" + chars.join("");
}

function getAddress(address) {
  let result = null;

  if (typeof (address) !== "string") {
    throw new Error("invalid address", "address", address);
  }

  if (address.match(/^(0x)?[0-9a-fA-F]{40}$/)) {

    // Missing the 0x prefix
    if (address.substring(0, 2) !== "0x") { address = "0x" + address; }

    result = getChecksumAddress(address);

    // It is a checksummed address with a bad checksum
    if (address.match(/([A-F].*[a-f])|([a-f].*[A-F])/) && result !== address) {
      throw new Error("bad address checksum", "address", address);
    }

    // Maybe ICAP? (we only support direct mode)
  } else if (address.match(/^XE[0-9]{2}[0-9A-Za-z]{30,31}$/)) {

    // It is an ICAP address with a bad checksum
    if (address.substring(2, 4) !== ibanChecksum(address)) {
      throw new Error("bad icap checksum", "address", address);
    }

    result = _base36To16(address.substring(4));
    while (result.length < 40) { result = "0" + result; }
    result = getChecksumAddress("0x" + result);

  } else {
    throw new Error("invalid address", "address", address);
  }

  return result;
}

function getCreate2Address(from, salt, initCodeHash) {
  if (hexDataLength(salt) !== 32) {
    throw new Error("salt must be 32 bytes", "salt", salt);
  }
  if (hexDataLength(initCodeHash) !== 32) {
    throw new Error("initCodeHash must be 32 bytes", "initCodeHash", initCodeHash);
  }
  return getAddress(hexDataSlice(keccak256(concat(["0xff", from, salt, initCodeHash])), 12))
}

function generatePairAddress(factory, token1, token2, initCodeHash) {
  return getCreate2Address(
    factory,
    keccak256_solidity(
      ['bytes'],
      [pack(
        ['address', 'address'],
        [token1, token2]
      )]
    ),
    initCodeHash
  );
}

module.exports = {
  generatePairAddress
}