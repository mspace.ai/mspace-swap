const { callMulti } = require("./_utils");
const { ProxyArtifact, Tokens } = require("../constants");

async function _upgradePair(factory, proxy, newImplAddress, token0, token1) {
  const token0Symbol = await token0.symbol();
  const token1Symbol = await token1.symbol();
  const pairText = `${token0Symbol}-${token1Symbol}`;
  const impl = await proxy.implementation();
  if (impl !== newImplAddress) {
    const tx = await (await factory.upgradePair(token0.address, token1.address)).wait();
    console.log(`upgraded pair: ${pairText} tx: ${tx.transactionHash}`);
  } else {
    console.log(`pair: ${pairText} was upgraded`);
  }
}

async function upgradePairByAddress(hre, address, newImplAddress) {
  const {factory, Pair, ERC20, Proxy} = await _getContracts(hre);
  const pair = Pair.attach(address);
  const token0Address = await pair.token0();
  const token1Address = await pair.token1();
  await _upgradePair(factory, Proxy.attach(address), newImplAddress, ERC20.attach(token0Address), ERC20.attach(token1Address));
}

async function upgradePairByTokens(hre, token0, token1, newImplAddress) {
  const network = await hre.ethers.provider.getNetwork();
  const token0Address = Tokens[token0] === undefined ? token0 : Tokens[token0][network.chainId];
  const token1Address = Tokens[token1] === undefined ? token1 :Tokens[token1][network.chainId];

  const {factory, Pair, ERC20, Proxy} = await _getContracts(hre);
  const pairAddress = await factory.getPair(token0Address, token1Address);
  if (pairAddress === hre.ethers.constants.AddressZero) {
    console.log("can not find pair address");
    return;
  }
  await _upgradePair(factory, Proxy.attach(pairAddress), newImplAddress, ERC20.attach(token0Address), ERC20.attach(token1Address));
}

async function _getContracts(hre) {
  const factory = await hre.ethers.getContract("UniswapV2Factory");
  const Pair = await hre.ethers.getContractFactory("UniswapV2Pair");
  const ERC20 = await hre.ethers.getContractFactory("ERC20Mock");
  const Proxy = await hre.ethers.getContractFactory(ProxyArtifact);
  return {factory, Pair, ERC20, Proxy};
}

async function upgradePairs({pairs}, hre) {
  const {factory, Pair, ERC20, Proxy} = await _getContracts(hre);
  const newImpl = await hre.ethers.getContract("UniswapV2Pair");
  const oldImpl = await factory.pairImplement();
  if (newImpl.address === oldImpl) {
    console.log(`PairImplement aready version: "${await newImpl.version()}" try upgrade pairs...`);
  } else {
    let tx = await (await factory.setPairImplement(newImpl.address)).wait();
    console.log(`set PairImplement to version: "${await newImpl.version()}" tx: ${tx.transactionHash}`);
  }

  if (typeof pairs === "string") {
    const addresses = pairs.split(",");
    addresses.length === 2 ?
      await upgradePairByTokens(hre, addresses[0], addresses[1], newImpl.address)
      : await upgradePairByAddress(hre, addresses[0], newImpl.address);
  } else {
    const multicall = await hre.ethers.getContract("Multicall2");
    const pairsCount = await factory.allPairsLength();
    const pairsIndex = Array.from(Array(pairsCount.toNumber()).keys());
    const chunk = 10;
    for (let index = 0; index<pairsIndex.length; index+=chunk) {
      const batch = pairsIndex.slice(index, index+chunk);
      const pairsResults = await callMulti(multicall, batch.map(b=>factory), batch.map(b=>"allPairs"), batch.map(b => [b]), true);
      const pairsContract = pairsResults.map(r => Pair.attach(r[0]));
      const token0Results = await callMulti(multicall, pairsContract, batch.map(b=>"token0"), pairsContract.map(p=>[]), true);
      const token1Results = await callMulti(multicall, pairsContract, batch.map(b=>"token1"), pairsContract.map(p=>[]), true);
      for (let i=0; i<batch.length; i++) {
        await _upgradePair(factory, Proxy.attach(pairsContract[i].address), newImpl.address, ERC20.attach(token0Results[i][0]), ERC20.attach(token1Results[i][0]));
      }
    }
  }
}

module.exports = {
  upgradePairs
}