const { Tokens } = require("../constants");
const { HomeNetwork, FactoryAddress, PairImplAddress, MSpaceAddress } = require("../setupTestnet/_constants");
const { isVerified, verifier } =  require("../deployHelper");

async function verifyPairs(args, hre) {
  const network = await hre.ethers.provider.getNetwork();
  const factory = await hre.ethers.getContract("UniswapV2Factory");
  const pairLength = await factory.allPairsLength();

  for (let i=0; i < pairLength; i++) {
    const pairAddress = await factory.allPairs(i);
    if (!(await isVerified({address: pairAddress, mainnet: network.chainId === 11}))) {
      const buildInfo = await hre.artifacts.getBuildInfo("flattened/UpgradeableProxy.sol:UpgradeableProxy");
      let metadata = JSON.parse(
        buildInfo.output
          .contracts["flattened/UpgradeableProxy.sol"]["UpgradeableProxy"].metadata
      );
      let result = await verifier({
        hre: hre,
        contractName: "UpgradeableProxy",
        address: pairAddress,
        metadata: metadata,
        source: metadata.sources["flattened/UpgradeableProxy.sol"].content,
        mainnet: network.chainId === 11
      });
      console.log(`verify Pair_Proxy="${pairAddress}"`, result);
    } else {
      console.log(`Pair_Proxy="${pairAddress}" was verified`);
    }
  }
}

module.exports = {
  verifyPairs
}