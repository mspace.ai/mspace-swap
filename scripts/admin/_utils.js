async function callMulti(multicall, contracts, funcNames, inputs, static) {
  const fragments = contracts.map((c, i) => c.interface.getFunction(funcNames[i]));
  const calls = contracts.map( (c, i) => [c.address, c.interface.encodeFunctionData(fragments[i], inputs[i])]);
  if (static === true) {
    const results = await multicall.callStatic.tryAggregate(false, calls);
    return results.map((r,i) => contracts[i].interface.decodeFunctionResult(fragments[i], r[1]));
  } else {
    return (await (await multicall.tryAggregate(true, calls)).wait()).transactionHash;
  }
}

module.exports = {
  callMulti
}