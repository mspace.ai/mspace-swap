const { Tokens } = require("../constants"); 
const { callMulti } = require("./_utils");

async function _unlockPair(factory, pair, token0, token1) {
  const token0Symbol = await token0.symbol();
  const token1Symbol = await token1.symbol();
  const pairText = `${token0Symbol}-${token1Symbol}`;
  const unlocked = await pair.unlocked();
  if (unlocked.eq(0)) {
    const tx = await (await factory.unlockPair(token0.address, token1.address)).wait();
    console.log(`unlocked pair: ${pairText} tx: ${tx.transactionHash}`);
  } else {
    console.log(`pair: ${pairText} was unlocked`);
  }
}

async function unlockPairByAddress(hre, address) {
  const {factory, Pair, ERC20} = await _getContracts(hre);
  const pair = Pair.attach(address);
  const token0Address = await pair.token0();
  const token1Address = await pair.token1();
  await _unlockPair(factory, pair, ERC20.attach(token0Address), ERC20.attach(token1Address));
}

async function unlockPairByTokens(hre, token0, token1) {
  const network = await hre.ethers.provider.getNetwork();
  const token0Address = Tokens[token0] === undefined ? token0 : Tokens[token0][network.chainId];
  const token1Address = Tokens[token1] === undefined ? token1 :Tokens[token1][network.chainId];

  const {factory, Pair, ERC20} = await _getContracts(hre);
  const pairAddress = await factory.getPair(token0Address, token1Address);
  if (pairAddress === hre.ethers.constants.AddressZero) {
    console.log("can not find pair address");
    return;
  }
  const pair = Pair.attach(pairAddress);
  await _unlockPair(factory, pair, ERC20.attach(token0Address), ERC20.attach(token1Address));
}

async function _getContracts(hre) {
  const factory = await hre.ethers.getContract("UniswapV2Factory");
  const Pair = await hre.ethers.getContractFactory("UniswapV2Pair");
  const ERC20 = await hre.ethers.getContractFactory("ERC20Mock");
  return {factory, Pair, ERC20};
}

async function unlockPairs({pairs}, hre) {
  if (typeof pairs === "string") {
    const addresses = pairs.split(",");
    addresses.length === 2 ?
      await unlockPairByTokens(hre, addresses[0], addresses[1])
      : await unlockPairByAddress(hre, addresses[0]);
  } else {
    const {factory, Pair, ERC20} = await _getContracts(hre);
    const pairsCount = await factory.allPairsLength();
    const pairsIndex = Array.from(Array(pairsCount.toNumber()).keys());

    const multicall = await hre.ethers.getContract("Multicall2");
    const chunk = 10;
    for (let index = 0; index<pairsIndex.length; index+=chunk) {
      const batch = pairsIndex.slice(index, index+chunk);
      const pairsResults = await callMulti(multicall, batch.map(b=>factory), batch.map(b=>"allPairs"), batch.map(b => [b]), true);
      const pairsContract = pairsResults.map(r => Pair.attach(r[0]));
      const token0Results = await callMulti(multicall, pairsContract, batch.map(b=>"token0"), pairsContract.map(p=>[]), true);
      const token1Results = await callMulti(multicall, pairsContract, batch.map(b=>"token1"), pairsContract.map(p=>[]), true);

      for (let i=0; i<batch.length; i++) { 
        await _unlockPair(factory, pairsContract[i], ERC20.attach(token0Results[i][0]), ERC20.attach(token1Results[i][0]));
      }
    }
  }
}

module.exports = {
  unlockPairs
}