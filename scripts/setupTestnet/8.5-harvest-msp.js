const hre = require("hardhat");
const logger = require("../logger");
const { HomeNetwork, BankAddress, MSpaceAddress } = require("./_constants");

async function main() {
  hre.changeNetwork(HomeNetwork);
  const signers = await hre.ethers.getSigners();
  const lp1 = signers[2];

  const Bank = await hre.ethers.getContractFactory("Bank");
  const bank = Bank.attach(BankAddress);

  const xstarBalance = await bank.balanceOf(lp1.address);
  const withDrawAmount = Math.floor(Math.random() * 500);
  const amount = xstarBalance.mul(withDrawAmount).div(1000);
  
  const ERC20 = await hre.ethers.getContractFactory("ERC20Mock");
  const mspace = ERC20.attach(MSpaceAddress);
  const oldBalance = await mspace.balanceOf(lp1.address);
  await (await bank.connect(lp1).leave(amount)).wait();
  const newBalance = await mspace.balanceOf(lp1.address);
  logger.info(
    lp1.address, "leave", 
    hre.ethers.utils.formatUnits(amount, 18), "xMSP", 
    "got", 
    hre.ethers.utils.formatUnits(newBalance.sub(oldBalance), 18), "MSP"
  );
}

if (require.main === module) {
  main()
    .then(() => process.exit(0))
    .catch((error) => {
      console.error(error);
      process.exit(1);
    });
}