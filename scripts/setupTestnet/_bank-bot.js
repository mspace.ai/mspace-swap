const CronJob = require('cron').CronJob;
const { mintFee } = require("./8.0-mint-fee.js");
const { convertSwapFee } = require("./8.3-convert.js");
const { updateBank } = require("./8.4-update-bank.js");

const cronTime = "0 0 */6 * * *";
const job = new CronJob(cronTime,  async() => {
  try {
    await mintFee();
    await convertSwapFee();
    await updateBank();
  } catch (error) {
    console.error(error);
  }
}, null, true);
job.start();