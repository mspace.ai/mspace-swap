const hre = require("hardhat");
const logger = require("../logger");
const { Tokens } = require("../constants");
const { 
  HomeNetwork, FactoryAddress, ExtractorAddress,
  WETH_WMETA_RATE, WETH_WMETA_RATE_DEC,
  WBTC_WMETA_RATE, WBTC_WMETA_RATE_DEC,
  USDC_WMETA_RATE, USDC_WMETA_RATE_DEC,
  DAI_WMETA_RATE, DAI_WMETA_RATE_DEC,
} = require("./_constants");
const { withdrawFromSwap } = require("./_withdraw");
const { addAllToWMETAPool } = require("./4.0-add-swap-pools");

async function changeMetaPoolAndCheckFee(user, tokenAddress, rate, rateDec) {
  const Factory = await hre.ethers.getContractFactory("UniswapV2Factory");
  const factory = Factory.attach(FactoryAddress);
  const Pair = await hre.ethers.getContractFactory("UniswapV2Pair");
  const pairAddress = await factory.getPair(tokenAddress, Tokens.WMETA[12]);
  const pair = Pair.attach(pairAddress);
  const mlpBalance = await pair.balanceOf(user.address);

  const ERC20 = await hre.ethers.getContractFactory("ERC20Mock");
  const erc20 = ERC20.attach(tokenAddress);
  const symbol = await erc20.symbol();
  const tkBalance = await erc20.balanceOf(user.address);

  const percent = Math.floor(Math.random() * 100)
  if (mlpBalance.eq(0)) {
    if (tkBalance.eq(0)) {
      logger.warn(user.address, "not have lps &", symbol, "tokens for change lp pool");
    } else {
      await addAllToWMETAPool(user, tokenAddress, rate, rateDec, percent);
    }
  } else if(tkBalance.eq(0)){
    await withdrawFromSwap(user, tokenAddress, Tokens.WMETA[12], percent);
  } else {
    Math.random() > 0.5 ?
      (await addAllToWMETAPool(user, tokenAddress, rate, rateDec, percent))
      : (await withdrawFromSwap(user, tokenAddress, Tokens.WMETA[12], percent))
  }

  logger.info(`Extractor have`, (await pair.balanceOf(ExtractorAddress)).toString(), `MPs in ${symbol}-WMETA`, );
}

async function mintFee() {
  hre.changeNetwork(HomeNetwork);
  const signers = await hre.ethers.getSigners();
  const admin = signers[0];
  await changeMetaPoolAndCheckFee(admin, Tokens.WETH[12], WETH_WMETA_RATE, WETH_WMETA_RATE_DEC);
  await changeMetaPoolAndCheckFee(admin, Tokens.WBTC[12], WBTC_WMETA_RATE, WBTC_WMETA_RATE_DEC);
  await changeMetaPoolAndCheckFee(admin, Tokens.USDC[12], USDC_WMETA_RATE, USDC_WMETA_RATE_DEC);
  await changeMetaPoolAndCheckFee(admin, Tokens.DAI[12], DAI_WMETA_RATE, DAI_WMETA_RATE_DEC);
}

if (require.main === module) {
  mintFee()
    .then(() => process.exit(0))
    .catch((error) => {
      console.error(error);
      process.exit(1);
    });
}

module.exports = {
  mintFee
}