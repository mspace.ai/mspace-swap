const hre = require("hardhat");
const logger = require("../logger");
const { Tokens } = require("../constants");
const { 
  HomeNetwork, FactoryAddress, RouterAddress,
  WETH_WMETA_RATE, WETH_WMETA_RATE_DEC,
  WBTC_WMETA_RATE, WBTC_WMETA_RATE_DEC,
  DAI_WMETA_RATE, DAI_WMETA_RATE_DEC,
  USDC_WMETA_RATE, USDC_WMETA_RATE_DEC
} = require("./_constants");
const { toBN } = require("../utils");

async function addAllToWMETAPool(user, tkAddress, rate, rateDecimal, percent) {
  const ERC20 = await hre.ethers.getContractFactory("ERC20Mock");

  percent = Number.isInteger(percent) && percent > 0 && percent <= 100 ? percent:100;
  const tk = ERC20.attach(tkAddress).connect(user);
  const amount = (await tk.balanceOf(user.address)).mul(percent).div(100);
  if (amount.eq(0)) {
    logger.info("can't not add liquidity because amount is 0");
    return;
  }
  const tkSymbol = await tk.symbol();
  const tkDecimals = await tk.decimals();

  // const WMETA = await hre.ethers.getContractFactory("WMETAMock");
  // const wmeta = WMETA.attach(Tokens.WMETA[12]).connect(user);
  // const wmetaBalance = await wmeta.balanceOf(user.address);
  const metaBalance = await hre.ethers.provider.getBalance(user.address);

  // TODO fix this if amount need too low this will be equal 0
  const need = amount.mul(toBN(10).pow(toBN(18).sub(tkDecimals))).mul(rate).div(rateDecimal);
  if (metaBalance.lt(need)) {
    logger.warn(user.address, "not have enough META for add to pool", metaBalance.toString(), need.toString());
    return;
    // logger.info("try to mint WMETA");
    // await (await wmeta.mint(user.address, need.sub(metaBalance))).wait();
  }

  await (await tk.approve(RouterAddress, amount)).wait();
  // await (await wmeta.approve(RouterAddress, need)).wait();

  const Router = await hre.ethers.getContractFactory("UniswapV2Router02");
  const router = Router.attach(RouterAddress).connect(user);
  logger.info("add to pool", `${tkSymbol}-META`, amount.toString(), need.toString());
  await (await router.connect(user).addLiquidityMETA(
    tkAddress,
    amount, 0, 0,
    user.address,
    Math.floor(new Date().getTime()/1000) + 3600,
    {value: need}
  )).wait();
  const Factory = await hre.ethers.getContractFactory("UniswapV2Factory");
  const factory = Factory.attach(FactoryAddress);
  const pairAddress = await factory.getPair(tkAddress, Tokens.WMETA[12]);
  const Pair = await hre.ethers.getContractFactory("UniswapV2Pair");
  const pair = Pair.attach(pairAddress);
  logger.info("got MP", (await pair.balanceOf(user.address)).toString());
}

async function checkPairRate(tk0Address, tk1Address) {
  const ERC20  = await hre.ethers.getContractFactory("ERC20Mock");
  const tk0 = ERC20.attach(tk0Address);
  const tk1 = ERC20.attach(tk1Address);
  const tk0Symbol = await tk0.symbol();
  const tk1Symbol = await tk1.symbol();

  const Router = await hre.ethers.getContractFactory("UniswapV2Router02"); 
  const router = Router.attach(RouterAddress);
  const [amount0In, amount1Out] = await router.getAmountsOut(toBN(1).mul(toBN(10).pow(await tk0.decimals())), [tk0Address, tk1Address]);
  const [amount1In, amount0Out] = await router.getAmountsOut(toBN(1).mul(toBN(10).pow(await tk1.decimals())), [tk1Address, tk0Address]);

  const Factory = await hre.ethers.getContractFactory("UniswapV2Factory");
  const factory = Factory.attach(FactoryAddress);
  const pairAddress = await factory.getPair(tk0Address, tk1Address);

  logger.info(
    "pair",
    `${tk0Symbol}/${tk1Symbol}`,
    hre.ethers.utils.formatUnits(amount1Out.toString(), await tk1.decimals()),
    pairAddress
    // `${tk1Symbol}/${tk0Symbol}`,
    // hre.ethers.utils.formatUnits(amount0Out.toString(), await tk0.decimals()),
  );
}

async function main() {
  hre.changeNetwork(HomeNetwork);
  // hre.changeNetwork("ether1");

  const signers = await hre.ethers.getSigners();
  const lp1 = signers[2];
  const lp2 = signers[3];
  const lp3 = signers[4];

  await addAllToWMETAPool(lp1, Tokens.WETH[12], WETH_WMETA_RATE, WETH_WMETA_RATE_DEC);
  await addAllToWMETAPool(lp1, Tokens.WBTC[12], WBTC_WMETA_RATE, WBTC_WMETA_RATE_DEC);
  await addAllToWMETAPool(lp1, Tokens.DAI[12], DAI_WMETA_RATE, DAI_WMETA_RATE_DEC);
  await addAllToWMETAPool(lp1, Tokens.USDC[12], USDC_WMETA_RATE, USDC_WMETA_RATE_DEC);

  await addAllToWMETAPool(lp2, Tokens.WETH[12], WETH_WMETA_RATE, WETH_WMETA_RATE_DEC);
  await addAllToWMETAPool(lp2, Tokens.WBTC[12], WBTC_WMETA_RATE, WBTC_WMETA_RATE_DEC);
  await addAllToWMETAPool(lp2, Tokens.DAI[12], DAI_WMETA_RATE, DAI_WMETA_RATE_DEC);
  await addAllToWMETAPool(lp2, Tokens.USDC[12], USDC_WMETA_RATE, USDC_WMETA_RATE_DEC);

  await addAllToWMETAPool(lp3, Tokens.WETH[12], WETH_WMETA_RATE, WETH_WMETA_RATE_DEC);
  await addAllToWMETAPool(lp3, Tokens.WBTC[12], WBTC_WMETA_RATE, WBTC_WMETA_RATE_DEC);
  await addAllToWMETAPool(lp3, Tokens.DAI[12], DAI_WMETA_RATE, DAI_WMETA_RATE_DEC);
  await addAllToWMETAPool(lp3, Tokens.USDC[12], USDC_WMETA_RATE, USDC_WMETA_RATE_DEC);

  await checkPairRate(Tokens.WETH[12], Tokens.WMETA[12]);
  await checkPairRate(Tokens.WBTC[12], Tokens.WMETA[12]);
  await checkPairRate(Tokens.DAI[12], Tokens.WMETA[12]);
  await checkPairRate(Tokens.USDC[12], Tokens.WMETA[12]);
}

if (require.main === module) {
  main()
    .then(() => process.exit(0))
    .catch((error) => {
      console.error(error);
      process.exit(1);
    });
}

module.exports = {
  addAllToWMETAPool
}