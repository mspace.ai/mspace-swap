const hre = require("hardhat");
const logger = require("../logger");
const { Tokens } = require("../constants");
const { HomeNetwork, RouterAddress, FactoryAddress, MiningAddress } = require("./_constants");

async function withdrawFromSwap(user, tk0, tk1, percent) {
  const Router = await hre.ethers.getContractFactory("UniswapV2Router02");
  const Factory = await hre.ethers.getContractFactory("UniswapV2Factory");
  const Pair = await hre.ethers.getContractFactory("UniswapV2Pair");

  const router = Router.attach(RouterAddress).connect(user);
  const factory = Factory.attach(FactoryAddress);
  const pairAddress = await factory.getPair(tk0, tk1);
  const pair = Pair.attach(pairAddress);

  percent = Number.isInteger(percent) && percent > 0 && percent <= 100 ? percent:100;
  const amount = (await pair.balanceOf(user.address)).mul(percent).div(100);
  if (amount.eq(0)) {
    logger.warn("user", user.address, "not have lps to withdraw");
    return
  }

  await (await pair.connect(user).approve(router.address, amount)).wait();
  // check
  if (tk0 === Tokens.WMETA[12]) {
    await (await router.removeLiquidityMETA(
      tk1, amount, 0, 0, user.address, 
      Math.floor(new Date().getTime()/1000) + 3600
    )).wait();
  } else if (tk1 === Tokens.WMETA[12]) {
    await (await router.removeLiquidityMETA(
      tk0, amount, 0, 0, user.address, 
      Math.floor(new Date().getTime()/1000) + 3600
    )).wait();
  } else {
    await (await router.removeLiquidity(
      tk0, tk1, amount, 0, 0, user.address, 
      Math.floor(new Date().getTime()/1000) + 3600
    )).wait();
  }
  logger.info("user", user.address, "draw", amount.toString(), "from pool", pairAddress);
}

async function withdrawFromMining(user){
  const Mining = await hre.ethers.getContractFactory("MiningHill");
  const mining = Mining.attach(MiningAddress);
  const length = await mining.poolLength();
  for (let pool=0; pool<length; pool++) {
    const userInfo = await mining.userInfo(pool, user.address);
    if (userInfo["amount"].eq(0)) {
      logger.warn(user.address, "not have lps of pool", pool);
    } else {
      await (await mining.connect(user).withdraw(pool, userInfo["amount"])).wait();
      logger.info(user.address, "withdraw from mining pool", pool, userInfo["amount"].toString());
    }
  }
}

async function main() {
  hre.changeNetwork(HomeNetwork);
  const signers = await hre.ethers.getSigners();
  const users = [
    signers[0], // admin
    signers[2], // lp1
    signers[3], // lp2
    signers[4], // lp3
  ];

  const tokens = [Tokens.WETH[12], Tokens.WBTC[12], Tokens.USDC[12], Tokens.DAI[12]];
  

  for (let i=0; i<users.length; i++) {
    await withdrawFromMining(users[i]);
  }

  for (let i=0; i<users.length; i++) {
    for (let k=0; k<tokens.length; k++) {
      await withdrawFromSwap(users[i], tokens[k], Tokens.WMETA[12]);
    }
  }
}

if (require.main === module) {
  main()
    .then(() => process.exit(0))
    .catch((error) => {
      console.error(error);
      process.exit(1);
    });
}

module.exports = {
  withdrawFromSwap
}