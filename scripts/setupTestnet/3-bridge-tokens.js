const hre = require("hardhat");
const logger = require("../logger");
const { Tokens } = require("../constants");
const { HomeMediatorAddress, ForeignMediatorAddress, HomeNetwork, ForeignNetwork } = require("./_constants");
const { toBN, timeout } = require("../utils");

const HOME_MEDIATOR = require("../build/HomeOmnibridge.json");
const FOREIGN_MEDIATOR = require("../build/ForeignOmnibridge.json");
const ERC677_TOKEN = require("../build/ERC677.json");

async function bridgeAllTokens(user, TokenAddress) {
  hre.changeNetwork(HomeNetwork);
  
  const ERC677 = await hre.ethers.getContractFactory(ERC677_TOKEN.abi, ERC677_TOKEN.bytecode); 
  const HomeMediator = await hre.ethers.getContractFactory(HOME_MEDIATOR.abi, HOME_MEDIATOR.bytecode);
  const homeMediator = HomeMediator.attach(HomeMediatorAddress);
  // logger.info(await homeMediator.homeTokenAddress(TokenAddress));

  let homeERC677;

  async function getHomeBalance(userAddress) {
    if (homeERC677 !== undefined) {
      return await homeERC677.balanceOf(userAddress);
    }
    const HomeErc677Address = await homeMediator.homeTokenAddress(TokenAddress);
    if (HomeErc677Address !== hre.ethers.constants.AddressZero) {
      homeERC677 = ERC677.attach(HomeErc677Address);
      return await homeERC677.balanceOf(userAddress);
    }
    return toBN(0);
  }

  hre.changeNetwork(ForeignNetwork);

  const ERC20 = await hre.ethers.getContractFactory("ERC20Mock");
  const foreignERC20 = ERC20.attach(TokenAddress);
  const amount = await foreignERC20.balanceOf(user.address);
  if (amount.eq(0)) {
    logger.warn("Can not bridge token because balance is 0");
    return;
  }

  const ForeignMediator = await hre.ethers.getContractFactory(FOREIGN_MEDIATOR.abi, FOREIGN_MEDIATOR.bytecode);
  const foreignMediator = ForeignMediator.attach(ForeignMediatorAddress);

  const oldBalance = await getHomeBalance(user.address);

  logger.info("Home balance:", oldBalance.toString());
  logger.info("Foreign balance:", (await foreignERC20.balanceOf(user.address)).toString());

  const txApprove = await foreignERC20.connect(user).approve(ForeignMediatorAddress, amount);
  logger.info("approve", txApprove.hash);
  await txApprove.wait();

  const tx = await foreignMediator.connect(user)["relayTokens(address,uint256)"](foreignERC20.address, amount);
  logger.info("tx", tx.hash);
  await tx.wait();

  let newBalance = oldBalance;
  while(newBalance.eq(oldBalance)) {
    logger.info("checking...");
    newBalance = await getHomeBalance(user.address);
    await timeout(15000);
  }

  logger.info("Home balance:", newBalance.toString());
  logger.info("Foreign balance:", (await foreignERC20.balanceOf(user.address)).toString());
  logger.info("home erc677", await homeMediator.homeTokenAddress(TokenAddress));
}

async function bridge(user) {
  logger.info(`#### start bridge for user ${user.address}`);
  logger.info(`** bridge usdc from foreign(${ForeignNetwork}) to home(${HomeNetwork})`);
  await bridgeAllTokens(user, Tokens.USDC[42]);
  logger.info(`** bridge dai from foreign(${ForeignNetwork}) to home(${HomeNetwork})`);
  await bridgeAllTokens(user, Tokens.DAI[42]);
  logger.info(`** bridge wbtc from foreign(${ForeignNetwork}) to home(${HomeNetwork})`);
  await bridgeAllTokens(user, Tokens.WBTC[42]);
  logger.info(`** bridge weth from foreign(${ForeignNetwork}) to home(${HomeNetwork})`);
  await bridgeAllTokens(user, Tokens.WETH[42]);
}

async function main() {
  hre.changeNetwork(ForeignNetwork);

  const signers = await hre.ethers.getSigners();
  const admin = signers[0];
  await bridge(admin);
  const dev = signers[1];
  await bridge(dev);

  const lp1 = signers[2];
  const lp2 = signers[3];
  const lp3 = signers[4];

  await bridge(lp1);
  await bridge(lp2);
  await bridge(lp3);

  const user1 = signers[5];
  const user2 = signers[6];
  const user3 = signers[7];
  const user4 = signers[8];
  const user5 = signers[9];

  await bridge(user1);
  await bridge(user2);
  await bridge(user3);
  await bridge(user4);
  await bridge(user5);
}

if (require.main === module) {
  main()
    .then(() => process.exit(0))
    .catch((error) => {
      console.error(error);
      process.exit(1);
    });
}

module.exports = {
  bridgeAllTokens, bridge
}
