const hre = require("hardhat");
const logger = require("../logger");
const { toBN } = require("../utils");
const { HomeNetwork } = require("./_constants");

async function main() {
  hre.changeNetwork(HomeNetwork);
  const WMETA = await hre.ethers.getContractFactory("WMETAMock");
  const wmeta = await WMETA.deploy();
  await wmeta.deployed();
  logger.info("WMETA", wmeta.address);

  hre.changeNetwork("kovan");
  const network = await hre.ethers.provider.getNetwork();
  const signers = await hre.ethers.getSigners();
  const admin = signers[0];

  const Dai = await hre.ethers.getContractFactory("DaiMock");
  const dai = await Dai.deploy(network.chainId);
  await dai.deployed();
  logger.info("DAI", dai.address);

  const WBTC = await hre.ethers.getContractFactory("WBTCMock");
  const wbtc = await WBTC.deploy();
  await wbtc.deployed();
  logger.info("WBTC", wbtc.address);

  const WETH = await hre.ethers.getContractFactory("WETHMock");
  const weth = await WETH.deploy();
  await weth.deployed();
  logger.info("WETH", weth.address);

  const USDC = await hre.ethers.getContractFactory("FiatTokenV2_1");
  const usdc = await USDC.deploy();
  await usdc.deployed();
  await (await usdc.initialize(
    "USD Coin", "USDC", "USD", 6, 
    admin.address, admin.address, admin.address, admin.address
  )).wait();
  await (await usdc.initializeV2("USD Coin")).wait();
  await (await usdc.initializeV2_1(admin.address)).wait();
  logger.info("USDC", usdc.address);
}

if (require.main === module) {
  main()
    .then(() => process.exit(0))
    .catch((error) => {
      console.error(error);
      process.exit(1);
    });
}
