const hre = require("hardhat");
const logger = require("../logger");
const { BankAddress, MSpaceAddress, HomeNetwork } = require("./_constants");

async function enterBank(user, mspace, bank, amount) {
  await (await mspace.connect(user).approve(bank.address, amount)).wait();
  await (await bank.connect(user).enter(amount)).wait();
  logger.info(
    user.address, "enter bank", 
    hre.ethers.utils.formatUnits(amount.toString(), 18),
    "MSP"
  )
}

async function leaveBank(user, bank, amount) {
  await (await bank.connect(user).leave(amount)).wait();
  logger.info(
    user.address, "leave bank", 
    hre.ethers.utils.formatUnits(amount.toString(), 18),
    "xMSP"
  )
}

async function updateBank() {
  hre.changeNetwork(HomeNetwork);
  
  const signers = await hre.ethers.getSigners();

  const admin = signers[0]; 

  const Bank = await hre.ethers.getContractFactory("Bank");
  const bank = Bank.attach(BankAddress);

  const xmspBalance = await bank.balanceOf(admin.address);
  const ERC20 = await hre.ethers.getContractFactory("ERC20Mock");
  const msp = ERC20.attach(MSpaceAddress);
  const mspBalance = await msp.balanceOf(admin.address);

  const percent = Math.floor(Math.random() * 100)
  if (mspBalance.eq(0)) {
    if(xmspBalance.eq(0)) {
      logger.warn(admin.address, "not have xMSP & MSP for update bank");
    } else {
      await leaveBank(admin, bank, xmspBalance.mul(percent).div(100));
    }
  } else if(xmspBalance.eq(0)) {
    await enterBank(admin, msp, bank, mspBalance.mul(percent).div(100));
  } else {
    Math.random() > 0.5 ? 
      (await enterBank(admin, msp, bank, mspBalance.mul(percent).div(100)))
      :(await leaveBank(admin, bank, xmspBalance.mul(percent).div(100)));
  }
}

if (require.main === module) {
  updateBank()
    .then(() => process.exit(0))
    .catch((error) => {
      console.error(error);
      process.exit(1);
    });
}

module.exports = {
  updateBank
}