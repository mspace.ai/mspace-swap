const hre = require("hardhat");
const logger = require("../logger");
const { Tokens } = require("../constants");
const { HomeNetwork, FactoryAddress, MiningAddress  } = require("./_constants");
const { toBN } = require("../utils");

async function main() {
  hre.changeNetwork(HomeNetwork);
  // hre.changeNetwork("ether1");
  const signers = await hre.ethers.getSigners();
  const lp1 = signers[2];
  const lp2 = signers[3];
  const lp3 = signers[4];
  const lps = [lp1, lp2, lp3];

  const Factory = await hre.ethers.getContractFactory("UniswapV2Factory");
  const factory = Factory.attach(FactoryAddress);
  const tokens = [Tokens.WETH[12], Tokens.WBTC[12], Tokens.USDC[12], Tokens.DAI[12]];
  const pairs = [];
  for (let i=0; i<tokens.length; i++){
    const pairAddress = await factory.getPair(Tokens.WMETA[12], tokens[i]);
    pairs.push(pairAddress);
  }

  // const smallest = [...pairs].sort((pair1, pair2) => pair1.lps.gt(pair2.lps) ? 1:-1)[0];
  // const allocPoints = pairs.map((pair) => pair.lps.div(smallest.lps).toString());
  // console.log(pairs, allocPoints);

  const Mining = await hre.ethers.getContractFactory("MiningHill");
  const mining = Mining.attach(MiningAddress);
  for (let i=0; i<pairs.length; i++) {
    // const allocPoint = allocPoints[i];
    const allocPoint = toBN(Math.floor(Math.random()*100));
    await (await mining.add(
      allocPoint,
      pairs[i], false)
    ).wait();
    logger.info("add pool", pairs[i], "with point", allocPoint.toString());
  }
}

if (require.main === module) {
  main()
    .then(() => process.exit(0))
    .catch((error) => {
      console.error(error);
      process.exit(1);
    });
}