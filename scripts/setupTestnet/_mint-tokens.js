const hre = require("hardhat");
const logger = require("../logger");
const { Tokens } = require("../constants");
const { ForeignNetwork, HomeNetwork } = require("./_constants");
const { toBN } = require("../utils");

const user = "0x3B3b8F1DE873a2A32C24Efce1FC75DDF896069C0";

async function mintERC20Mock(address, user, amount) {
  const ERC20Mock = await hre.ethers.getContractFactory("ERC20Mock");
  const erc20 = ERC20Mock.attach(address);
  const decimals = await erc20.decimals();
  await (await erc20.mint(user, amount.mul(toBN(10).pow(decimals)))).wait();
  logger.info("mint", await erc20.symbol(), user, amount.toString());
}

async function mintUSDC(admin, user, amount) {
  hre.changeNetwork("kovan");
  const USDC = await hre.ethers.getContractFactory("FiatTokenV2_1");
  const usdc = USDC.attach(Tokens.USDC[42]);
  const decimals = await usdc.decimals();
  await (await usdc.configureMinter(admin, amount.mul(toBN(10).pow(decimals)))).wait();
  await (await usdc.mint(user, amount.mul(toBN(10).pow(decimals)))).wait();
  logger.info("mint USDC", user, amount.toString());
}

async function main() {
  hre.changeNetwork(ForeignNetwork);

  const signers = await hre.ethers.getSigners();
  const admin = signers[0];

  let amount = toBN("10000");

  await mintUSDC(admin.address, user, amount);
  await mintERC20Mock(Tokens.DAI[42], user, amount);
  await mintERC20Mock(Tokens.WBTC[42], user, amount);
  await mintERC20Mock(Tokens.WETH[42], user, amount);

  // hre.changeNetwork(HomeNetwork);
  // await mintERC20Mock(Tokens.WMETA[12], user, amount);
}

if (require.main === module) {
  main()
    .then(() => process.exit(0))
    .catch((error) => {
      console.error(error);
      process.exit(1);
    });
}
