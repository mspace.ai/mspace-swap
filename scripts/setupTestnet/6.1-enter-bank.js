const hre = require("hardhat");
const logger = require("../logger");
const { HomeNetwork, MSpaceAddress, MiningAddress, BankAddress } = require("./_constants");

async function main() {
  hre.changeNetwork(HomeNetwork);
  
  const signers = await hre.ethers.getSigners();
  const lp1 = signers[2];
  const lp2 = signers[3];
  const lp3 = signers[4];
  const lps = [lp1, lp2, lp3];

  // const Mining = await hre.ethers.getContractFactory("MiningHill");
  // const mining = Mining.attach(MiningAddress);
  const MSpace = await hre.ethers.getContractFactory("MSpaceToken");
  const mspace = MSpace.attach(MSpaceAddress);
  const Bank = await hre.ethers.getContractFactory("Bank");
  const bank = Bank.attach(BankAddress);

  for (let i=0; i<lps.length; i++) {
    const user = lps[i];
    const balance = await mspace.balanceOf(user.address);
    
    if (balance.eq(0)) {
      logger.warn(user.address, "not have MSP to enter Bank");
    } else { 
      await (await mspace.connect(user).approve(bank.address, balance)).wait();
      await (await bank.connect(user).enter(balance)).wait();
      logger.info(
        user.address, "enter", 
        hre.ethers.utils.formatUnits(balance.toString(), 18), "MSP to Bank"
      );
    }
  }
}

if (require.main === module) {
  main()
    .then(() => process.exit(0))
    .catch((error) => {
      console.error(error);
      process.exit(1);
    });
}