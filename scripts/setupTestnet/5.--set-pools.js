const hre = require("hardhat");
const logger = require("../logger");
const { HomeNetwork, MiningAddress } = require("./_constants");

async function main() {
  hre.changeNetwork(HomeNetwork);

  const Mining = await hre.ethers.getContractFactory("MiningHill");
  const mining = Mining.attach(MiningAddress);

  const length = await mining.poolLength();
  for (let pool=0; pool<length; pool++){
    const allocPoint = Math.floor(Math.random()*100);
    await (await mining.set(pool, allocPoint, false)).wait();
    logger.info("set", pool, allocPoint);
  }
}

if (require.main === module) {
  main()
    .then(() => process.exit(0))
    .catch((error) => {
      console.error(error);
      process.exit(1);
    });
}