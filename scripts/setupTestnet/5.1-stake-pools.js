const hre = require("hardhat");
const logger = require("../logger");
const { Tokens } = require("../constants");
const { HomeNetwork, FactoryAddress, MiningAddress } = require("./_constants");
const { toBN } = require("../utils");

async function pairInfo(users, tk0, tk1) {
  const Factory = await hre.ethers.getContractFactory("UniswapV2Factory");
  const Pair = await hre.ethers.getContractFactory("UniswapV2Pair");

  const factory = Factory.attach(FactoryAddress);
  const pairAddress = await factory.getPair(tk0, tk1);

  const pair = Pair.attach(pairAddress);
  const lps = (await Promise.all(users.map(user => pair.balanceOf(user.address)))).reduce((a, b) => a.add(b));
  // logger.info(pairAddress, lps.toString());
  return {lps: lps, address: pairAddress}
}

async function depositLPs(user, pairAddress, poolIndex) {
  const Mining = await hre.ethers.getContractFactory("MiningHill");
  const mining = Mining.attach(MiningAddress);

  const Pair = await hre.ethers.getContractFactory("UniswapV2Pair");
  const pair = Pair.attach(pairAddress);

  const amount = await pair.balanceOf(user.address);
  if (amount.eq(0)) {
    logger.warn("user", user.address, "not have lps in pool", pair.address,"to stake");
    return
  }
  await (await pair.connect(user).approve(mining.address, amount)).wait();
  await (await mining.connect(user).deposit(poolIndex, amount)).wait();
  logger.info(user.address, "stake to pool", pair.address, amount.toString());
}

async function main() {
  hre.changeNetwork(HomeNetwork);
  // hre.changeNetwork("ether1");
  const signers = await hre.ethers.getSigners();
  const lps = [
    signers[2], // lp1
    signers[3], // lp2
    signers[4], // lp3
  ];
  
  const pairs = [
    await pairInfo(lps, Tokens.WMETA[12], Tokens.WETH[12]),
    await pairInfo(lps, Tokens.WMETA[12], Tokens.WBTC[12]),
    await pairInfo(lps, Tokens.WMETA[12], Tokens.USDC[12]),
    await pairInfo(lps, Tokens.WMETA[12], Tokens.DAI[12]),
  ]

  for (let i=0; i<pairs.length; i++) {
    for (let k=0; k<lps.length; k++) {
      await depositLPs(lps[k], pairs[i].address, i);
    }
  }
}

if (require.main === module) {
  main()
    .then(() => process.exit(0))
    .catch((error) => {
      console.error(error);
      process.exit(1);
    });
}