const hre = require("hardhat");
const logger = require("../logger");
const { toBN } = require("../utils");
const { Tokens } = require("../constants");
const { RouterAddress, HomeNetwork } = require("./_constants");

async function swap(user, tk1Address, amount, tk2Address) {
  const ERC20 = await hre.ethers.getContractFactory("ERC20Mock");
  const tk1 = ERC20.attach(tk1Address);
  if (tk1Address !== Tokens.META[12]) {
    await (await tk1.connect(user).approve(RouterAddress, amount)).wait();
  }

  const tk2 = ERC20.attach(tk2Address);
  const oldBalance = await tk2.balanceOf(user.address);

  const Router = await hre.ethers.getContractFactory("UniswapV2Router02");
  const router = Router.attach(RouterAddress).connect(user);
  // TODO check slippage before swap
  try {
    if (tk1Address !== Tokens.META[12]) {
      await (await router.connect(user).swapExactTokensForMETA(
        amount, 0, [tk1Address, tk2Address],
        user.address,
        Math.floor(new Date().getTime()/1000) + 3600
      )).wait();
    } else {
      await (await router.connect(user).swapExactMETAForTokens(
        0, [tk1Address, tk2Address],
        user.address,
        Math.floor(new Date().getTime()/1000) + 3600,
        {value: amount}
      )).wait();
    }
  } catch(_e) {
    logger.warn(
      user.address, "got error when swap",
      hre.ethers.utils.formatUnits(amount.toString(), await tk1.decimals()), await tk1.symbol(), "->",
      await tk2.symbol(), "at block",
      await hre.ethers.provider.getBlockNumber()
    );
  }

  const amountOut = (await tk2.balanceOf(user.address)).sub(oldBalance);
  logger.info(
    user.address, "swap", 
    hre.ethers.utils.formatUnits(amount.toString(), await tk1.decimals()), await tk1.symbol(), "->", 
    hre.ethers.utils.formatUnits(amountOut.toString(), await tk2.decimals()), await tk2.symbol()
  );
}

async function randomSwapWithWMETAPool(user, tokens) {
  const token = tokens[Math.floor(Math.random()*tokens.length)];
  const isSwapMeta = Math.round(Math.random()) == 1;
  const [tk1Address, tk2Address] = isSwapMeta ? [Tokens.META[12], token] : [token, Tokens.META[12]];
  const ERC20 = await hre.ethers.getContractFactory("ERC20Mock");
  const erc20 = ERC20.attach(tk1Address);
  const balance = tk1Address === Tokens.META[12] ? 
    (await hre.ethers.provider.getBalance(user.address)) 
    : await erc20.balanceOf(user.address);
  if (balance.eq(0)) {
    logger.warn(user.address, "can not swap", await erc20.symbol(), "because balance is 0");
    return
  }

  const randomLenth = balance.toString().length > 8 ? 8:balance.toString().length;
  const randomMax = toBN(balance.toString().substring(0,randomLenth)).toNumber();
  const amount = toBN(
    Math.floor(randomMax*0.25 + Math.random() * randomMax*0.5)
  ).mul(toBN(10).pow(balance.toString().length - randomLenth));
  // logger.info(balance.toString(), amount.toString());

  await swap(user, tk1Address, amount, tk2Address);
}

async function runSwap() {
  hre.changeNetwork(HomeNetwork);

  const signers = await hre.ethers.getSigners();
  const user1 = signers[5];
  const user2 = signers[6];
  const user3 = signers[7];
  const user4 = signers[8];
  const user5 = signers[9];

  const tokens = [Tokens.ETH[12], Tokens.BTC[12], Tokens.DAI[12], Tokens.USDC[12]];

  await randomSwapWithWMETAPool(user1, tokens);
  await randomSwapWithWMETAPool(user2, tokens);
  await randomSwapWithWMETAPool(user3, tokens);
  await randomSwapWithWMETAPool(user4, tokens);
  await randomSwapWithWMETAPool(user5, tokens);
}

if (require.main === module) {
  runSwap()
    .then(() => process.exit(0))
    .catch((error) => {
      console.error(error);
      process.exit(1);
    });
}

module.exports = {
  runSwap
}