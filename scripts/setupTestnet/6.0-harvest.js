const hre = require("hardhat");
const logger = require("../logger");
const { HomeNetwork, MSpaceAddress, MiningAddress, BankAddress } = require("./_constants");

async function main() {
  hre.changeNetwork(HomeNetwork);
  
  const signers = await hre.ethers.getSigners();
  const lp1 = signers[2];
  const lp2 = signers[3];
  const lp3 = signers[4];
  const lps = [lp1, lp2, lp3];

  const Mining = await hre.ethers.getContractFactory("MiningHill");
  const mining = Mining.attach(MiningAddress);
  const MSpace = await hre.ethers.getContractFactory("MSpaceToken");
  const mspace = MSpace.attach(MSpaceAddress);
  // const Bank = await hre.ethers.getContractFactory("Bank");
  // const bank = Bank.attach(BankAddress);

  for (let i=0; i<lps.length; i++) {
    for (let pool=0; pool<4; pool++) {
      const user = lps[i];

      logger.info(
        user.address, pool,
        hre.ethers.utils.formatUnits((await mining.pendingMSpace(pool, user.address)).toString(), 18)
      );

      const oldBalance = await mspace.balanceOf(user.address);
      await (await mining.connect(user).harvest(pool)).wait();
      const newBalance = await mspace.balanceOf(user.address);
      logger.info(
        user.address, "harvested", 
        hre.ethers.utils.formatUnits(newBalance.sub(oldBalance).toString(), 18), "MSP"
      );
    }
  }
}

if (require.main === module) {
  main()
    .then(() => process.exit(0))
    .catch((error) => {
      console.error(error);
      process.exit(1);
    });
}