const hre = require("hardhat");
const logger = require("../logger");
const { Tokens } = require("../constants");
const { HomeNetwork, FactoryAddress, ExtractorAddress, BankAddress, MSpaceAddress } = require("./_constants");

async function convertSwapFee() {
  hre.changeNetwork(HomeNetwork);

  const Extractor = await hre.ethers.getContractFactory("Extractor");
  const extractor = Extractor.attach(ExtractorAddress);
  const ERC20 = await hre.ethers.getContractFactory("ERC20Mock");
  const Factory = await hre.ethers.getContractFactory("UniswapV2Factory");
  const factory = Factory.attach(FactoryAddress);

  const tokens = [
    Tokens.WETH[12], 
    Tokens.WBTC[12], 
    Tokens.USDC[12], 
    Tokens.DAI[12]
  ];
  for (let i=0; i<tokens.length; i++) {
    const erc20 = ERC20.attach(tokens[i]);
    const tkSymbol = await erc20.symbol();

    const pairAddress = await factory.getPair(tokens[i], Tokens.WMETA[12]);
    const pair = ERC20.attach(pairAddress);
    const lps = await pair.balanceOf(ExtractorAddress);
    logger.info(`${tkSymbol}-META`, "MP", lps.toString());
    if (lps.gt(0)) {
      try {
        await (await extractor.convert(tokens[i], Tokens.WMETA[12])).wait();
        logger.info("converted swap fee for pool", `${tkSymbol}-META`);
      } catch (e) {
        // TODO check lps too low
        logger.error("can not converted swap fee for pool", `${tkSymbol}-META`, "maybe lps too low");
      }
    } else {
      logger.warn("Extractor not have swap fee in pool", `${tkSymbol}-META`);
    }
  }

  // check rate MSP-xMSP
  const Bank = await hre.ethers.getContractFactory("Bank");
  const bank = Bank.attach(BankAddress);
  const mspace = ERC20.attach(MSpaceAddress);
  const xMSP = await bank.totalSupply();
  const MSP = await mspace.balanceOf(BankAddress);
  logger.info(
    "rate MSP->xMSP",
    Number(hre.ethers.utils.formatUnits(MSP, 18))/Number(hre.ethers.utils.formatUnits(xMSP, 18)),
  );
}

if (require.main === module) {
  convertSwapFee()
    .then(() => process.exit(0))
    .catch((error) => {
      console.error(error);
      process.exit(1);
    });
}

module.exports = {
  convertSwapFee
}