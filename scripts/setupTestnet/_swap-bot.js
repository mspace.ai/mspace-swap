const CronJob = require('cron').CronJob;
const { runSwap } = require("./4.--swap");

const cronTime = "0 0 * * * *";
const job = new CronJob(cronTime,  async() => {
  try {
    await runSwap();
  } catch (error) {
    console.error(error);
  }
}, null, true);
job.start();