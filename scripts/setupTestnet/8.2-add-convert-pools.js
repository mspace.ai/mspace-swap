const hre = require("hardhat");
const logger = require("../logger");
const { toBN } = require("../utils");
const { Tokens } = require("../constants");
const { HomeNetwork, RouterAddress, FactoryAddress, MSpaceAddress } = require("./_constants");

async function addAllToMSpacePool(user, tkAddress, rate, rateDecimal, keep) {
  const ERC20 = await hre.ethers.getContractFactory("ERC20Mock");
 
  // if tkAddress is WMETA
  let amount;
  if (tkAddress !== Tokens.WMETA[12]) {
    const tk = ERC20.attach(tkAddress).connect(user);
    amount = await tk.balanceOf(user.address);
    amount = amount.sub(toBN(keep).mul(toBN(10).pow(await tk.decimals())));
  } else {
    amount = await hre.ethers.provider.getBalance(user.address);
    amount = amount.sub(toBN(keep).mul(toBN(10).pow(18)));
  }
  if (amount.lte(0)) {
    logger.info("can't not add liquidity because amount is 0");
    return;
  }
  const tkSymbol = await tk.symbol();
  const tkDecimals = await tk.decimals();

  const MSpace = await hre.ethers.getContractFactory("MSpaceToken");
  const mspace = MSpace.attach(MSpaceAddress).connect(user);
  const mspaceBalance = await mspace.balanceOf(user.address);

  // TODO fix this if amount need too low this will be equal 0
  const need = amount.mul(toBN(10).pow(toBN(18).sub(tkDecimals))).mul(rate).div(rateDecimal);
  if (mspaceBalance.lt(need)) {
    logger.warn(user.address, "not have enough MSP for add to pool", mspaceBalance.toString(), need.toString());
    return
  }

  await (await tk.approve(RouterAddress, amount)).wait();
  await (await mspace.approve(RouterAddress, need)).wait();

  const Router = await hre.ethers.getContractFactory("UniswapV2Router02");
  const router = Router.attach(RouterAddress).connect(user);
  logger.info("add to pool", `${tkSymbol}-MSP`, amount.toString(), need.toString());
  if (tkAddress !== Tokens.WMETA[12]) {
    await (await router.connect(user).addLiquidity(
      tkAddress, MSpaceAddress,
      amount, need, 0, 0,
      user.address,
      Math.floor(new Date().getTime()/1000) + 3600
    )).wait();
  } else {
    await (await router.connect(user).addLiquidityMETA(
      MSpaceAddress,
      need, 0, 0,
      user.address,
      Math.floor(new Date().getTime()/1000) + 3600,
      {value: amount}
    )).wait();
  }
  const Factory = await hre.ethers.getContractFactory("UniswapV2Factory");
  const factory = Factory.attach(FactoryAddress);
  const pairAddress = await factory.getPair(tkAddress, MSpaceAddress);
  const Pair = await hre.ethers.getContractFactory("UniswapV2Pair");
  const pair = Pair.attach(pairAddress);
  logger.info("got MP", (await pair.balanceOf(user.address)).toString());
}

async function main() {
  hre.changeNetwork(HomeNetwork);
  const signers = await hre.ethers.getSigners();
  const dev = signers[1];

  // add to conver pools
  // MSP price 0.1$
  const WETH_MSP_RATE = 30338; const WETH_MSP_RATE_DEC = 1;
  const WBTC_MSP_RATE = 451652; const WBTC_MSP_RATE_DEC = 1;
  const USDC_MSP_RATE = 10; const USDC_MSP_RATE_DEC = 1;
  const DAI_MSP_RATE = 10; const DAI_MSP_RATE_DEC = 1; 
  const WMETA_MSP_RATE = 12; const WMETA_MSP_RATE_DEC = 10; 

  await addAllToMSpacePool(dev, Tokens.WETH[12], WETH_MSP_RATE, WETH_MSP_RATE_DEC, 0);
  await addAllToMSpacePool(dev, Tokens.WBTC[12], WBTC_MSP_RATE, WBTC_MSP_RATE_DEC, 0);
  await addAllToMSpacePool(dev, Tokens.USDC[12], USDC_MSP_RATE, USDC_MSP_RATE_DEC, 0);
  await addAllToMSpacePool(dev, Tokens.DAI[12], DAI_MSP_RATE, DAI_MSP_RATE_DEC, 0);
  await addAllToMSpacePool(dev, Tokens.WMETA[12], WMETA_MSP_RATE, WMETA_MSP_RATE_DEC, 50);
}

if (require.main === module) {
  main()
    .then(() => process.exit(0))
    .catch((error) => {
      console.error(error);
      process.exit(1);
    });
}