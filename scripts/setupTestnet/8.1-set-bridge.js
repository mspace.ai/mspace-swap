const hre = require("hardhat");
const logger = require("../logger");
const { Tokens } = require("../constants");
const { HomeNetwork, ExtractorAddress, MSpaceAddress } = require("./_constants");

async function main() {
  hre.changeNetwork(HomeNetwork);
  const Extractor = await hre.ethers.getContractFactory("Extractor");
  const extractor = Extractor.attach(ExtractorAddress);
  const tokens = [
    Tokens.WETH[12], 
    Tokens.WBTC[12], 
    Tokens.USDC[12], 
    Tokens.DAI[12],
    // Tokens.WMETA[12],
  ];
  const ERC20 = await hre.ethers.getContractFactory("ERC20Mock");
  for (let i=0;i<tokens.length; i++) {
    const erc20 = ERC20.attach(tokens[i]);
    await (await extractor.setBridge(tokens[i], MSpaceAddress)).wait();
    logger.info(`set bridge ${await erc20.symbol()}-MSP`);
  }
}

if (require.main === module) {
  main()
    .then(() => process.exit(0))
    .catch((error) => {
      console.error(error);
      process.exit(1);
    });
}