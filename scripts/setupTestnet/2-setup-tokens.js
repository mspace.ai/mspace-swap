const hre = require("hardhat");
const logger = require("../logger");
const { toBN } = require("../utils");
const { Tokens } = require("../constants");
const { ForeignNetwork, HomeNetwork } = require("./_constants");

async function mintTokens(user, numberator, denominators) {
  const liquidityUSDC = toBN(446943).mul(toBN(10).pow(6)).mul(numberator).div(denominators);
  const liquidityDAI = toBN(446943).mul(toBN(10).pow(18)).mul(numberator).div(denominators);
  const liquidityWBTC = toBN(9).mul(toBN(10).pow(8)).mul(numberator).div(denominators);
  const liquidityWETH = toBN(132).mul(toBN(10).pow(18)).mul(numberator).div(denominators);
  const WMETA_PRICE = 0.12;
  const WBTC_PRICE = 45165.20;
  const WETH_PRICE = 3033.80;
  const liquidityWMETA = toBN(
    Math.round(
      446943/WMETA_PRICE + 446943/WMETA_PRICE + 9*WBTC_PRICE/WMETA_PRICE + 132*WETH_PRICE/WMETA_PRICE
    )
  ).mul(toBN(10).pow(18)).mul(numberator).div(denominators);

  hre.changeNetwork(ForeignNetwork);
  const signers = await hre.ethers.getSigners();
  const admin = signers[0];

  const USDC = await hre.ethers.getContractFactory("FiatTokenV2_1");
  const usdc = USDC.attach(Tokens.USDC[42]);
  await (await usdc.configureMinter(admin.address, liquidityUSDC)).wait();
  await (await usdc.mint(user, liquidityUSDC)).wait();
  logger.info("USDC balance of", user, (await usdc.balanceOf(user)).toString());

  const WBTC = await hre.ethers.getContractFactory("WBTCMock");
  const wbtc = WBTC.attach(Tokens.WBTC[42]);
  await (await wbtc.mint(user, liquidityWBTC)).wait();
  logger.info("WBTC balance of", user, (await wbtc.balanceOf(user)).toString());
  
  const WETH = await hre.ethers.getContractFactory("WETHMock");
  const weth = WETH.attach(Tokens.WETH[42]);
  await (await weth.mint(user, liquidityWETH)).wait();
  logger.info("WETH balance of", user, (await weth.balanceOf(user)).toString());

  const Dai = await hre.ethers.getContractFactory("DaiMock");
  const dai = Dai.attach(Tokens.DAI[42]);
  await (await dai.mint(user, liquidityDAI)).wait();
  logger.info("DAI balance of", user, (await dai.balanceOf(user)).toString());

  hre.changeNetwork(HomeNetwork);
  const WMETA = await hre.ethers.getContractFactory("WMETAMock");
  const wmeta = WMETA.attach(Tokens.WMETA[12]);
  await (await wmeta.mint(user, liquidityWMETA)).wait();
  logger.info("WMETA balance of", user, (await wmeta.balanceOf(user)).toString());
}

async function main() {
  hre.changeNetwork(ForeignNetwork);
  
  const signers = await hre.ethers.getSigners();
  const admin = signers[0];
  await mintTokens(admin.address, 1, 100);
  const dev = signers[1];
  await mintTokens(dev.address, 1, 100);

  const lp1 = signers[2];
  const lp2 = signers[3];
  const lp3 = signers[4];
  await mintTokens(lp1.address, 100, 100);
  await mintTokens(lp2.address, 60, 100);
  await mintTokens(lp3.address, 10, 100); 

  const user1 = signers[5];
  const user2 = signers[6];
  const user3 = signers[7];
  const user4 = signers[8];
  const user5 = signers[9];
  await mintTokens(user1.address, 10, 100000);
  await mintTokens(user2.address, 5, 100000);
  await mintTokens(user3.address, 4, 100000); 
  await mintTokens(user4.address, 7, 100000); 
  await mintTokens(user5.address, 2, 100000); 
}

if (require.main === module) {
  main()
    .then(() => process.exit(0))
    .catch((error) => {
      console.error(error);
      process.exit(1);
    });
}

module.exports = {
  mintTokens
}