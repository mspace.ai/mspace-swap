const HomeMediatorAddress = "0x07852350BfA23c4DB993e901554E90D269FD311e";
const ForeignMediatorAddress = "0xd5f353acc0a0d60422bee5B7435c6ad94A7E5dAf";
const HomeNetwork = "metatestnet";
const ForeignNetwork = "kovan";

const FactoryAddress = "0x044a1a5B0Dd5a337CC815C314170657225D15d52";
// const RouterAddress = "0x82c6dC84fbc097AB85D4f8BDc2f34042d69E1F2E";
const RouterAddress = "0x3FDFA448F66cC28ff29689823d7f8180141a27FE";
const PairImplAddress = "0x3CfBdc4c5EC6040a2a82D2bA4236b50CB9d138a2";

const MSpaceAddress = "0x8a7C8755ff2d52F41346841176bEb54D4A3ad934";
const MiningAddress = "0x60550807c387A7050D0c3B2c06E8d7b14211D14d";
const BankAddress = "0xF74475AdD8CB29794D8105dc7E4ece99bFC38575";
const ExtractorAddress = "0xc0DA7eB11913767d1D5Cbe7a5DC64Ff31b00433a";

const Multicall2Address = "0x21ff38B0D0CB707c2ac7D437145074c9F6aaE103";
const ENSAddress = "0xfbbe0ceE1fcB936156e87143b120F7720603db23";
const BoringHelperAddress = "0xF26750552e929cd29561317719Bf45d9Cd296820";
const ConverterAddress = "0x488562F55af7DAC19E31029435626644C5c17627";

const WETH_WMETA_RATE = 25281; const WETH_WMETA_RATE_DEC = 1;
const WBTC_WMETA_RATE = 376376; const WBTC_WMETA_RATE_DEC = 1;
const DAI_WMETA_RATE = 8333; const DAI_WMETA_RATE_DEC = 1000; 
const USDC_WMETA_RATE = 8333; const USDC_WMETA_RATE_DEC = 1000; 

module.exports = {
  RouterAddress, FactoryAddress, PairImplAddress,
  HomeMediatorAddress, ForeignMediatorAddress, HomeNetwork, ForeignNetwork,
  MSpaceAddress, MiningAddress, BankAddress, ExtractorAddress,
  Multicall2Address, ENSAddress, BoringHelperAddress, ConverterAddress,
  WETH_WMETA_RATE, WETH_WMETA_RATE_DEC,
  WBTC_WMETA_RATE, WBTC_WMETA_RATE_DEC,
  DAI_WMETA_RATE, DAI_WMETA_RATE_DEC,
  USDC_WMETA_RATE, USDC_WMETA_RATE_DEC,
}