const pino = require('pino');

const logger = pino({
  prettyPrint: true,
  enabled: true,
  name: "MSpaceSwap",
  level: 'info',
  base: {}
});

// web3Home.currentProvider.setLogger(logger)

module.exports = logger;
