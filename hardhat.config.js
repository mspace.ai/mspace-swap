const { task, types } = require('hardhat/config');

const path = require('path');
const fs = require("fs");
const env_path = path.join(__dirname, '.', '.env');
require('dotenv').config({
  path: fs.existsSync(env_path) ? env_path : path.join(__dirname, '.', '.local.env')
})

require("@nomiclabs/hardhat-waffle");
require("@nomiclabs/hardhat-ganache");
require("@nomiclabs/hardhat-web3");

require("hardhat-deploy");
require("hardhat-deploy-ethers");
require("hardhat-change-network");

// TODO if env is LOCAL use folder `contracts`
const sourceDir = "flattened";
// const sourceDir = "contracts";

const privateKeys = [
  /* accounts for deployment */
  process.env.ADMIN_KEY, 
  process.env.DEV_KEY, 

  /* accounts below for test */
  ...(process.env.ACCOUNTS === undefined ? []:process.env.ACCOUNTS.split(",")),
].filter(a => !!a);

// "test test test test test test test test test test test meta"
const mnemonic = process.env.MNEMONIC;
// default path (i.e.  `m/44'/60'/0'/0/0`
const accounts = mnemonic !== undefined ? { 
  mnemonic: mnemonic 
} : privateKeys

// admin tasks
const { lockPairs } = require("./scripts/admin/swap-lock");
task("swap-lock", "Lock a pair, or all pairs if params --pairs not set")
  .addOptionalParam("pairs", "symbol0,symbol1 | token0,token1 | pairAddress", undefined, types.string)
  .setAction(lockPairs);
const { unlockPairs } = require("./scripts/admin/swap-unlock");
task("swap-unlock", "Unlock a pair, or all pairs if params --pairs not set")
  .addOptionalParam("pairs", "symbol0,symbol1 | token0,token1 | pairAddress", undefined, types.string)
  .setAction(unlockPairs);
const { upgradePairs } = require("./scripts/admin/swap-upgrade");
task("swap-upgrade", "Upgrade a pair, or all pairs if params --pairs not set")
  .addOptionalParam("pairs", "symbol0,symbol1 | token0,token1 | pairAddress", undefined, types.string)
  .setAction(upgradePairs);
const { verifyPairs } = require("./scripts/admin/swap-verify-pair-proxy");
task("swap-contract-verify", "Check all pair contracts and verify them in explorer")
    .setAction(verifyPairs);

const { flatten, deploy } = require("./scripts/hardhat.tasks");
task("flatten", "To support verify on meta-scan, Contract must be flattened. This task will flatten and remove multiple licenses in a list of main contracts", async() => flatten(sourceDir));
task("deploy", "Inject task flatten:custom to hardhhat-deploy task", deploy);
task("accounts", "Prints the list of accounts", async (taskArgs, hre) => {
  const accounts = await hre.ethers.getSigners();

  for (const account of accounts) {
    console.log(account.address);
  }
});

module.exports = {
  mocha: {
    timeout: false
  },
  solidity: {
    compilers: [{
      version: "0.8.2",
      settings: {
        optimizer: {
          enabled: true,
          runs: 200,
        },
      },
    }, {
      version: "0.6.12",
      settings: {
        optimizer: {
          enabled: true,
          runs: 200,
        },
      }
    }],
  },
  defaultNetwork: "hardhat",
  namedAccounts: {
    admin: 0,
    dev: 1,
    ... !process.env.LEDGER_ADDRESS ? {}: {ledger: `ledger://${process.env.LEDGER_ADDRESS}`}
  },
  networks: {
    hardhat: {
      saveDeployments: true,
      accounts: mnemonic !== undefined ? 
        { mnemonic: mnemonic } 
        : privateKeys.map((privateKey) => { 
          return {privateKey: privateKey, balance: "1000000000000000000000"} 
        }),

      // forking: {
      //   url: "https://api.metadium.com/dev",
      //   // url: "http://13.124.111.52:8588",
      //   blockNumber: 20847779,
      // },
      // chainId: 12,
      
      gasPrice: 120000000000,
      blockGasLimit: 268435456,
      allowUnlimitedContractSize: true,
    },
    ether1: {
      url: "http://localhost:8545",
      accounts: accounts,
      chainId: 12,
      gasPrice: 120000000000,
      blockGasLimit: 268435456,
      allowUnlimitedContractSize: true,
      timeout: 120000
    },
    ether2: {
      url: "http://localhost:8547",
      accounts: accounts,
      chainId: 42,
      gasPrice: 120000000000,
      blockGasLimit: 268435456,
      allowUnlimitedContractSize: true,
      timeout: 120000
    },
    kovan: {
      url: "https://kovan.infura.io/v3/9aa3d95b3bc440fa88ea12eaa4456161",
      accounts: accounts,
      chainId: 42,
      gasPrice: 15000000000,
      blockGasLimit: 12500000,
      allowUnlimitedContractSize: true,
      timeout: 120000
    },
    metatestnet: {
      url: "https://api.metadium.com/dev",
      // url: "http://13.124.111.52:8588",
      accounts: accounts,
      chainId: 12,
      gasPrice: 120000000000,
      blockGasLimit: 268435456,
      allowUnlimitedContractSize: true,
      timeout: 120000
    },
    metadium: {
      url: "https://api.metadium.com/prod",
      accounts: accounts,
      chainId: 11,
      // gasPrice: 320000000000,
      gasPrice: 200000000000,
      blockGasLimit: 268435456,
      allowUnlimitedContractSize: true,
      timeout: 120000
    }
  },
  paths: {
    sources: sourceDir,
    tests: "test",
    cache: "cache",
    artifacts: "artifacts",
    deploy: 'deploy',
    deployments: 'deployments',
    imports: 'imports',
  },
};

