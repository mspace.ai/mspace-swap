// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";

contract ERC20Mock is ERC20 {
  uint8 private _decimals;
  constructor(
    string memory name,
    string memory symbol,
    uint8 __decimals
  ) ERC20(name, symbol) {
    _decimals = __decimals;
  }
  function decimals() public view virtual override returns (uint8) {
    return _decimals;
  }
  function mint(address _to, uint256 _amount) public {
    _mint(_to, _amount);
  }
  function burn(address _to) public {
    uint amount = balanceOf(address(this));
    _burn(_to, amount);
  }
}