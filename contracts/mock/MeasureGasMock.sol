// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

contract MeasureGasMock {
  mapping(address => mapping(address => address)) public getPair;
  uint256 public dummy;
  constructor() {
    dummy = 1;
  }
  function changeDummy() public {
    dummy += 1;
  }
  function orderToken(address tokenA, address tokenB, address pair) public {
    (address token0, address token1) = tokenA < tokenB ? (tokenA, tokenB) : (tokenB, tokenA);
    changeDummy();
  }
  function storePair(address tokenA, address tokenB, address pair) public {
    getPair[tokenA][tokenB] = pair;
    changeDummy();
  }
  function orderStorePair(address tokenA, address tokenB, address pair) public {
    (address token0, address token1) = tokenA < tokenB ? (tokenA, tokenB) : (tokenB, tokenA);
    getPair[token0][token1] = pair;
    changeDummy();
  }
  function orderAccessPair(address tokenA, address tokenB, address pair) public {
    (address token0, address token1) = tokenA < tokenB ? (tokenA, tokenB) : (tokenB, tokenA);
    address p = getPair[token0][token1];
    changeDummy();
  }
  function accessPair(address tokenA, address tokenB, address pair) public {
    address p = getPair[tokenA][tokenB];
    changeDummy();
  }
}