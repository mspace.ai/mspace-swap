// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

abstract contract ERC20 {
  function allowance(address _owner, address _spender) public virtual view returns (uint256);
  function transferFrom(address _from, address _to, uint256 _value) public virtual returns (bool);
  function approve(address _spender, uint256 _value) public virtual returns (bool);
  event Approval(address indexed owner, address indexed spender, uint256 value);

  function totalSupply() public virtual view returns (uint256);
  function balanceOf(address _who) public virtual view returns (uint256);
  function transfer(address _to, uint256 _value) public virtual returns (bool);
  event Transfer(address indexed from, address indexed to, uint256 value);
}