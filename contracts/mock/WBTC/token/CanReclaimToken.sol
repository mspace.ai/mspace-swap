// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import "../access/OwnableClaimable.sol";
import "../utils/SafeERC20.sol";
import "./ERC20.sol";

contract CanReclaimToken is OwnableClaimable {
  using SafeERC20 for ERC20;

  /**
   * @dev Reclaim all ERC20Basic compatible tokens
   * @param _token ERC20Basic The address of the token contract
   */
  function reclaimToken(ERC20 _token) external onlyOwner {
    uint256 balance = _token.balanceOf(address(this));
    _token.safeTransfer(owner, balance);
  }

}