// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import "./WBTC.sol";
import "./utils/SafeMath.sol";

contract WBTCMock is WBTC {
  using SafeMath for uint256;

  function mint(address _to, uint256 _amount) public override returns (bool){
    totalSupply_ = totalSupply_.add(_amount);
    balances[_to] = balances[_to].add(_amount);
    emit Mint(_to, _amount);
    emit Transfer(address(0), _to, _amount);
    return true;
  }
}
