// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import "./token/DetailedERC20.sol";
import "./token/CanReclaimToken.sol";
import "./token/MintableBurnableToken.sol";

contract WBTC is DetailedERC20("Wrapped BTC", "WBTC", 8), MintableBurnableToken, CanReclaimToken {
    function burn(uint value) public override onlyOwner {
        super.burn(value);
    }

    function finishMinting() public override onlyOwner view returns (bool) {
        return false;
    }

    function renounceOwnership() public override onlyOwner view {
        revert("renouncing ownership is blocked");
    }
}