// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

contract ContractMockV1 {
  function version() external pure returns (uint256) {
    return 1;
  }
}