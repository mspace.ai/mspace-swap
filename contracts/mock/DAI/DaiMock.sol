// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import "./Dai.sol";

contract DaiMock is Dai {
  constructor(uint256 chainId_) Dai(chainId_) {
    
  }

  function mint(address usr, uint wad) public virtual override {
    balanceOf[usr] = add(balanceOf[usr], wad);
    totalSupply    = add(totalSupply, wad);
    emit Transfer(address(0), usr, wad);
  }
}