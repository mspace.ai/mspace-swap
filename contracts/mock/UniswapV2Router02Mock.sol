// SPDX-License-Identifier: GPL-3.0

pragma solidity ^0.8.0;

import "../uniswapv2/libraries/SafeMathUniswap.sol";
import '../uniswapv2/libraries/UniswapV2Library.sol';
import '../uniswapv2/libraries/TransferHelper.sol';
import './IUniswapV2Router02Mock.sol';
import '../uniswapv2/interfaces/IUniswapV2Factory.sol';
import '../uniswapv2/interfaces/IWMETA.sol';
import '@openzeppelin/contracts/proxy/utils/Initializable.sol';

contract UniswapV2Router02Mock is IUniswapV2Router02Mock, Initializable {
    using SafeMathUniswap for uint;

    address public override factory;
    address public override WMETA;

    modifier ensure(uint deadline) {
        require(deadline >= block.timestamp, 'Router: EXPIRED');
        _;
    }

    function init(address _factory, address _WMETA) public initializer {
        factory = _factory;
        WMETA = _WMETA;
    }


    receive() external payable {
        assert(msg.sender == WMETA); // only accept META via fallback from the WMETA contract
    }

    // **** ADD LIQUIDITY ****
    function _addLiquidity(
        address tokenA,
        address tokenB,
        uint amountADesired,
        uint amountBDesired,
        uint amountAMin,
        uint amountBMin
    ) internal virtual returns (uint amountA, uint amountB) {
        // create the pair if it doesn't exist yet
        if (IUniswapV2Factory(factory).getPair(tokenA, tokenB) == address(0)) {
            IUniswapV2Factory(factory).createPair(tokenA, tokenB);
        }
        (uint reserveA, uint reserveB) = UniswapV2Library.getReserves(factory, tokenA, tokenB);
        if (reserveA == 0 && reserveB == 0) {
            (amountA, amountB) = (amountADesired, amountBDesired);
        } else {
            uint amountBOptimal = UniswapV2Library.quote(amountADesired, reserveA, reserveB);
            if (amountBOptimal <= amountBDesired) {
                require(amountBOptimal >= amountBMin, 'Router: INSUFFICIENT_B_AMOUNT');
                (amountA, amountB) = (amountADesired, amountBOptimal);
            } else {
                uint amountAOptimal = UniswapV2Library.quote(amountBDesired, reserveB, reserveA);
                assert(amountAOptimal <= amountADesired);
                require(amountAOptimal >= amountAMin, 'Router: INSUFFICIENT_A_AMOUNT');
                (amountA, amountB) = (amountAOptimal, amountBDesired);
            }
        }
    }
    function addLiquidity(
        address tokenA,
        address tokenB,
        uint amountADesired,
        uint amountBDesired,
        uint amountAMin,
        uint amountBMin,
        address to,
        uint deadline
    ) external virtual override ensure(deadline) returns (uint amountA, uint amountB, uint liquidity) {
        (amountA, amountB) = _addLiquidity(tokenA, tokenB, amountADesired, amountBDesired, amountAMin, amountBMin);
        // address pair = UniswapV2Library.pairFor(factory, tokenA, tokenB);
        address pair = IUniswapV2Factory(factory).getPair(tokenA, tokenB);
        TransferHelper.safeTransferFrom(tokenA, msg.sender, pair, amountA);
        TransferHelper.safeTransferFrom(tokenB, msg.sender, pair, amountB);
        liquidity = IUniswapV2Pair(pair).mint(to);
    }
    function addLiquidityMETA(
        address token,
        uint amountTokenDesired,
        uint amountTokenMin,
        uint amountMETAMin,
        address to,
        uint deadline
    ) external virtual override payable ensure(deadline) returns (uint amountToken, uint amountMETA, uint liquidity) {
        (amountToken, amountMETA) = _addLiquidity(
            token,
            WMETA,
            amountTokenDesired,
            msg.value,
            amountTokenMin,
            amountMETAMin
        );
        // address pair = UniswapV2Library.pairFor(factory, token, WMETA);
        address pair = IUniswapV2Factory(factory).getPair(token, WMETA);
        TransferHelper.safeTransferFrom(token, msg.sender, pair, amountToken);
        IWMETA(WMETA).deposit{value: amountMETA}();
        assert(IWMETA(WMETA).transfer(pair, amountMETA));
        liquidity = IUniswapV2Pair(pair).mint(to);
        // refund dust meta, if any
        if (msg.value > amountMETA) TransferHelper.safeTransferMETA(msg.sender, msg.value - amountMETA);
    }

    // **** REMOVE LIQUIDITY ****
    function removeLiquidity(
        address tokenA,
        address tokenB,
        uint liquidity,
        uint amountAMin,
        uint amountBMin,
        address to,
        uint deadline
    ) public virtual override ensure(deadline) returns (uint amountA, uint amountB) {
        // address pair = UniswapV2Library.pairFor(factory, tokenA, tokenB);
        address pair = IUniswapV2Factory(factory).getPair(tokenA, tokenB);
        IUniswapV2Pair(pair).transferFrom(msg.sender, pair, liquidity); // send liquidity to pair
        (uint amount0, uint amount1) = IUniswapV2Pair(pair).burn(to);
        (address token0,) = UniswapV2Library.sortTokens(tokenA, tokenB);
        (amountA, amountB) = tokenA == token0 ? (amount0, amount1) : (amount1, amount0);
        require(amountA >= amountAMin, 'Router: INSUFFICIENT_A_AMOUNT');
        require(amountB >= amountBMin, 'Router: INSUFFICIENT_B_AMOUNT');
    }
    function removeLiquidityMETA(
        address token,
        uint liquidity,
        uint amountTokenMin,
        uint amountMETAMin,
        address to,
        uint deadline
    ) public virtual override ensure(deadline) returns (uint amountToken, uint amountMETA) {
        (amountToken, amountMETA) = removeLiquidity(
            token,
            WMETA,
            liquidity,
            amountTokenMin,
            amountMETAMin,
            address(this),
            deadline
        );
        TransferHelper.safeTransfer(token, to, amountToken);
        IWMETA(WMETA).withdraw(amountMETA);
        TransferHelper.safeTransferMETA(to, amountMETA);
    }

    // **** SWAP ****
    // requires the initial amount to have already been sent to the first pair
    function _swap(uint[] memory amounts, address[] memory path, address _to) internal virtual {
        for (uint i; i < path.length - 1; i++) {
            (address input, address output) = (path[i], path[i + 1]);
            (address token0,) = UniswapV2Library.sortTokens(input, output);
            uint amountOut = amounts[i + 1];
            (uint amount0Out, uint amount1Out) = input == token0 ? (uint(0), amountOut) : (amountOut, uint(0));
            // address to = i < path.length - 2 ? UniswapV2Library.pairFor(factory, output, path[i + 2]) : _to;
            address to = i < path.length - 2 ? IUniswapV2Factory(factory).getPair(output, path[i + 2]) : _to;
            // IUniswapV2Pair(UniswapV2Library.pairFor(factory, input, output)).swap(
            //     amount0Out, amount1Out, to, new bytes(0)
            // );
            IUniswapV2Pair(IUniswapV2Factory(factory).getPair(input, output)).swap(
                amount0Out, amount1Out, to, new bytes(0)
            );
        }
    }
    function swapExactTokensForTokens(
        uint amountIn,
        uint amountOutMin,
        address[] calldata path,
        address to,
        uint deadline
    ) external virtual override ensure(deadline) returns (uint[] memory amounts) {
        amounts = UniswapV2Library.getAmountsOut(factory, amountIn, path);
        require(amounts[amounts.length - 1] >= amountOutMin, 'Router: INSUFFICIENT_OUTPUT_AMOUNT');
        // TransferHelper.safeTransferFrom(
        //     path[0], msg.sender, UniswapV2Library.pairFor(factory, path[0], path[1]), amounts[0]
        // );
        TransferHelper.safeTransferFrom(
            path[0], msg.sender, IUniswapV2Factory(factory).getPair(path[0], path[1]), amounts[0]
        );
        _swap(amounts, path, to);
    }
    function swapExactMETAForTokens(uint amountOutMin, address[] calldata path, address to, uint deadline)
        external
        virtual
        override
        payable
        ensure(deadline)
        returns (uint[] memory amounts)
    {
        require(path[0] == WMETA, 'Router: INVALID_PATH');
        amounts = UniswapV2Library.getAmountsOut(factory, msg.value, path);
        require(amounts[amounts.length - 1] >= amountOutMin, 'Router: INSUFFICIENT_OUTPUT_AMOUNT');
        IWMETA(WMETA).deposit{value: amounts[0]}();
        // assert(IWMETA(WMETA).transfer(UniswapV2Library.pairFor(factory, path[0], path[1]), amounts[0]));
        assert(IWMETA(WMETA).transfer(IUniswapV2Factory(factory).getPair(path[0], path[1]), amounts[0]));
        _swap(amounts, path, to);
    }

    // **** LIBRARY FUNCTIONS ****
    function quote(uint amountA, uint reserveA, uint reserveB) public pure virtual override returns (uint amountB) {
        return UniswapV2Library.quote(amountA, reserveA, reserveB);
    }

    function getAmountOut(uint amountIn, uint reserveIn, uint reserveOut)
        public
        pure
        virtual
        override
        returns (uint amountOut)
    {
        return UniswapV2Library.getAmountOut(amountIn, reserveIn, reserveOut);
    }

    function getAmountIn(uint amountOut, uint reserveIn, uint reserveOut)
        public
        pure
        virtual
        override
        returns (uint amountIn)
    {
        return UniswapV2Library.getAmountIn(amountOut, reserveIn, reserveOut);
    }

    function getAmountsOut(uint amountIn, address[] memory path)
        public
        view
        virtual
        override
        returns (uint[] memory amounts)
    {
        return UniswapV2Library.getAmountsOut(factory, amountIn, path);
    }

    function getAmountsIn(uint amountOut, address[] memory path)
        public
        view
        virtual
        override
        returns (uint[] memory amounts)
    {
        return UniswapV2Library.getAmountsIn(factory, amountOut, path);
    }

    // function pairFor(address tokenA, address tokenB) public view returns (address){
    //     return UniswapV2Library.pairFor(factory, tokenA, tokenB);
    // }
}
