// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

// import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "../uniswapv2/UniswapV2ERC20.sol";

contract PairMockV2000 is UniswapV2ERC20 {
  address public token0;
  address public token1;
  
  // function init(address _token0, address _token1, address _token2) external initializer {
  function init(address _token0, address _token1) external {
    // __Ownable_init();
    UniswapV2ERC20_init();
    token0 = _token0;
    token1 = _token1;
  }

  function version() external virtual pure returns (string memory) {
    return "2000";
  }
}
