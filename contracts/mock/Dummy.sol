// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";

contract Dummy is OwnableUpgradeable {
  uint public chainId;
  uint256 public prelaunch; 
  uint256 public prelaunchReward;

  constructor() {
    uint _chainId;
    assembly {
        _chainId := chainid()
    }
    chainId = _chainId;
  }

  function init(
    uint256 _prelaunch, uint256 _prelaunchReward
  ) public initializer {
    __Ownable_init();
    prelaunch = _prelaunch;
    prelaunchReward = _prelaunchReward;
  }

}