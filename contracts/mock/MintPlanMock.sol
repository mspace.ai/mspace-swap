// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import "../stake/MSpaceMintPlan.sol";

contract MintPlanMock is MSpaceMintPlan {
  function init(
    uint256 startReward, 
    uint256 blockStep
  ) public initializer {
    super.initPlan(startReward, blockStep);
  }
}