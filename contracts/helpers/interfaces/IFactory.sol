// SPDX-License-Identifier: GPL-3.0

pragma solidity ^0.8.0;

import "./IERC20.sol";
import "./IPair.sol";

interface IFactory {
    function allPairsLength() external view returns (uint256);

    function allPairs(uint256 i) external view returns (IPair);

    function getPair(IERC20 token0, IERC20 token1) external view returns (IPair);

    function feeTo() external view returns (address);

    function feeToSetter() external view returns (address);
}