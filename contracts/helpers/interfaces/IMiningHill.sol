// SPDX-License-Identifier: GPL-3.0

pragma solidity ^0.8.0;

interface IMiningHill {
    function totalAllocPoint() external view returns (uint256);

    function poolLength() external view returns (uint256);

    function poolInfo(uint256 nr)
        external
        view
        returns (
            address,
            uint256,
            uint256,
            uint256,
            uint256
        );

    function userInfo(uint256 nr, address who) external view returns (uint256, uint256);

    function pendingMSpace(uint256 nr, address who) external view returns (uint256);
}