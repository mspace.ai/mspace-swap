// SPDX-License-Identifier: GPL-3.0

pragma solidity ^0.8.0;

import './interfaces/IUniswapV2Factory.sol';
import './interfaces/IUniswapV2Pair.sol';
import '../proxy/UpgradeableProxy.sol';
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";

contract UniswapV2Factory is IUniswapV2Factory, OwnableUpgradeable {
    address public override feeTo;
    address public override migrator;

    mapping(address => mapping(address => address)) public override getPair;
    address[] public override allPairs;
    IUniswapV2Pair public pairImplement;

    function init(IUniswapV2Pair erc20Pair) public initializer {
        __Ownable_init();
        pairImplement = erc20Pair;
    }

    function setPairImplement(IUniswapV2Pair erc20Pair) public onlyOwner {
        require(address(erc20Pair) != address(0), "UniswapV2: IMPLEMENT_INVALID");
        pairImplement = erc20Pair;
    }

    function upgradePair(address tokenA, address tokenB) public {
        UpgradeableProxy pairProxy = UpgradeableProxy(payable(getPair[tokenA][tokenB]));
        require(
          address(pairProxy) != address(0) 
          && pairProxy.admin() == address(this)
          && pairProxy.implementation() != address(pairImplement)
        , "UniswapV2: UPGRADE_ERROR");
        pairProxy.upgradeTo(address(pairImplement));
    }

    function lockPair(address tokenA, address tokenB) public onlyOwner {
        address pair = getPair[tokenA][tokenB];
        require(pair != address(0), "UniswapV2: PAIR_INVALID");
        IUniswapV2Pair(pair).setLock(true);
    }

    function unlockPair(address tokenA, address tokenB) public onlyOwner {
        address pair = getPair[tokenA][tokenB];
        require(pair != address(0), "UniswapV2: PAIR_INVALID");
        IUniswapV2Pair(pair).setLock(false);
    }

    function allPairsLength() external override view returns (uint) {
        return allPairs.length;
    }

    function createPair(address tokenA, address tokenB) external override returns (address pair) {
        require(tokenA != tokenB, 'UniswapV2: IDENTICAL_ADDRESSES');
        (address token0, address token1) = tokenA < tokenB ? (tokenA, tokenB) : (tokenB, tokenA);
        require(token0 != address(0), 'UniswapV2: ZERO_ADDRESS');
        require(getPair[token0][token1] == address(0), 'UniswapV2: PAIR_EXISTS'); // single check is sufficient
        UpgradeableProxy pairProxy = new UpgradeableProxy(
          address(pairImplement), address(this), bytes("")
        );
        pair = address(pairProxy);
        IUniswapV2Pair(address(pair)).initialize(token0, token1);
        getPair[token0][token1] = pair;
        getPair[token1][token0] = pair; // populate mapping in the reverse direction
        allPairs.push(pair);
        emit PairCreated(token0, token1, pair, allPairs.length);
    }

    function setFeeTo(address _feeTo) external override onlyOwner {
        feeTo = _feeTo;
    }

    function setMigrator(address _migrator) external override onlyOwner {
        migrator = _migrator;
    }
}
