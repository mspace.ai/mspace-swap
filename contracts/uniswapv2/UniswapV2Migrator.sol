// SPDX-License-Identifier: GPL-3.0

pragma solidity ^0.8.0;

import "./interfaces/IUniswapV2Factory.sol";

contract UniswapV2Migrator {
  address public mining;
  address public oldFactory;
  IUniswapV2Factory public factory;
  uint256 public notBeforeBlock;
  
  constructor(
    address _oldFactory,
    IUniswapV2Factory _factory,
    uint256 _notBeforeBlock
  ) {
    oldFactory = _oldFactory;
    factory = _factory;
    notBeforeBlock = _notBeforeBlock;
  }

  function desiredLiquidity() external view returns (uint256) {
    // TODO write this
  }
}