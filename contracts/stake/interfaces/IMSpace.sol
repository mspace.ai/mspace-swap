// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";

// need for import in MiningHill. 
// to pass error: Task flatten and then build will discover multiple MSpaceToken 
// => cause error `cannot find artifact "MSpaceToken"`
interface IMSpace is IERC20 {
  function setTeamWallet(address teamWallet) external;
  function setAirdropWallet(address airdropWallet) external;
  function airdrop1() external;
  function airdrop2() external;
  function teamVesting() external;
  function mint(address to, uint256 amount) external;
}