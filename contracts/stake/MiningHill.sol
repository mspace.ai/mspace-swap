// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";
import "@openzeppelin/contracts/utils/math/SafeMath.sol";
import "./interfaces/IMSpace.sol";
import "./MSpaceMintPlan.sol";

interface IMigrator {
  function migrate(IERC20 token) external returns (IERC20);
}

contract MiningHill is MSpaceMintPlan {
  // version 0.2.0
  using SafeMath for uint256;
  using SafeERC20 for IERC20;
  // Info of each user.
  struct UserInfo {
    uint256 amount; // How many LP tokens the user has provided.
    uint256 lastUpdate;
    uint256 poolShare;
  }
  // Info of each pool.
  struct PoolInfo {
    IERC20 lpToken; // Address of LP token contract.
    uint256 allocPoint; // How many allocation points assigned to the pool.
    uint256 lastUpdate; // Last block number that MSPs distribution occurs.
    uint256 totalShare; // total share of each users when last update
    uint256 rewardDebt;
  }
  // The MSP TOKEN!
  IMSpace public mspace;
  // The migrator contract. It has a lot of power. Can only be set through governance (owner).
  IMigrator public migrator;
  // Info of each pool.
  PoolInfo[] public poolInfo;
  mapping(address => bool) public poolIsExist;
  // Info of each user that stakes LP tokens.
  mapping(uint256 => mapping(address => UserInfo)) public userInfo;
  // Total allocation poitns. Must be the sum of all allocation points in all pools.
  uint256 public totalAllocPoint = 0;

  event Deposit(address indexed user, uint256 indexed pid, uint256 amount);
  event Withdraw(address indexed user, uint256 indexed pid, uint256 amount);
  event EmergencyWithdraw(address indexed user, uint256 indexed pid, uint256 amount);
  event AddPool(uint256 allocPoint, IERC20 lpToken, bool withUpdate);
  event SetPoolAllocPoint(uint256 pid, uint256 allocPoint, bool withUpdate);
  event SetMigrator(IMigrator poolMigrator);
  event Migrate(uint256 pid);
  event MassUpdatePools();
  event UpdatePool(uint256 pid);
  event UpdateUser(uint256 pid, uint256 userAddress);
  event Harvest(address indexed user, uint256 indexed pid, uint256 amount);

  function init(
    address operationWallet,
    IMSpace mspaceToken,
    uint256 startReward, 
    uint256 blockStep
  ) public initializer {
    require(
      operationWallet != address(0) 
        && address(mspaceToken) != address(0)
    , "Mining: init error");
    super.initPlan(startReward, blockStep);
    setAddress("operationWallet", operationWallet);
    mspace = mspaceToken;
  }

  function poolLength() external view returns (uint256) {
    return poolInfo.length;
  }

  // Add a new lp to the pool. Can only be called by the owner.
  // XXX DO NOT add the same LP token more than once. Rewards will be messed up if you do.
  function add(uint256 allocPoint, IERC20 lpToken, bool withUpdate) public onlyOwner {
    require(poolIsExist[address(lpToken)] == false, "Mining: pool existed");
    if (withUpdate) {
      massUpdatePools();
    }
    totalAllocPoint = totalAllocPoint.add(allocPoint);
    PoolInfo memory pool = PoolInfo({
      lpToken: lpToken, 
      allocPoint: allocPoint, 
      lastUpdate: block.number, 
      totalShare: 0, 
      rewardDebt: 0
    });
    poolIsExist[address(lpToken)] = true;
    poolInfo.push(pool);
    emit AddPool(allocPoint, lpToken, withUpdate);
  }

  // Update the given pool's MSP allocation point. Can only be called by the owner.
  function set(uint256 pid, uint256 allocPoint, bool withUpdate) public onlyOwner {
    if (withUpdate) {
      massUpdatePools();
    } else {
      updatePool(pid);
    }
    totalAllocPoint = totalAllocPoint.sub(poolInfo[pid].allocPoint).add(allocPoint);
    poolInfo[pid].allocPoint = allocPoint;
    emit SetPoolAllocPoint(pid, allocPoint, withUpdate);
  }

  // Set the migrator contract. Can only be called by the owner.
  function setMigrator(IMigrator poolMigrator) public onlyOwner {
    migrator = poolMigrator;
    emit SetMigrator(poolMigrator);
  }

  // Migrate lp token to another lp contract. Can be called by anyone. We trust that migrator contract is good.
  function migrate(uint256 pid) public {
    require(address(migrator) != address(0), "migrate: no migrator");
    PoolInfo storage pool = poolInfo[pid];
    IERC20 lpToken = pool.lpToken;
    uint256 bal = lpToken.balanceOf(address(this));
    lpToken.safeApprove(address(migrator), bal);
    IERC20 newLpToken = migrator.migrate(lpToken);
    require(bal == newLpToken.balanceOf(address(this)), "migrate: bad");
    pool.lpToken = newLpToken;
    emit Migrate(pid);
  }

  function rewardDebt(uint256 pid) public view returns (uint256) {
    require(pid < poolInfo.length, "Mining: pool invalid");
    PoolInfo memory pool = poolInfo[pid];
    uint256 reward = calcReward(pool.lastUpdate, block.number);
    uint256 mspaceReward = totalAllocPoint==0 ? 0:reward.mul(pool.allocPoint).div(totalAllocPoint);
    return pool.rewardDebt.add(mspaceReward.mul(90).div(100));
  }

  function pendingMSpace(uint256 pid, address userAddress) external view returns (uint256) {
    require(pid < poolInfo.length, "Mining: pool invalid");
    uint256 startReward = getUint("startReward");
    PoolInfo memory pool = poolInfo[pid];
    UserInfo memory user = userInfo[pid][userAddress];
    uint256 lpSupply = pool.lpToken.balanceOf(address(this));
    if (block.number < startReward) {
      return 0;
    }
    uint256 totalShare = pool.totalShare
      .add(block.number.sub(pool.lastUpdate > startReward ? pool.lastUpdate:startReward).mul(lpSupply));
    uint256 userShare = user.poolShare
      .add(block.number.sub(user.lastUpdate > startReward ? user.lastUpdate:startReward).mul(user.amount));

    return totalShare==0 ? 0:rewardDebt(pid).mul(userShare).div(totalShare);
  }

  // Update reward vairables for all pools. Be careful of gas spending!
  function massUpdatePools() public {
    uint256 length = poolInfo.length;
    for (uint256 pid = 0; pid < length; ++pid) {
      updatePool(pid);
    }
    emit MassUpdatePools();
  }

  // Update reward variables of the given pool to be up-to-date.
  function updatePool(uint256 pid) public {
    require(pid < poolInfo.length, "Mining: not valid pool");
    PoolInfo storage pool = poolInfo[pid];
    uint256 lpSupply = pool.lpToken.balanceOf(address(this));
    uint256 reward = calcReward(pool.lastUpdate, block.number);
    uint256 mspaceReward = totalAllocPoint==0 ? 0:reward.mul(pool.allocPoint).div(totalAllocPoint);
    if (mspaceReward > 0) {
      mspace.mint(address(this), mspaceReward.mul(90).div(100));
      mspace.mint(getAddress("operationWallet"), mspaceReward.div(10));
      pool.rewardDebt = pool.rewardDebt.add(mspaceReward.mul(90).div(100));
    }
    uint256 startReward = getUint("startReward");
    if (block.number > startReward) {
      pool.totalShare = pool.totalShare.add(
        block.number.sub(pool.lastUpdate > startReward ? pool.lastUpdate:startReward).mul(lpSupply)
      );
    }
    pool.lastUpdate = block.number;
    emit UpdatePool(pid);
  }

  function updateUser(uint256 pid, address userAddress) internal {
    UserInfo storage user = userInfo[pid][userAddress];
    uint256 startReward = getUint("startReward");
    if (block.number > startReward) {
      user.poolShare = user.poolShare
        .add(block.number.sub(user.lastUpdate > startReward ? user.lastUpdate:startReward).mul(user.amount));
    }
    user.lastUpdate = block.number;
  }

  // Deposit LP tokens to Mining for MSP allocation.
  function deposit(uint256 pid, uint256 amount) public {
    require(amount > 0, "Mining: not valid amount");
    updatePool(pid);
    updateUser(pid, msg.sender);
    PoolInfo storage pool = poolInfo[pid];
    UserInfo storage user = userInfo[pid][msg.sender];
    pool.lpToken.safeTransferFrom(msg.sender, address(this), amount);
    user.amount = user.amount.add(amount);
    emit Deposit(msg.sender, pid, amount);
  }

  function harvest(uint256 pid) public {
    updatePool(pid);
    updateUser(pid, msg.sender);
    PoolInfo storage pool = poolInfo[pid];
    UserInfo storage user = userInfo[pid][msg.sender];
    uint256 pending = pool.totalShare == 0 ? 0:pool.rewardDebt.mul(user.poolShare).div(pool.totalShare);
    if (pending > 0) {
      // correct rounding error
      if (pending > pool.rewardDebt) {
        pending = pool.rewardDebt;
      }
      safeMSpaceTransfer(msg.sender, pending);
      pool.totalShare = pool.totalShare.sub(user.poolShare);
      pool.rewardDebt = pool.rewardDebt.sub(pending);
      user.poolShare = 0;
    }
    emit Harvest(msg.sender, pid, pending);
  }

  // Withdraw LP tokens from Mining.
  function withdraw(uint256 pid, uint256 amount) public {
    updatePool(pid);
    updateUser(pid, msg.sender);
    PoolInfo storage pool = poolInfo[pid];
    UserInfo storage user = userInfo[pid][msg.sender];
    require(user.amount >= amount, "Mining: not enough");
    user.amount = user.amount.sub(amount);
    pool.lpToken.safeTransfer(msg.sender, amount);
    emit Withdraw(msg.sender, pid, amount);
  }

  // Withdraw without caring about rewards. EMERGENCY ONLY.
  function emergencyWithdraw(uint256 pid) public {
    updatePool(pid);
    updateUser(pid, msg.sender);
    PoolInfo storage pool = poolInfo[pid];
    UserInfo storage user = userInfo[pid][msg.sender];
    uint256 amount = user.amount;
    user.amount = 0;
    pool.lpToken.safeTransfer(msg.sender, amount);
    emit EmergencyWithdraw(msg.sender, pid, amount);
  }

  // Safe MSP transfer function, just in case if rounding error causes pool to not have enough MSPs.
  function safeMSpaceTransfer(address to, uint256 amount) internal {
    uint256 mspaceBal = mspace.balanceOf(address(this));
    if (amount > mspaceBal) {
      mspace.transfer(to, mspaceBal);
    } else {
      mspace.transfer(to, amount);
    }
  }

  function setTeamWallet(address teamWallet) public onlyOwner {
    mspace.setTeamWallet(teamWallet);
  }
  function setAirdropWallet(address airdropWallet) public onlyOwner {
    mspace.setAirdropWallet(airdropWallet);
  }
  function airdrop1() public onlyOwner {
    mspace.airdrop1();
  }
  function airdrop2() public onlyOwner {
    mspace.airdrop2();
  }
  function teamVesting() public onlyOwner {
    mspace.teamVesting();
  }
}
