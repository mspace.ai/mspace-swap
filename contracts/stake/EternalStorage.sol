// SPDX-License-Identifier: MIT

// for future upgrade purposes
// code adopted from https://github.com/poanetwork/tokenbridge-contracts/blob/908a481079/contracts/upgradeability/EternalStorage.sol

pragma solidity ^0.8.0;

contract EternalStorage {
  mapping(bytes32 => address) internal addressStorage;
  mapping(bytes32 => uint256) internal uintStorage;
  mapping(bytes32 => string) internal stringStorage;
  mapping(bytes32 => bool) internal boolStorage;
  mapping(bytes32 => bytes) internal bytesStorage;
  mapping(bytes32 => int256) internal intStorage;

  function setAddress(string memory _key, address _value) internal {
    addressStorage[keccak256(abi.encodePacked(_key))] = _value;
  }
  function getAddress(string memory _key) public view returns(address) {
    return addressStorage[keccak256(abi.encodePacked(_key))];
  }

  function setUint(string memory _key, uint256 _value) internal {
    uintStorage[keccak256(abi.encodePacked(_key))] = _value;
  }
  function getUint(string memory _key) public view returns(uint256) {
    return uintStorage[keccak256(abi.encodePacked(_key))];
  }

  function setString(string memory _key, string memory _value) internal {
    stringStorage[keccak256(abi.encodePacked(_key))] = _value;
  }
  function getString(string memory _key) public view returns(string memory) {
    return stringStorage[keccak256(abi.encodePacked(_key))];
  }

  function setBool(string memory _key, bool _value) internal {
    boolStorage[keccak256(abi.encodePacked(_key))] = _value;
  }
  function getbool(string memory _key) public view returns(bool) {
    return boolStorage[keccak256(abi.encodePacked(_key))];
  }

  function setBytes(string memory _key, bytes memory _value) internal {
    bytesStorage[keccak256(abi.encodePacked(_key))] = _value;
  }
  function getBytes(string memory _key) public view returns(bytes memory) {
    return bytesStorage[keccak256(abi.encodePacked(_key))];
  }

  function setInt(string memory _key, int256 _value) internal {
    intStorage[keccak256(abi.encodePacked(_key))] = _value;
  }
  function getInt(string memory _key) public view returns(int256) {
    return intStorage[keccak256(abi.encodePacked(_key))];
  }
  
}