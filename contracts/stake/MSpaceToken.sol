// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import "@openzeppelin/contracts-upgradeable/token/ERC20/ERC20Upgradeable.sol";  
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts/utils/math/SafeMath.sol";
import "./EternalStorage.sol";

contract MSpaceToken is EternalStorage, ERC20Upgradeable, OwnableUpgradeable {
  using SafeMath for uint256;
  /// @notice Creates `_amount` token to `_to`. Must only be called by the owner (SpaceCenter).
  function mint(address _to, uint256 _amount) public onlyOwner {
    _mint(_to, _amount);
  }

  function init(
    uint256 blockPerYear,

    address teamWallet, 
    uint256 teamVestPerYear,
    uint256 teamVestStart,

    address airdropWallet,
    uint256 airdropAmount,
    uint256 airdrop1st,
    uint256 airdrop2nd
  ) public initializer {
    __ERC20_init("MSP Token", "MSP");
    __Ownable_init();
    require(
      blockPerYear > 0
        && teamWallet != address(0)
        && teamVestPerYear > 0
        && teamVestStart >= block.number

        && airdropWallet != address(0)
        && airdropAmount > 0
        && airdrop1st > block.number
        && airdrop2nd > airdrop1st
    , "MSP: init error");
    setUint("blockPerYear", blockPerYear);

    setAddress("teamWallet", teamWallet);
    setUint("teamVestPerYear", teamVestPerYear);
    setUint("teamVestStart", teamVestStart);
    setUint("teamVestYear", 0);

    setAddress("airdropWallet", airdropWallet);
    setUint("airdropAmount", airdropAmount);
    setUint("airdrop1st", airdrop1st);
    setUint("airdrop2nd", airdrop2nd);
  }

  function setTeamWallet(address teamWallet) public onlyOwner {
    setAddress("teamWallet", teamWallet);
  }
  function setAirdropWallet(address airdropWallet) public onlyOwner {
    setAddress("airdropWallet", airdropWallet);
  }

  function airdrop1() public onlyOwner {
    require(getUint("airdrop1st") <= block.number, "MSP: not in time to drop round 1st");
    mint(getAddress("airdropWallet"),  getUint("airdropAmount"));
  }
  function airdrop2() public onlyOwner {
    require(getUint("airdrop2nd") <= block.number, "MSP: not in time to drop round 2nd");
    mint(getAddress("airdropWallet"), getUint("airdropAmount"));
  }

  function teamVesting() public onlyOwner {
    uint256 teamVestYear = getUint("teamVestYear");
    require(teamVestYear<4, "MSP: vesting done");

    uint256 teamVestStart = getUint("teamVestStart");
    uint256 blockPerYear = getUint("blockPerYear");
    require(block.number > teamVestStart, "MSP: not in time");
    uint256 yearsPassed = block.number.sub(teamVestStart).div(blockPerYear);
    require(yearsPassed > teamVestYear, "MSP: not in time");
    if (yearsPassed>4) {
      yearsPassed = 4;
    }

    mint(getAddress("teamWallet"), getUint("teamVestPerYear").mul(yearsPassed.sub(teamVestYear)));
    setUint("teamVestYear", yearsPassed);
  }
}