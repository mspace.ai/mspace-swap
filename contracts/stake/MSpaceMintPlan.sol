// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import "@openzeppelin/contracts/utils/math/SafeMath.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "./EternalStorage.sol";

contract MSpaceMintPlan is EternalStorage, OwnableUpgradeable {
  using SafeMath for uint256;

  // reward strategy, each point is reward per block of a month
  uint256[] public rewardStrategy;
  event UpdateConfig();

  function initPlan(
    uint256 startReward,
    uint256 blockStep
  ) internal {
    require(startReward >= block.number && blockStep > 0, "MintPlan: init error");
    __Ownable_init();
    
    // Block number when start reward
    setUint("startReward", startReward);
    // Block range for each formula apply
    setUint("blockStep", blockStep);
  }

  function setStrategy(uint256[] calldata points) public onlyOwner {
    uint256 length = points.length;
    for (uint256 i = 0; i < length; ++i) {
      rewardStrategy.push(points[i]);
    }
    emit UpdateConfig();
  }

  function setStrategyPoint(uint256 index, uint256 point) public onlyOwner {
    require(index < rewardStrategy.length, "MintPlan: point index invalid");
    rewardStrategy[index] = point;
    emit UpdateConfig();
  }

  function setBlockStep(uint256 blockStep) public onlyOwner {
    setUint("blockStep", blockStep);
    emit UpdateConfig();
  }

  function strategyLength() external view returns (uint256) {
    return rewardStrategy.length;
  }

  function mspacePerBlock() external view returns (uint256) {
    uint256 startReward = getUint("startReward");
    uint256 current = block.number;
    if (current < startReward) {
      return 0;
    }
    uint256 blockStep = getUint("blockStep");
    uint256 pos = current.sub(startReward).div(blockStep);
    if (pos >= rewardStrategy.length) {
      return 0;
    }
    return rewardStrategy[pos];
  }

  function calcReward(uint256 from, uint256 to) public view returns(uint256) {
    require(to >= from, "MintPlan: calc reward input error");
    uint256 startReward = getUint("startReward");
    from = from < startReward ? 0:from.sub(startReward);
    to = to < startReward ? 0:to.sub(startReward);

    uint256 blockStep = getUint("blockStep");
    uint256 start = from.div(blockStep);
    uint256 end = to.div(blockStep);
    if (end >= rewardStrategy.length) {
      end = rewardStrategy.length;
    }
    if (start >= rewardStrategy.length) {
      return 0;
    }
    if(start == end) {
      return to.sub(from).mul(rewardStrategy[end]);
    }
    uint256 reward = 0;
    for (uint256 i = start; i < end; ++i) {
      // .    .    .
      //   [|  ]
      reward = reward.add(
        i.add(1).mul(blockStep).sub(from).mul(rewardStrategy[i])
      );
      // .   .   .
      //     |    
      from = i.add(1).mul(blockStep);
    }
    //   .   . | .  
    //      [  |]
    if (end >= 0 && end < rewardStrategy.length) {
      reward = reward.add(
        to.sub(end.mul(blockStep)).mul(rewardStrategy[end])
      );
    }
    return reward;
  }
}