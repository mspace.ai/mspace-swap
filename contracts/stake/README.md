# Stake area

Code from [sushiswap](https://github.com/sushiswap/sushiswap/tree/94ea7712daaa13155dfab9786aacf69e24390147/contracts) with the following modifications.

1. Change contract version to 0.8.0 and do the necessary patching.
2. Change `MasterChef` to `MiningHill` & change the way mint token
3. Change `SushiToken` to `MSpaceToken` & remove unused functions
4. Change `SushiBar` to `Bank`
5. Change `SushiMaker` to `Extractor`