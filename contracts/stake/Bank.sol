// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC20/ERC20Upgradeable.sol";
import "@openzeppelin/contracts/utils/math/SafeMath.sol";

// This contract handles swapping to and from MSP, MSpace's staking token.
contract Bank is ERC20Upgradeable {
    using SafeMath for uint256;
    IERC20 public mspace;

    // Define the MSP token contract
    function init(IERC20 _mspace) public initializer {
        __ERC20_init("Bank", "xMSP");
        mspace = _mspace;
    }

    // Locks MSP and mints xMSP
    function enter(uint256 _amount) public {
        // Gets the amount of MSP locked in the contract
        uint256 totalMSpace = mspace.balanceOf(address(this));
        // Gets the amount of xMSP in existence
        uint256 totalShares = totalSupply();
        // If no XStar exists, mint it 1:1 to the amount put in
        if (totalShares == 0 || totalMSpace == 0) {
            _mint(msg.sender, _amount);
        } 
        // Calculate and mint the amount of xMSP the MSP is worth. The ratio will change overtime, as XStar is burned/minted and MSP deposited + gained from fees / withdrawn.
        else {
            uint256 what = _amount.mul(totalShares).div(totalMSpace);
            _mint(msg.sender, what);
        }
        // Lock the MSP in the contract
        mspace.transferFrom(msg.sender, address(this), _amount);
    }

    // Leave the bar. Claim back your MSPs.
    // Unlocks the staked + gained MSP and burns xMSP
    function leave(uint256 _share) public {
        // Gets the amount of xMSP in existence
        uint256 totalShares = totalSupply();
        // Calculates the amount of MSP the xMSP is worth
        uint256 what = _share.mul(mspace.balanceOf(address(this))).div(totalShares);
        _burn(msg.sender, _share);
        mspace.transfer(msg.sender, what);
    }
}
